A GTFS (General Transit Feed Specification) feed
contains information about public transit systems (with a focus on information relevant for riders).
It is composed of a series of text (`CSV`) files collected in a `ZIP` file.

Actually there are two specifications: schedule (static) and realtime.

**Official references**:
- [**gtfs.org**](https://gtfs.org) 
- [Google developer reference](https://developers.google.com/transit/gtfs/reference)

> [!TIP] Check out https://github.com/CUTR-at-USF/awesome-transit 
> for an up to date list of GTFS tools and data sources.

# Schedule Format

In a nutshell a GTFS schedule consist of:
- **Routes**, that define attributes of a line with transit type, name, etc. (e.g. metro line U6)
- **Trips** (of a route), which further define a trip (e.g. U6 towards Floridsdorf) through 
  - a reference to days when the trip is active (via **Calendar** and/or **Calendar Dates**) and
  - stops and their arrival and departure time (via **Stop Times**)
 
A GTFS feed must consist of at least these mandatory files:
(Actually it's a bit more complex with the relationship between `calendar.txt` and `calendar_dates.txt`)

| file                 | primary key                | foreign keys             |
| -------------------- | -------------------------- | ------------------------ |
| `agency.txt`         | `agency_id`                |                          |
| `stops.txt`          | `stop_id`                  |                          |
| `routes.txt`         | `route_id`                 | `agency_id`              |
| `trips.txt`          | `trip_id`                  | `route_id`, `service_id` |
| `stop_times.txt`     | `trip_id`, `stop_sequence` | `trip_id`, `stop_id`     |
| `calendar.txt`       | `service_id`               |                          |
| `calendar_dates.txt` | `service_id`, `date`       | (`calendar.service_id`)  |

Relationship between these files:

```mermaid
flowchart 
    A[agency.txt]
    S[stops.txt]
    R[routes.txt]
    T[trips.txt]
    ST[stop_times.txt]
    C[calendar.txt]
    CD[calendar_dates.txt]
    C_META{one of}
    R -- agency_id --> A
    T -- route_id --> R
    T -- service_id --> C_META
    C_META --> C
    C_META --> CD
    ST -- trip_id --> T
    ST -- stop_id --> S
    CD -. service_id .-> C
```

# Available Data

Worldwide infos about feeds:
- https://database.mobilitydata.org (=fork / updated version of https://transitfeeds.com/feeds)
- https://transit.land/feed-registry/

Specific feeds:
- Austria: https://mobilitaetsverbuende.at/ 
  - Vienna: https://www.data.gv.at/katalog/dataset/ab4a73b6-1c2d-42e1-b4d9-049e04889cf0
  - ÖBB: http://data.oebb.at
- Switzerland: https://opendata.swiss/de/dataset/timetable-2019-gtfs

# Tools

## Viewer & Manual Editors

### GTFS.html

Quickly browse/validate a GTFS file. It includes a map view for routes (although there is no overview of all routes).

Either
- visit https://gtfs.pleasantprogrammer.com and upload your GTFS (it's a local web app) or
- clone the repo at https://github.com/thatsmydoing/gtfs.html and follow the installation instructions.

In your browser make sure the calls to Google maps are not hindered (ublock, cors).

### IBI transit data tools

[Data tools](https://data-tools-docs.ibi-transit.com/en/latest/dev/deployment/) is a suite for importing / creating / editing and publishing GTFS feeds.

Quite complex to set up, usability-wise not perfect, but very powerful.

Useful for *manual editing* - we successfully added / removed lines and changed timetables for a small amount of routes. 

### static-GTFS-manager

GTFS editor, not as powerful as IBI transit (e.g. does not even have a map view for routes), but easy to install (simply download and run the executable)

https://github.com/WRI-Cities/static-GTFS-manager

## Libraries

Also looks interesting: [partridge (python)](https://github.com/remix/partridge)

### gtfs_kit (python)

https://github.com/mrcagney/gtfs_kit

Python (pandas-based) library for in-memory analysis but also changing and exporting (writing).

Notes for experimenting  with v6.0.0 and the VOR GTFS:
- files should not contain a `shapes.txt` (?) -> nope, daran scheints nicht zu liegen.
  - with shapes: loading failed after 85 minutes
  - without shapes: loading of 
- roundtrip (load and write) works, files only differ in line endings and nr of decimal places

## gtfs-lib (Java)

https://github.com/conveyal/gtfs-lib

Java library for loading and saving GTFS feeds of arbitrary size with disk-backed storage.

## CLI tools
### OneBusAway (Java/CLI)

Nice command line tool to extract / merge / edit GTFS.

[OneBusAway](https://github.com/OneBusAway/onebusaway/wiki) is a library for transforming and merging GTFS data sets. Amongst others it features two handy CLIs:

- http://developer.onebusaway.org/modules/onebusaway-gtfs-modules/current/onebusaway-gtfs-transformer-cli.html
- http://developer.onebusaway.org/modules/onebusaway-gtfs-modules/current/onebusaway-gtfs-merge-cli.html

Note, as of 2023 the developer page and ~~nexus.onebusaway.org~~ are outdated, current builds can be found on https://repo.camsys-apps.com

However, I could not find out how to browse this repo. I only managed to download resources with a direct link. E.g.
https://repo.camsys-apps.com/releases/org/onebusaway/onebusaway-gtfs-transformer-cli/1.4.4/onebusaway-gtfs-transformer-cli-1.4.5.jar
https://repo.maven.apache.org/maven2/org/onebusaway/onebusaway-gtfs-merge-cli/1.4.4/onebusaway-gtfs-merge-cli-1.4.5.jar

Find the links you need with `mvn clean package` and the following `pom.xml` - check on https://github.com/OneBusAway/onebusaway-gtfs-modules/tags what the most recent version is:
```xml
<project xmlns="http://maven.apache.org/POM/4.0.0"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <groupId>at.ac.ait</groupId>
    <artifactId>onebusaway-download</artifactId>
    <version>1.0</version>
    <properties>
        <onebusaway.version>1.4.5</onebusaway.version>
    </properties>
    <repositories>
        <repository>
            <id>public.onebusaway.org</id>
            <url>https://repo.camsys-apps.com/releases/</url>
        </repository>
    </repositories>
    <dependencies>
        <dependency>
            <groupId>org.onebusaway</groupId>
            <artifactId>onebusaway-gtfs-transformer-cli</artifactId>
            <version>${onebusaway.version}</version>
        </dependency>
        <dependency>
            <groupId>org.onebusaway</groupId>
            <artifactId>onebusaway-gtfs-merge-cli</artifactId>
            <version>${onebusaway.version}</version>
        </dependency>
    </dependencies>
</project>
```

#### Example: merge GTFS

    java -jar onebusaway-gtfs-merge-cli-1.3.118.jar a.zip b.zip out.zip

**arguments**

The merge application supports merging the following files:

- agency.txt
- stops.txt
- routes.txt
- trips.txt and stop_times.txt
- calendar.txt and calendar_dates.txt
- shapes.txt
- fare_attributes.txt
- fare_rules.txt
- frequencies.txt
- transfers.txt

For each file you can specify these arguments
- `--duplicateDetection=identity|fuzzy|none`
- `--logDroppedDuplicates`
- `--errorOnDroppedDuplicates`

This works as follows:
    `--file=stops.txt --duplicateDetection=identity --errorOnDroppedDuplicates`

Full conservative run with warnings for any duplicates:
```bash
java -jar onebusaway-gtfs-merge-cli-1.3.118.jar \
--file=agency.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=stops.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=routes.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=trips.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=calendar.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=shapes.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=fare_attributes.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=fare_rules.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=frequencies.txt --duplicateDetection=identity --logDroppedDuplicates \
--file=transfers.txt --duplicateDetection=identity --logDroppedDuplicates \
a.zip b.zip out.zip
```

#### Example: remove AST

no_ast.txt:

```
{"op":"remove","match":{"file":"routes.txt","route_short_name":"AST AST"}}
{"op":"remove","match":{"file":"routes.txt","route_short_name":"AST AST Maxi"}}
```

	java -jar onebusaway-gtfs-transformer-cli-1.3.X.jar --transform=remove_ast.txt gtfs.zip gtfs_without_ast.zip


#### Example: only keep two lines from a whole dataset

	java -jar onebusaway-gtfs-transformer-cli-1.3.34.jar --transform='{"op":"retain", "match":{"file":"routes.txt", "route_short_name":"31"}}' --transform='{"op":"retain", "match":{"file":"routes.txt", "route_short_name":"33A"}}' gtfs_vienna.zip gtfs_vienna_only31+33A.zip


## Route Shape converter

Converts `shapes.txt` to geojsons: https://github.com/kotrc/GTFS-route-shapes

Note, that this does *not* export routes without a shape file!