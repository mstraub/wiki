
How to produce plots and maps that are accessible (for color blind people)?

[Paul Tol](https://personal.sron.nl/~pault/) designed palettes for **sequential, diverging and qualitative** use cases [pdf](https://personal.sron.nl/~pault/data/colourschemes.pdf).
See e.g. the [R package khroma](https://cran.r-project.org/web/packages/khroma/vignettes/tol.html)

Other useful resources:
- https://colorbrewer2.org (for cartography)