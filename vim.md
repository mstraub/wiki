Run shell commands from within vim: prefix with `!`
```
:!ls
```

Activate paste mode (avoids unwanted indentation after each pasted line)
```
:set paste # activate
:set nopaste # deactivate
```