> [!tldr] [R](https://www.r-project.org/) is a free software environment for statistical computing and graphics

# Installation

Quick and dirty in Ubuntu:
```bash
sudo apt install r-base
```

Note, that multiple installs of R on a single Linux machine is OK and supported: https://docs.posit.co/resources/install-r.html

The most commonly used IDEs are [RStudio](https://posit.co/download/rstudio-desktop/) and VSCode.

# Basics

Start an interactive session
```bash
R
```

Get some environment infos
```r
sessionInfo() # shows R version, locale,..
library() # list available libraries
packageVersion("ggplot2") # print version of a specific package
```

Print all libraries and their versions ([source](https://www.r-bloggers.com/2015/06/list-of-user-installed-r-packages-and-their-versions/)):
```r
ip <- as.data.frame(installed.packages()[,c(1,3:4)])
rownames(ip) <- NULL
ip <- ip[is.na(ip$Priority),1:2,drop=FALSE]
print(ip, row.names=FALSE)
```

# Package Management

All R dependencies (in our case) are coming from [CRAN](https://cran.r-project.org/). The CRAN contains current and old versions of those packages. For Linux only source distributions are available (see the .tgz) - for Windows and Mac binary distributions are available.

Packages can be either installed via
1. `apt` (e.g. `apt install r-cran-ggplot2`)
2. `install.packages` from within `R`. Note, that you may need to install additional libraries (e.g. `libcurl4-openssl-dev`) for the compilation process!
```r
sudo -i R
install.packages("ggplot2")
```
3. `install.r` or `install2.r` are common bash script shortcuts to install packages

Installed packages are usually found in `/usr/local/lib/R/site-library`.

## Consistent State

Packages taken from CRAN should be in a [consistent state](https://solutions.posit.co/envs-pkgs/environments/shared/). Such a state is easily achieved by either
 1. installing all packages using the latest version available
 2. using a frozen state with the [Posit Public Package Manager](https://packagemanager.posit.co/client) aka https://p3m.dev - with e.g. this repo URL: https://packagemanager.posit.co/cran/__linux__/bookworm/2024-09-01
 3. installing all packages from your distro (e.g. using `apt install r-base r-cran-*`)

### Versioned Rocker images

Rocker provides [versioned images](https://github.com/rocker-org/rocker-versioned2)  - Note, for the latest R point release the CRAN packages are [**not frozen**](https://github.com/rocker-org/rocker-versioned2/wiki/Versions).
- barebone: https://hub.docker.com/r/rocker/r-ver
- with shiny and ~150 additional commonly used packages: https://hub.docker.com/r/rocker/shiny-verse (also preconfigured to download linux binaries via p3m)

Note: `rocker` is an open source community, `rstudio` is the old name of the company `posit` (which develops RStudio).

## Specific Versions

For reliable builds specifying versions of all packages is crucial.

This can be achieved using either
1. `renv`
2. `devtools`

With `renv` all package versions are written to a `renv.lock` file.

With `devtools` the version is contained in the R source code (see https://support.posit.co/hc/en-us/articles/219949047-Installing-older-versions-of-packages):
```r
require(devtools)
install_version("ggplot2", version = "0.9.1", repos = "http://cran.us.r-project.org")
```

Some useful links:
- R + Docker tutorial: https://solutions.posit.co/envs-pkgs/environments/docker/
- Docker shiny with dependencies: https://hosting.analythium.io/dockerized-shiny-apps-with-dependencies/
- Docker shiny Tutorial: https://www.appsilon.com/post/r-shiny-docker-getting-started
- Faster build with caches: https://github.com/robertdj/renv-docker