 >[!tldr] [pandas](https://pandas.pydata.org) is a fast, powerful, flexible and easy to use open source data analysis and manipulation tool, built on top of the [Python](https://www.python.org) programming language.

```python
import pandas as pd
```

# Configure pandas

By default pandas doesn't print all rows and columns (which makes sense of course).
Here is how you can adjust the stdout settings:
```python
pd.set_option("display.max_rows", 500)
pd.set_option("display.max_columns", 500)
pd.set_option("display.width", 1000)
```

# Inspect data

Create an example data frame
```python
data = {  
  "a": [10, 11, 12],
  "b": [99, 77, 66],
  "c": [1, 0, 0]
}
df = pd.DataFrame(data)
```

Inspect data frame
```python
df.columns # column labels
df.dtypes # column labels + data type
df.memory_usage(deep=True) # system level memory usage per column
df.describe() # quick descriptive statistics (count, mean, min, max, quantiles,..)
df.unique() # unique values
df.isna().sum() # how many nan values are present
```

Remove all NaN and infinite values from a series
```python
import numpy as np
series.replace([np.inf, -np.inf], np.nan).dropna()
```

Show only data of columns including/excluding specific types:
```python
df.select_dtypes(include=bool)
df.select_dtypes(exclude=[bool,int])
```

Count NaNs in a column
```python
df.colname.isna().sum()
```

Frequencies of values in a series
```python
series.value_counts()
series.value_counts(normalize=True) # normalize to 1 aka calc percentage
```

# Random

Equality checks useful for unit tests:
```python
from pandas.testing import assert_frame_equal, assert_series_equal
```