# Configuration

Global configuration is stored in  `~/.gitconfig`

Set commit user name and email (add `--global` so that it is written to the global config)

```bash
git config user.name "John Doe"
git config user.email "john@doe.org"
```

# Usage

show log
```bash
git log
git log --numstat # show added/deleted lines per file
```

show current branch
```bash
git branch
```

show all branches (including remote branches)
```bash
git branch -a
```

checkout a remote branch (for the first time)
```bash
git checkout -b <BRANCH> origin/<BRANCH>
```

change branch
```bash
git checkout <BRANCH>
```

create new branch and take all uncommitted changes with you
```bash
git checkout -b <BRANCH>
```

show tracked (remote) repos, e.g. the configured upstream repo
```bash
git remote -v
```

sync a github fork with the original repo that we forked from
```bash
git remote add upstream https://github.com/...
git fetch upstream
git checkout master
git merge upstream/master
git push
```

merge branch into master

	git checkout master
	git merge nameOfBranch

update branches and tags

	git pull
	git fetch --tags

update remote branches (they don't get updated by git pull)

	git remote update origin --prune

delete branch

	git branch -d <BRANCH>

delete remote branch (use the branch name without the remotes/origin prefix)

	git push origin --delete <BRANCH>

create tag (for an existing commit)

	git tag -a "name of tag" 1c880f64c441bb028e8f256293a99e49a685bbef
	git push --tags

make file executable

	git update-index --chmod=+x <FILE>
	chmod +x <FILE> # necessary because the git command does not change the flag of the local file

stash changes
```bash
git stash # by default: staged and unstaged changes
git stash --keep-index # don't stash staged files
git stash --include-untracked # also add untracked files
```

view all stashes
```bash
git stash list
```

apply stash
```bash
git stash pop  # or apply
```

reset all current changes (go back to head)

	git reset --hard HEAD

find history for a specific file (can also be deleted)

	git log --full-history -- path/to/file

Undo last (local) commit while leaving the working tree (the state of your files on disk) untouched.

	git reset HEAD~

View all ignored folders and files

    git status --ignored

## GitHub

To locally check out a GitHub pull request simply pull based on the PR ID:

```bash
git fetch origin pull/ID/head:NAME_OF_NEW_BRANCH
# e.g. git fetch origin pull/1/head:pr35
git checkout NAME_OF_NEW_BRANCH
```
## Archive

**Zipped bare git repos**, i.e. repos without the checked out content, are great for archiving purposes.

## Use an archive

Copy the .tar.gz to your local machine, unzip it, and create a new clone:

	git clone -l unzipped_folder the_repo

## Prepare repo for archiving

Simply create a fresh clone with `--bare` as argument, e.g.

	git clone --bare git@gitlab-intern.ait.ac.at:dts/myrepo.git

If the repo is not on GitLab but in a local folder use this:

	git clone --bare -l folder_with_git_repo bare_repo.git

Then create a .tar.gz

# Migrate other VCS to Git

Very helpful official guide: https://git-scm.com/book/en/v2/Git-and-Other-Systems-Migrating-to-Git

## From SVN
```bash
sudo apt install git-svn
```

We follow the official guide, whose example works perfectly for small SVN repos that follow the trunk/branches/tags convention. The `-s` switch configures the clone command to assume this convention.

**Q:** How to I simply import the whole repo?
- **A:** Replace `-s` with `--trunk=/` - this imports the whole repo as-is.

**Q:** My repo contains several subprojects and I want to keep the history and tags for them. E.g. from the `jcommons` repo we want to migrate `jcommons/licensemanger`.
- **A:** If you want to migrate a specific subdirectory simply use its path as clone URL. Note: probably this will break if contents have been moved around within the parent repo. However, this method worked well for e.g. our our `jcommons` and `commons` repos.

### Import

	git svn clone https://10.101.21.111/svn/jcommons/licensemanager --authors-file=authors_svn.txt --no-metadata --prefix "" -s licensemanager_git
	cd licensemanager_git

Use this an `authors_svn.txt` with contents like this:
```
mstraub = Markus Straub <markus.straub@ait.ac.at>
mstubenschrott = Martin Stubenschrott <martin.stubenschrott@ait.ac.at>
```

### Fix tags and branches

```bash
for t in $(git for-each-ref --format='%(refname:short)' refs/remotes/tags); do git tag ${t/tags\//} $t && git branch -D -r $t; done
for b in $(git for-each-ref --format='%(refname:short)' refs/remotes); do git branch $b refs/remotes/$b && git branch -D -r $b; done
for p in $(git for-each-ref --format='%(refname:short)' | grep @); do git branch -D $p; done
git branch -d trunk
```


### Optional: remove parts of the history

In case of very large and/or old repositories you might want to remove old cruft, e.g. binaries committed (and deleted) long ago.

A feasible approach, which we used for `mped`, might be to just keep all files currently in master and cherrypicked tags and branches (and their history of course). The rest is dropped (and archived). The approach is [(based on this stackoverflow post)](https://stackoverflow.com/a/17909526/1648538).

First analyze the repo, determine which tags and branches you want to keep, and delete the rest:

	git tag -d <LIST_OF_TAGS_TO_DELETE>
	git branch -d <LIST_OF_BRANCHES_TO_DELETE>

Then create a list of all files referenced in the tags and branches:

```bash
for tag in `git for-each-ref refs/tags --format='%(refname)' | cut -d / -f 3`
do
    echo $tag; sleep 3 # sleep to avoid: fatal: Unable to create '.git/index.lock': File exists.
    git checkout "$tag"
    git ls-files > ../keep_files_tag_$tag.txt
    git ls-files >> ../keep_files_all.txt
done
for branch in `git for-each-ref refs/heads --format='%(refname)' | cut -d / -f 3`
do
    echo $branch; sleep 3 # sleep to avoid: fatal: Unable to create '.git/index.lock': File exists.
    git checkout "$branch"
    git ls-files > ../keep_files_branch_$branch.txt
    git ls-files >> ../keep_files_all.txt
done
sort ../keep_files_all.txt | uniq > keep_files_uniqe.txt
```


> [!warning] Check if you there are any file names with non-ASCII characters!
> `ls-files` with scramble them (e.g. f\303\274r instead of für) - you must unscramble them, otherwise they will be deleted in the next step!

Finally delete all other files from the repo:

```bash
git checkout master
git filter-branch --force --index-filter \
    "git rm  --ignore-unmatch --cached -qr . ; \
    cat $PWD/keep_files_uniqe.txt | tr '\n' '\0' | xargs -d '\0' git reset -q \$GIT_COMMIT --" \
    --prune-empty --tag-name-filter cat -- --all
rm -rf .git/refs/original/
git reflog expire --expire=now --all
git gc --prune=now
```


You could also have a look at tools like [bfg](https://rtyley.github.io/bfg-repo-cleaner) or [repocutter](http://www.catb.org/~esr/reposurgeon).

### Upload

Note: Gitlab conveniently shows you the upload URL when you create a new project

	git remote add origin git@gitlab-intern.ait.ac.at:dts/commons/licensemanager.git
	git push origin --all
	git push origin --tags

### Cleanup

Now it's best practice to delete the folder in SVN and note the move to GIT (preferably with the link to the GitLab project) in the commit message.

Also check if you referenced the svn in your build process, e.g. for the [maven release plugin](Continuous Integration and Delivery), and fix this.


### Different approaches: Reposurgeon

Reposurgeon is probably a better options for large repositores where we don't want to take the whole history - we should look into it as soon as the official way is not sufficient anymore.

I experimented with Ariadne a bit, mostly following http://www.catb.org/~esr/reposurgeon/dvcs-migration-guide.html
Unfortunately I got stuck with `repocutter` (the program was just stuck..)

	repocutter expunge '^ariadne-webfrontend' '^ariadne2-etc' '^etc' '^main' '^test-data'
	repocutter pathrename '^new' ''

### Yet another different approach
I (Matthias) had some troubles with the approach given above. StackOverFlow saved my life with the answer to the question posted here: https://stackoverflow.com/questions/35900710/svn-to-git-conversion-correct-remote-ref-must-start-with-refs-error/

## From Mercurial

Example:

	hg clone https://hgweb/hg/projects/bikewave/ bikewave-hg
	git init bikewave-git
	cd bikewave-git/
	hg-fast-export -r ../bikewave-hg
	git push --all --set-upstream https://gitlab-intern.ait.ac.at/dts/bikewave.git
	git push --tags --set-upstream https://gitlab-intern.ait.ac.at/dts/bikewave.git
