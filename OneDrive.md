# Installation

Ubuntu ships https://github.com/abraunegg/onedrive by default:

```bash
sudo apt install onedrive
```

Then run `onedrive` in the terminal, past the authorization URL in a browser, request it, then past the **changed URI** (otherwise there is no response!) and you are authorized. (probably a )

# One-shot synchronization

```bash
onedrive --synchronize
```

# Start service

```bash
systemctl --user stop onedrive.service
```

# Caveats

as of late 2023:

- For Ubuntu only the outdated version is available, which is explicitly disencouraged by the docs. no ppa :/
- Syncing sharepoint libraries (my main use case) seems to be annoyingly complicated - there is a lengthy process involved to add each single library: https://github.com/abraunegg/onedrive/blob/master/docs/SharePoint-Shared-Libraries.md
    - and then they warn about data loss!

... I will avoid using it for now I guess :/