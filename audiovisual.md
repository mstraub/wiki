[TOC]

# Codecs

> as of summer 2022

- Pure open source: AV1 + Opus + Matroska
- Highest compatibility: H.264 + AAC + MP4 (that's what e.g. my Android phone records and what works in most browers)

**Audio**:

[Opus](https://en.wikipedia.org/wiki/Opus_(audio_format)) is open source, outperforms all competitors, and is well supported (except on macOS).
See [Opus' recommended bitrate for different use cases](https://wiki.xiph.org/Opus_Recommended_Settings),
e.g. 128k for high quality stereo music.

Therefore, consider it before using aac, ogg vorbis or mp3.

**Video**:

For most use cases [H.264 (AVC)](https://en.wikipedia.org/wiki/Advanced_Video_Coding) is a sensible choice:
most platforms (e.g. Android, browsers,..) support playback, 
also (open source) software en/decoding works nicely.

The successor [H.265 (HEVC)](https://en.wikipedia.org/wiki/High_Efficiency_Video_Coding)  features better compression and supports resolutions up to 8k.
Encoding software is available as well,
but playback capabilities are not ubiquitous due to licensing/patent issues.
Most importantly: most browsers don't support it.

In the near future the completely (patent) free [AV1](https://de.wikipedia.org/wiki/AOMedia_Video_1) will hopefully be the best choice.
It features compression at least as good as H.265 but is open source.
Unfortunately encoding is still extremely slow (1,5 hours for 3 seconds of video with `ffmpeg` 4.4 on my Thinkpad).
However, apparently the [SVT-AV1](https://gitlab.com/AOMediaCodec/SVT-AV1) encoder should be reasonably fast (and is well supported in `ffmpeg` > 5.0)
Also: the first GPU with a hardware encoder (for realtime encoding!) was released in August 2022 by intel.

**Container**:

Relevant container formats are

- [Matroska (.mkv)](https://en.wikipedia.org/wiki/Matroska): open source, widely used and supported
- [WebM (.webm)](https://en.wikipedia.org/wiki/WebM): open source subset of Matroska, widely supported, developed by Google, can only contain VP8/VP9/AV1/Vorbis/Opus
- [MP4 (.mp4)](https://en.wikipedia.org/wiki/MPEG-4_Part_14): widely used, developed by the MPEG group, use it for H.264/H.265 videos.


# Metadata

To view (and change) various types of metadata for image or video files (e.g. Exif for .jpg or QuickTime Tags for .mp4)
use `exiftool` (or alternatively `exiv2`, but it seems to support less data types).

View metadata

	exiftool image.jpg
	exiftool video.mp4

Adjust all relevant timestamps

    exiftool "-AllDates+=1:42" image.jpg    # add 1 hour 42 minutes

Retrieve GPS locations

	exiftool -n -p '$gpslatitude, $gpslongitude, $gpsdatetime' image.jpg

## Local Time or UTC?

**Warning:** typically only a timestamp and not a timezone is stored in meta data.
If the timestamp is in local time or UTC is difficult to say:

On my Samsung phone:
- .jpg: local time
- .mp4: UTC (as it should be according to the [QuickTime Tags spec](https://exiftool.org/TagNames/QuickTime.html))

However, `exiftool` behaves like this:

> According to the specification, integer-format QuickTime date/time tags should be stored as UTC. Unfortunately, digital cameras often store local time values instead (presumably because they don't know the time zone). For this reason, by default ExifTool does not assume a time zone for these values. However, if the API QuickTimeUTC option is set, then ExifTool will assume these values are properly stored as UTC, and will convert them to local time when extracting. (Thx to https://askubuntu.com/a/989649)

So after checking if your device really stores timestamps in UTC use exiftool as follows:

    exiftool -api QuickTimeUTC video.mp4


## Rename and organize files according to Exif timestamp


Simple rename (does not modify the file):

	exiftool '-FileName<CreateDate' -d '%Y-%m-%d_%H-%M-%S%%-c.%%le' image.jpg
	exiftool -api QuickTimeUTC '-FileName<CreateDate' -d '%Y-%m-%d_%H-%M-%S%%-c.%%le' video.mp4

Organize all files of the current directory into monthly directories:

    exiftool -api QuickTimeUTC '-Directory<CreateDate' -d '%Y-%m' .

Rename and rotate (modifies the file!):

	renrot -n %Y-%m-%d_%H-%M-%S image.jpg


# Media Information

identify is part of `imagemagick` and shows info (e.g. size) about image files

    identify image.jpg

The information-only counterpart to `ffmpeg` that only show details (e.g. codecs) of video file

	ffprobe video.mkv
	
Another tool whose output is much cleaner

    mediainfo video.mkv


# Audio/Video Conversion

## handbrake

[Handbrake](https://handbrake.fr) is a mostly GUI based open source video transcoder but also features a CLI.

Convert a DVD folder (typically containing the subfolders `Video_TS` and `Audio_TS`) to a video file

    HandbrakeCLI -i folder --main-feature -o video.mkv

## ffmpeg

Convert a video (autodetection of formats, default quality)

	ffmpeg -i input.mpeg output.mp4

### [H.264](https://trac.ffmpeg.org/wiki/Encode/H.264)

Convert to **high-quality H.264+AAC+MP4 video** (and copy all metadata)

	ffmpeg -i input.mpeg -c:v libx264 -preset medium -crf 17 -c:a aac -b:a 128k -map_metadata 0 output.mp4

> *CRF* = Constant Rate Factor, where 0 = lossless and 51 = extremely compressed on an exponential scale (default: 23).
> The [manual](https://trac.ffmpeg.org/wiki/Encode/H.264) **recommends values between 17 (very high quality) and 28**.

### [H.265](https://trac.ffmpeg.org/wiki/Encode/H.265)

Convert to **high-quality H.265+AAC+MP4 video** (and copy all metadata)

	ffmpeg -i input.mpeg -c:v libx265 -preset slow -crf 18 -c:a aac -b:a 128k -map_metadata 0 output.mp4

> CRF range is 0-51: 0 = lossless and 51 = extremely compressed on an exponential scale (default: 28).
> Recommendations (on the web) for archive quality are to use CRF 18 together with the slow profile.

### [AV1](https://trac.ffmpeg.org/wiki/Encode/AV1)

Convert to **high-quality AV1+Opus+Matroska video** (and copy all metadata)

	I could not really test this scenario (slightly broken ffmpeg installation)
	#ffmpeg -i input.mpeg -c:v libaom-av1 -crf 30 -b:v 0 -c:a libopus -b:a 128k -map_metadata 0 output.mkv

> CRF range is 0-63: 0 = lossless and 63 = extremely compressed on an exponential scale.

### Batch Conversion

ubuntu skeleton script (adjust the input / output file types and ffmpeg arguments):

```bash
#!/bin/sh
OUTPUT_DIR=./minified
mkdir $OUTPUT_DIR
IFS=$'\n' # handle files with spaces, split only on newlines
for FILE in *.mp4
do
    BASENAME=`basename "$FILE" .mp4`
    ffmpeg -i "$FILE" ... "$OUTPUT_DIR/$BASENAME.mp4"
done
```

> basename is useful if the input format is different

#### Trim at end of audio/video

Unfortunately we must first detect the media duration and then cut the point until the media should be exported. Note the use of `-c copy` to avoid reencoding.

```bash
INPUT_DURATION=$(ffprobe -v error -select_streams a:0 -show_entries stream=duration -of default=noprint_wrappers=1:nokey=1 "${IN_FILE}")
OUTPUT_DURATION=$(bc <<< "$INPUT_DURATION"-"$TRIM_EOF_DURATION")
ffmpeg -i "${IN_FILE}" -map 0 -c copy -t "$OUTPUT_DURATION" "${OUT_FILE}"
```

# Image Conversion

## imagemagick

resize images (on the fly)

	mogrify -resize 1200 -quality 85 *.jpg

crop image files (see http://www.imagemagick.org/script/command-line-processing.php)

	for i in `ls *.png`; do convert $i -crop 900x700+1800+300 $i; done

concatenate multiple pictures into one

    montage * -tile 4x2 -auto-orient -resize 1920x1080 -mode Concatenate  montage.jpg

# Video Editing

I am a happy user of [Kdenlive](https://kdenlive.org)  so far :)