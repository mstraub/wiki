see [Java](Java.md) and [python](python.md) for language-specific tools

https://github.com/grafana/pyroscope looks interesting: continuous profiling for multiple languages!

# Heap Profiler

- [KDE heaptrack](https://github.com/KDE/heaptrack) including its fantastic GUI works well with little CPU and memory overhead
- [valgrind massif](https://valgrind.org/docs/manual/ms-manual.html) is said to be more powerful but also has a larger CPU and memory overhead (I did not test it yet)