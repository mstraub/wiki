> [!tldr] [nginx](https://nginx.org/) ([docs](http://nginx.org/en/docs/)), pronounced *engine x*, is a web server that can also be used as a reverse proxy, load balancer, mail proxy and HTTP cache 

As of 2024 it is a popular choice, more popular than [lighttpd](https://www.lighttpd.net/).

# Installation

```bash
sudo apt install nginx
```

# Config

```
/etc/nginx/nginx.conf # main config file
/etc/nginx/conf.d/ # directory for additional config files (if referenced in nginx.conf)
```

Relevant directive hierarchies:
- http -> server -> location

# Status + Logs

```bash
sudo systemctl status nginx.service
sudo journalctl -xeu nginx.service
```