
- ES5 = ECMAScript 5, a dialect of JavaScript finalized in 2009 that even old browsers can understand
- ES6 = ES2015, a newer version of Javascript and major update to ES5
- ES7 = ES2016, the first of the now yearly updates of ES

Two techniques required for letting modern code (ES6 or newer) run on older browsers (supporting only ES5):
1. Polyfills: fix missing APIs on the browser side; "replicate an API using JavaScript (or Flash or whatever) if the browser doesn’t have it natively"
    - https://polyfill.io is a great service to easily add polyfills
2. Transpiling: convert new JS language constructs to older notation; "convert from one programming language to another"

Check feature availability: http://caniuse.com/

# node.js

The whole JavaScript ecosystem evolves rapidly, and packages for Ubuntu are rather outdated.

Use [nvm](https://github.com/nvm-sh/nvm) (node version manager) to install / update / select different versions of `node`.

```bash
nvm install --lts # install latest LTS version
nvm list          # list all versions
nvm use v18.14.0   # use a specific version
```

# Mapping Frameworks

- Leaflet - old but gold for maps with 2D raster tiles
- OpenLayers - if you need many GIS functions (reprojections,..) .. has 3D capabilities now
- [MapLibre GL JS](https://github.com/maplibre/maplibre-gl-js) - main focus are GPU accelerated vector tiles (fork of Mapbox GL)
- [deck.gl](https://deck.gl/) - aims for GPU accelerated (big) data visualization
- and more: [kepler.gl](https://kepler.gl), CARTO