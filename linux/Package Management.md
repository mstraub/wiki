The Linux way of (un)installing programs.

# Debian (.deb)

Package management for debian is handled by the [Advanced Packaging Tool (APT)](https://debian-handbook.info/browse/stable/sect.apt-get.html)

The configuration files specify (amongst others) which repositories are used for downloading packages:

```bash
/etc/apt/sources.list    # main config file
/etc/apt/sources.list.d  # directory with more configs (mostly for repositories with their own config file)
```

Since ~2014 the standard CLI tool is `apt`, which is a more modern version the older tools `apt-get`, `apt-cache`, etc. Most commands known from the older tools work in `apt`, but most of the times with nicer or more verbose output.

```bash
apt update        # synchronize list of available packages with server
apt search        # search for a package (partial name is OK)
apt install       # install packages (exact name required)
apt upgrade       # upgrade upgradable packages (not if new dependencies are required)
apt full-upgrade  # or `dist-upgrade` upgrade upgradable packages (and install new dependencies if required)
apt list --installed # list installed packages
apt remove        # remove packages (exact name required) but leave config files
apt purge         # remove packages (exact name required) and config files
apt policy        # see source ppa(s) for a package (and if it is installed)
```

However, if you still need the stable lower-level tools they are still available too. This is also relevant for writing scripts, as of 2019 apt tells you `WARNING: apt does not have a stable CLI interface. Use with caution in scripts.`

```bash
dpkg         # low-level tool to handle installations of single packages
apt-get      # medium-level tool to install / remove / update packages and their dependencies (uses dpkg)
apt-mark     # hold / unhold packages (aka apt pinning)
apt-cache    # medium-level tool to query the package cache
```

Finally there are also third party tools that provide similar functionality:

	dselect                     # curses GUI for selecting and installing programs
	aptitude                    # curses GUI and command line tool that unites apt-get, apt-cache and dselect


## Third party PPAs

Add a repository (e.g. deadsnakes)
```bash
sudo add-apt-repository ppa:deadsnakes/ppa
```
Then use `sudo apt udpate / sudo apt install` to update / install its packages.

Cleanly remove a PPA and downgrade packages to the vanilla Ubuntu version (or uninstall them if they are not part of vanilla Ubuntu)
```bash
sudo apt install ppa-purge
sudo ppa-purge ppa:deadsnakes/ppa
```

##  Advanced Usage

Some more advanced usage examples:

```bash
apt-cache depends package   # show dependencies (and suggestions) for a package
dpkg -l                     # list names of all installed packages
dpkg -L package             # list all files (full path) contained in a package
dpkg -s package             # show status of package (e.g. is it installed or not)
dpkg -S file                # show packages that contain this file (matching by name)
```


##### alien

`alien` is a package converter that can convert `.deb` packages into `.rpm` and vice versa.

## Verify Suspect Files / Packages

find package of suspect file

	dpkg -S `<filename>`


basic check:

	debsums `<package_name>`


a little more paranoid check:

	aptitude download `<package_name>`
	ar x `<package.deb>`
	tar xfvz control.tar.gz
	grep $(md5sum `<filename>`) md5sums


# Red Hat (.rpm)

There is only one relevant tool for package managemnt in Red Hat: `rpm`. It has five basic modes:

```bash
rpm -i             # --install
rpm -U             # --upgrade
rpm -e             # --uninstall (erase)
rpm -V             # verify
rpm -q             # --query
```

Flags for (un)installing and upgrading:


	--force            # force install/update, same as --install --force
	--no-deps          # (un)install/update even if dependencies are not satisfied
	--test             # just test and don't make any changes


Flags for querying:


	-a  or  --all      # list all installed packages
	-f  or  --file     # list the package a file belongs to
	-l  or  --list     # list all files in a package
	-i                 # information about an installed package
	-p `<package>`.rpm   # query regarding the specific package-file


##  Advanced Usage

Some more advanced usage examples:

```bash
rpm -qR package             # show dependencies for a package
rpm -qa                     # list names of all installed packages
rpm -qlp package.rpm        # list all files (full path) contained in a package
rpm -qf file                # show packages that contain this file (matching by name)
```

### Extract files from .rpm files

rpm packages can not be unzipped directly, but this can be achieved by using `rpm2cpio` and `cpio`. The following command extracts an rpm into the current directory:

```bash
rpm2cpio package.rpm | cpio -idmv
```

## YUM

The *yellowdog updater, modified* aka `yum` is a package management tool used in many distributions based on .rpm packages. Its usage strongly resembles `aptitude`:

```bash
check-update                # synchronize list of available packages with server
yum search                  # search for a package (partial name is OK)
yum update                  # update all packages
yum install                 # install packages (exact name required)
yum remove                  # remove packages (exact name required) but leave config files
```

### yumdownloader

`yumdownloader` downloads RPMs from Yum repositories. The relevant configuration file is `/etc/yum.conf` and the files in `/etc/yum.repos.d`.

