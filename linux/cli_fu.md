# run specific pytests

    pytest path/to/test_file.py -k matchstring
# print currently used display server

    echo "$XDG_SESSION_TYPE"

# systemd log

```bash
# see free / used storage of logs
sudo systemctl status systemd-journald
# remove old logs
sudo journalctl --vacuum-time=30d
```

# MATSim crazyness

Freespeed statistics

    zcat network.xml.gz | grep freespeed | sed -E 's/.*freespeed="(.*)" cap.*/\1/' | sort | uniq -c

# count log lines for different ForkJoin groups

    grep ForkJoin slurm-145666.out | sed -E 's/.*(ForkJoin.*\]).*(WARN|INFO).*/\1/' | sort | uniq -c
# serve a directory via http

    python -m http.server 8080
# trace system calls

run a command via `strace` to see its system calls and signals - which can be quite useful for crashing / stuck programs!

    strace -f command

# compare directory contents
prints an overview of which files and subdirectories differ:

    diff --brief --recursive dir1 dir2

# find duplicate files

Finds equal files (same size, md5 sum, same bytes)

    fdupes -r directory1 directory2 > duplicatelist.txt

# Recursively find newest files in directory

	find . -type f -print0 | xargs -0 stat --format '%Y :%y %n' | sort -nr | cut -d: -f2- | head

# Get pretty-printed diary JSON from MyTrips csv

	csvcut -d ';' -c 4 route_sets_sp.csv | head | tail -n 1 | csvformat -Q "µ" | sed 's/µ//g' | jq

# jq

MyTrips multi-line parsing

	jq -c '[.[0].latitude, .[0].longitude]' < diary_day_regular_only.csv | sed -e 's#\[##' -e 's#\]##' > home.csv

# nice color prompt (from .bashrc)

	if [ "$color_prompt" = yes ]; then
		#PS1='${debian_chroot:+($debian_chroot)}\[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
		PS1="${debian_chroot:+($debian_chroot)}\[\e[38;5;202m\]\[\e[38;5;245m\]\u\[\e[00m\]@\[\e[38;5;172m\]\h\[\033[41m\]\[\033[97m\]-WAALTeR:PRODUCTION-\[\033[0m\]\[\e[00m\]:\[\e[38;5;5m\]\w\[\e[00m\] "
	else
		PS1='${debian_chroot:+($debian_chroot)}\u@\h:\w\$ '
	fi
	unset color_prompt force_color_prompt


# rsync - filtered copy from remote to local via SSH

	rsync -avr -e "ssh -l mstraub" --exclude 'ITERS' demeter:output/output_* .

# remove lines based on duplicate fields in a CSV file

Sort file and only print lines where the fourth field is not a duplicate
	sort -u -t ',' -k '4,4' file.csv

Same, but do not sort (useful to keep the header as first line)
	awk -F ',' '!seen[$4]++' file.csv

# handle files with spaces in a loop
	IFS=$'\n'
	for file in $(ls); do echo "$file"; done

# copy certain files over ssh

	for folder in `ssh demeter ls -1 -d "output*"`; do mkdir $folder; scp -r "demeter:$folder/*.{png,txt,log}" $folder; done


# reformat xml file

default (2 spaces):

	xmllint --format file.xml

with 1 tabulator

	XMLLINT_INDENT=$'\t' xmllint --format file.xml

# random
check validity of xml file (if a dtd is specified in the xml - e.g. <!DOCTYPE config SYSTEM "http://www.matsim.org/files/dtd/config_v2.dtd"> for MATSim)
	xmllint config_vanilla.xml --noout --valid

# crazy matsim conversion from network.xml to taxis.xml (for each link)
	grep -o "link id=\"[0-9]*" network.xml | sed 's/link id=\"//' | sed 's#\(.*\)#<vehicle id="taxi\1" start_link="\1" t_0="0.0" t_1="86400.0"/>#g' > taxis_random.xml
	shuf taxis_random.xml | head -n 1000 > taxis_1000.xml

# line count of all .java files in all subdirectories
	for dir in `ls -1 -d */`; do count=`find $dir -name "*.java" | xargs wc -l | tail -1 | awk '{$1=$1};1'`; echo "${count} in ${dir}" >> contrib_loc.txt; done;
	sort -t " " -k 1 -n  contrib_loc.txt

# grep unique types from MATSim plan
	grep act plans_0.1pct.xml | grep -o "type=\"[^\"]*\"" | sort | uniq -c


# join two CSV files (ignoring the header)

	join -t ";" -1 1 -2 1 <(tail -n +2 a.csv | sort -t ";" -k 1) <(tail -n +2 b.csv | sort -t ";" -k 1)

or with https://csvkit.readthedocs.io (leaving the attributes as is with no-inference)

	csvjoin --left --no-inference -c columnName first.csv second.csv > joined.csv

# reduce pdf size (compress contained images)
	gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/printer -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf

# find all used modes of transport in matsim plans
	grep "mode" plans_vlbg_10_g.xml | sed -r 's/.*leg mode="(.*)" .*/\1/g' | sort | uniq -c

# tree of directories containing xml files
	tree  -P "*.xml"  --prune

# list intersections
https://stackoverflow.com/questions/2696055/intersection-of-two-lists-in-bash

	comm -12 sortedfileA sortedfileB

# search in selected files
	find . -name '*.js' -exec grep -i 'string to search for' {} \; -print

# serve current directory with python
	python3 -m http.server 8080

# kill hanging SSH connection

type: ~. (press enter first if it does not work)
source: https://unix.stackexchange.com/questions/2919/kill-an-unresponsive-ssh-session-without-closing-the-terminal


# get WAALTeR users that requested routes
	sudo journalctl -u wildfly | grep "Start route calculation" | sed 's/\(.*\)VH.*\(waalter_[0-9]*\).*/\1 \2/' > route_requests.txt


# one-liner bash script for a dummy webserver just printing the requests it receives
	while true; do { echo -e 'HTTP/1.1 666 OK\r\n';  } | nc -l 1500; done

# for longname.zip GTFS files create a directory with longname and copy the GTFS file as gtfs.zip into it
	for f in *.zip; do dirname=${f%.zip}; mkdir $dirname; mv $f $dirname/gtfs.zip; done


# rename files found with 'find'
find . -name "stationflows.csv" -exec bash -c 'rename -n "s/stationflows/static_solution_stationflows/" $1 ' -- {} \;



# json pretty print
	jq . x.json > x_prettyprinted.json
	alias prettyjson='python -m json.tool'



# copy & resume for huge files:
# copies file matrix.csv.gz into current directory
curl -C - -O "file:///home/mstraub/projects/emilia/code/instancegenerator_schachinger/matrix.csv.gz"


# markdown -> html
grip README.md --export README.html
pandoc perceivingspaces_ideas.md -o example13.html


Ports testing

http://superuser.com/questions/621870/test-if-a-port-on-a-remote-system-is-reachable-without-telnet


http://ubuntuhandbook.org/index.php/2014/12/install-linux-kernel-3-18-ubuntu/


# ifup und down (kubuntu 14.04)
    # https://userbase.kde.org/NetworkManagement
    # /etc/NetworkManager/NetworkManager.conf  -> no-auto-default=EC:F4:BB:2E:6E:4B
    sudo ifconfig eth1 down
    sudo ifconfig eth1 up

# sum of a column in a text file
        grep calculated /tmp/calc.txt | cut -d ' ' -f 3 | paste -sd+ | bc
    
# split outputFormat=GML
        csplit --digits=2  --prefix=altroute getAlternativeRoutes "/######/" "{*}"
        sed -i -e '/^######/d' altroute*
    rename 's/(.*)$/$1.gml/' altroute*


# ascent descent files from anita
        sed -i -e 's/oid,id,sum_up,sum_down/oid,roadID,ascent,descent/' -e 's/,/;/g' -e 's/-//g' ascentDescent_rawFromAnita.csv
        cut -d ';' 2-4 file.csv > ascentDescent.csv



# replace nexus url in all pom.xmls
    locate pom.xml | grep "/home/mstraub/projects.*/trunk/pom.xml" | xargs sed -i "s#10.101.21.111:8081/nexus#10.101.21.230:8081/nexus#"


# prepend line number; to each row in a file
```bash
awk '{ print FNR ";" $0 } filename > newfile
```

# clear latex temp files
rm *.aux *.log *.out *.snm *.nav *.toc

# nr of unique links in several trafficstate.xml files
    grep "<linkId>" * | awk 'BEGIN {FS="<|>"} {print $3}' | sort | uniq | wc -l


    grep "model matrix w/ direction H" test_getStaticInstanceWithFakeRequests.txt | awk '/./ {print $8}'

    grep "SEVERE\|WARNING\|ERROR" /opt/jboss/standalone/log/server.log.2012-04-13

    $ sudo apt-get install libmtp9 mtp-tools mtpfs
    $ sudo mkdir /mnt/gnex
    $ sudo chmod 0755 /mnt/gnex
    $ sudo mtpfs -o allow_other /mnt/gnex

# trim whitespace
    echo file | sed 's/^ *//;s/ *$//'

# xml pretty printing on terminal

        xml_pp file.xml
    (install package xml-twig-tools first)

# select columns

    grep modelMatrixSu test_getStaticInstanceWithFakeRequests.txt | awk '/./ {print $4}'
    
# set field separator to semicolon and print second column
    less <file> | awk -F";" '{print $2}'

# split and combine files

split files on all occurences of regex

	csplit filename /regex/ {*}

split file into fixed-sized pieces

	split

combine files

	cat file file > combinedfile


# add-apt-repository hinter firewall

if no route to host:
http://askubuntu.com/questions/23211/how-do-i-add-a-repository-from-behind-a-proxy

# SSH authentication only via public key

http://lani78.wordpress.com/2008/08/08/generate-a-ssh-key-and-disable-password-authentication-on-ubuntu-server/
the local public key in ~/.ssh/id_rsa.pub
must be added to the target server's ~/.ssh/authorized_keys

	ssh-keygen
	ssh-copy-id <target-host>

now disable password authentication on the target-host in /etc/ssh/sshd_config

	PasswordAuthentication no

then restart sshd

# X over SSH

http://www.vanemery.com/Linux/XoverSSH/X-over-SSH2.html

automatically sets up everything - just start an X application (no need to set $DISPLAY)

	ssh -X <IP>


# file permissions

http://www.cyberciti.biz/faq/howto-apply-conditional-recursive-chmod-file-permissions/

# recursive rm

	find . -iname "*.txt" -exec rm '{}' ';'
	cd
	# remove all directories that contain files containing UBAHN
	grep -r  UBAHN * | sed -e 's#/.*##' | xargs rm -r
	grep -r BIKE * | sed -e 's#/.*##' | uniq | wc -l


# search string in java files (in subdirs)

	find . -iname "*.java" | xargs grep "searchString"

# find CSV files bigger than 39 bytes in or below current directory

	find . -name "*.csv" -size +39c

# get info about a file

	file -i <filename>

# gnuplot (plotting a function)

	$ gnuplot

	set xrange[0:10000]
	set xlabel "bla"
	set ylabel "blub"
	f(x)=x*x*52/(1000*60*60)
	plot f(x)

export an image

	set terminal png
	set output "output.png"
	replot

# postgres

	sudo -u postgres psql   # connect to default db as db user (first thing after apt-get install)
	createdb mydb           # create thet new db mydb
	psql -l                 # list all databases
	\d 			# list all databases
	\c database		# connect to database

connect to default DB (create new dbs, users,.. from here)

	psql postgres

create new user (SQL):

	CREATE USER user;
	ALTER USER user WITH PASSWORD 'password';

create new db

	CREATE DATABASE mydb WITH OWNER = owner;

change permissions for db/table

	GRANT ALL ON DATABASE myits TO myits;   --- only changes the db, not the tables
	ALTER DATABASE myits OWNER TO myits;    --- only changes the db, not the tables

rename db (you can only renmae the db you are not currently connected to)

	ALTER DATABASE mydb RENAME TO mydb_new;

change permissions for all dbs/tables recursively

	for tbl in `psql -qAt -c "SELECT tablename FROM pg_tables WHERE schemaname = 'public';" mydb` ; do  psql -c "ALTER TABLE $tbl OWNER TO ownername" mydb ; done

connect to DB and execute command

	psql -U myuser -h localhost -d mydb -c "SELECT * FROM mytable;"

connect to DB and execute script (tip: you can use -h localhost to connect via tcp/ip)

	psql -U myuser -h localhost -d mydb -f script.sql

dump & restore DB

	pg_dump -h hostname -U username --exclude-schema experimental databasename | gzip > "$(date +%Y-%m-%d)_databasename.sql.gz"
	gunzip dump.sql.gz
	psql -f dump.sql -d <dbname> -U <username>
	# when the DB is postgis-enabled importing won't work. the workaround:
	# export only the required tables with -t table1 -t table2 ... and
	# import the dump into an already postgis-enabled db

easy dump/restore (should also work with postgis?)

	pg_dump -Fc dbname > dbname.dmp
	pg_restore -Fc -d dbname dbname.dmp

	-> role / database must exist on the target host! use these statements if you move a db to a new host:
	create database <dbname>;
	create role <rolename>;

get size of DB

	SELECT pg_size_pretty(pg_database_size('geekdb'));

if db on target system does not exist yet, create it

	CREATE DATABASE comover WITH OWNER=owner;


update postgres 8.4 to 9.1 (ubuntu 12.04) - make backups first!!

	sudo /etc/init.d/postgresql stop
	sudo apt-get install postgresql-9.1

	sudo -u postgres pg_dropcluster --stop 9.1 main

	sudo /etc/init.d/postgresql start
	sudo -u postgres pg_upgradecluster 8.4 main

	sudo -u postgres pg_dropcluster 8.4 main
	sudo apt-get purge postgresql-8.4
	sudo apt-get autoremove

create postgis-enabled postgres-db (postgres 8.4)

	sudo su postgres
	createdb postgis_template
	createlang plpgsql postgis_template
	psql -d postgis_template -f /usr/share/postgresql/8.4/contrib/postgis.sql
	psql -d postgis_template -f /usr/share/postgresql/8.4/contrib/spatial_ref_sys.sql

create database from postgis_template

	CREATE DATABASE database WITH TEMPLATE=postgis_template
	CREATE SCHEMA myschema;
	ALTER DATABASE "database" SET search_path=myschema,public;


kick users connected to db

	SELECT usename, procpid FROM pg_stat_activity WHERE datname = current_database();
	SELECT pg_terminate_backend(offending_procpid);
	http://stackoverflow.com/questions/2432555/phppgadmin-how-does-it-kick-users-out-of-postgres-so-it-can-db-drop

	# query to csv
	psql -d dbname -t -A -F"," -c "select * from users" > output.csv
	COPY (SELECT * from users) To '/tmp/output.csv' With CSV;
	other approaches: http://stackoverflow.com/questions/1517635/save-postgres-sql-output-to-csv-file


# add prefix/suffix to filenames

using a for loop and brace extension

	for f in routepart*; do mv $f{,.gml}; done


# add a new sudo user

	sudo adduser mstraub
	sudo usermod -a -G admin mstraub


# delete all .svn folders in a directory and all its subdirectories

	find . -name ".svn" -print0 | xargs -0 rm -r


# find directory in current dir

	ls -d *MyITS*

# recursively list files containing a string

http://www.cyberciti.biz/faq/howto-search-find-file-for-text-string/

	grep -H  "<search term>" * -R | cut -d: -f1 | uniq


# SVN (subversion)

## show ignored files in SVN repo

	svn status -v --no-ignore | grep ^I

## delete unversioned files from SVN

	svn status --no-ignore | grep '^\?' | sed 's/^\?      //'
	svn status --no-ignore | grep '^\?' | sed 's/^\?      //'  | xargs rm -rf

	svn status | grep ^\? | cut -c9- | xargs -d \\n rm -r


## svn properties

	svn propedit svn:ignore .

--> an editor will open, add one line for each file/directory to be ignored (in this directory)

	svn propedit svn:global-ignores .

--> similar to svn:ignore, but also affects all subdirectories

set executable bit for a single FILE

	svn ps svn:executable yes FILE

remove executable bit for a single FILE

	svn propdel svn:executable FILE

set executable bit for several files

	for file in `find . -name FILENAME`; do
		svn ps svn:executable yes ${file}
	done

## svnadmin (partial dump and import of repository)

	svnadmin dump /var/svn/mprandtstetter/ | svndumpfilter exclude router/lib router/evaluation presentations > mprandstetter.svn-dump
	sudo svnadmin load /var/svn/router/ < mprandstetter.svn-dump


## revert to old revision

1. svn merge --dry-run -r:73:68 http://my.repository.com/my/project/trunk
2. svn merge -r:73:68 http://my.repository.com/my/project/trunk
3. svn commit -m "Reverted to revision 68."

http://aralbalkan.com/1381

## create a tag in SVN

	svn cp trunk tags/mytagname
	svn ci

## show changed files in two directory trees without .svn

	diff -rq --exclude=*.svn /home/mstraub/indicator /home/mstraub/indicator_changed
	grep -r --exclude=*.svn* python2.5 *

## set executable flag in svn

	for file in `find . -name nameOfFiles`; do
	svn ps svn:executable yes ${file}
	done


# mount iso file

	mount -o loop -t iso9660 foo.iso /mountpoint



# fun with linux commandline text mashing

remove s.select([ )] markup from qgis select strings

	sed -e 's/^.*\[//g' -e 's/,\])//g' ilos_test_qgis.csv

remove many duplicates (replace commas with newlines and sort)

	awk 'BEGIN {RS=","}; {print}' ilos_test_qgis__clean.csv | sort -u

replace all newlines with commas again

	cat file | tr '\n' ','

remove all " characters in all csv files in all subdirs

	sed -i 's/"//g' */*.csv

replace all , with . in file.csv

	sed -i 's/,/./g' file.csv


# awk - write fourth field of CSV-file in another file

	awk 'BEGIN {FS=";"} {print $4}' /tmp/ilos_test_qgis.csv > wooha


# find classes in JAR files

	find . -name "*.jar" -exec bash -c "echo {} && jar tvf {} | grep ServiceMBeanSupport " \;



# sqlite copy/paste part of a table

copy frc 7 to frc 8

	INSERT INTO timeseries SELECT 8,laneid,interval,speed,stddev,count,smoothed_speed FROM timeseries WHERE roadid=7;


# background / foreground

	app .. start app in foreground (normal thing to do)
	app & .. start app in background
	nohup app .. start app and detach it from console (the console can be closed and the app will still be running)
	ctrl+z .. suspend foreground app
	bg .. let suspended app run in background (like starting it with &)
	fg .. let suspended app run in foreground again


# Change encoding of a file

	iconv -l # lists available encodings
	iconv -f <present encoding> -t <desired encoding> <file>



# Bash Snippets


## Date / Time

### Date loop

	for i in `seq -w 1 31`; do ./myscript 2007-08-$i; done

You can use inner loops for month and year of course.

## File Juggling

### Recursively count number of files in directory

```bash
find DIRECTORY -type f -print | wc -l
```

### Parse a Logfile

```bash
cat $1 | while read line; do
	sectionId=`echo $line | grep -o "[0-9]\{4\}-[0-9]\{4\}"`
	statusString=`echo $line | grep -o  "auf .*$"`

	case $statusString in
		auf\ FREI )
		STATUS=1;;
		auf\ DICHT )
		STATUS=2;;
		auf\ STOCKEND )
		STATUS=3;;
		auf\ STAU )
		STATUS=4;;
		auf\ NICHT\ VERFÜGBAR )
		STATUS=5;;
	esac

	NEWLINE="${line};${sectionId};${STATUS}"
	echo $NEWLINE >> out.txt
done
```

### Make a tar'ed archive from selected files

```bash
# starting from current directory, recursively tar and compress everything NOT ending with .csv
tar cfvz  output.tar.gz * --exclude "*.csv"
```

### Remove empty lines from text


	grep -v '^$' input.txt > output.txt

Or


	sed -i '/^$/d' input.txt


## Remove newlines from text


	tr "\n" " " input.txt



#### Change name for multiple files in a directory

XML taxi files from heedfled have syntax of webservices as name:


- First step:

```bash
rename -n s/.*Start=// FCD*
```

- Second step:

```bash
rename -n s/T.*// 2007*
```
NOTE: remove -n (=no-act) to run the rename

#### Dry run

Run your rename with -n to see which files would be renamed.


## Finding Stuff


### Delete Files Older than 15 days

```bash
find /path/to/dir -mtime +15 -delete
```

### Execute a command on every found file

```bash
find / -name muh.txt  -exec cat '{}' \;
```

Multiple -exec switches can be used to execute more commands on one file:

```bash
find / -name muh.txt -exec echo '{}' \;  -exec cat '{}' \;
```



## PDF

#### Shrink pdf files

Compresses the images in a pdf to a reasonable size (but still useful for printing)

```bash
gs -sDEVICE=pdfwrite -dCompatibilityLevel=1.4 -dPDFSETTINGS=/preprint -dNOPAUSE -dQUIET -dBATCH -sOutputFile=output.pdf input.pdf
```

See also http://www.ghostscript.com/doc/9.05/Ps2pdf.htm#Options

#### Split pdf files

```bash
pdfposter -p 2x1a4 in.pdf out_across_2_A4_pages_sidebyside.pdf
```

see also https://wiki.ubuntuusers.de/pdfposter/


