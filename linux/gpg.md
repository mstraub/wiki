`gpg`, the [GNU Privacy Guard](https://www.gnupg.org/) is an implementation of the OpenPGP standard

# Use Cases

## Encrypt Files

Encrypt `the_file.txt` to `the_file.txt.gpg` with a password you are prompted in a GUI:
```bash
gpg --symmetric --cipher-algo AES256 the_file.txt
```

## Decrypt Files

Decrypt `the_file.txt.gpg` to `stdout`
```bash
gpg --decrypt the_file.txt.gpg
```

Decrypt `the_file.txt.gpg` to `the_file.txt` (by letting `gpg` guess what you want or by specifying it explicitly)
```bash
gpg the_file.txt.gpg
gpg --decrypt --output the_file.txt the_file.txt.gpg
```

> [!NOTE] gpg asks nicely before overwriting existing files.

## Password hints

### Enter password on terminal

Normally `gpg` asks for the password via a popup.
This can be impractical, especially on remote machines.
The solution:
```bash
export GPG_TTY=$(tty)
```

### Clear password cache

`gpg` will remember passwords for a while.
In case of a stale password in the cache stop the `gpg` agent to clear the cached password:
```bash
pkill -SIGHUP gpg-agent
```

## Renew an expired key

https://gist.github.com/krisleech/760213ed287ea9da85521c7c9aac1df0