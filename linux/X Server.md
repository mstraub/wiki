# X Server

The X Window System (X11) is a windowing system that handles **user data input** (e.g. keyboard) and **graphical output**. It uses a client server model, where one server can handle several clients.

A complete graphical environment consists of four layers:
 1.  Desktop Environment Manager: collection of programs designed for a good user experience, look&feel,.. (KDE, Gnome,..)
 2.  Window Manager: draw and manager windows (KWin, Metaciy, Enlightenment,..)
 3.  Display Manager: handle user login and start window manager (lightdm, kdm, gdm, xdm,..)
 4.  Display Server: interface to hardware (X-Server, wayland,..)

X can be started with the command `X`.


## Configuration with xorg.conf

The central configuration file to configure all aspects of X11 is `/etc/X11/xorg.conf`, although in 2015 this file is seldomly used. Instead auto-configuration (and hotplugging) through `xrandr`, HAL, udev,... is used.

The file is structured in sections, the most important being:

- Monitor: properties of physical monitors (e.g. monitor modelines)
- Screen: configuration of screens (that may span several monitors)
- Device: configuration of video card
- Files: locations of fonts (FontPath) or modules (ModulePath)
- InputDevice: configuration of keyboard, mouse, touchpad,..

## Parameters for Starting Programs

### geometry

The parameter `-geometry` defines width, height, x offset (from left side) and y offset (from top) in that order. The unit for width and height depend on the program (e.g. pixels or characters)

```bash
xterm -geometry 120x20+100+100  # 120 characters wide, 20 lines tall, 100 pixels offset to top left corner
xterm -geometry 120x20-0-0      # same as above, but in the right lower corner of the screen
```

### display

With `-display` the display where the started program should be displayed is chosen. Because of the client server architecture of X it is possible to run a program one computer and display it on another.

The syntax is: `hostname:XServer.Display`, but hostname and display can be omitted.

```bash
xeyes -display :0             # display xeyes on the first display of the first local X-Server
xeyes -display remotehost:1:2 # display xeyes on the third display of the second X-Server on remotehost
```

The default display is stored in the environment variable `DISPLAY`.

```bash
export DISPLAY=":0"
```

## X Over Network

### Display on Remote Host

As mentioned before the `DISPLAY` variable can contain other hosts - meaning the window will be shown on a different host.

To configure an X-Server to receive connections via TCP/IP:

- X must not be started with `-nolisten tcp` (as is the case in e.g. Ubuntu: see `/etc/X11/xinit/xserverrc`)
- the display manager must be configured accordingly (e.g. lightdm by default disables tcp listening, it must be turned on by adding `xserver-allow-tcp=true` in `/etc/lightdm/lightdm.conf`
- the remote host must have been whitelisted with `xhost`

```bash
xhost +             # allow connections from everywhere
xhost -             # disallow connections from non-listed IPs
xhost +localhost    # allow TCP/IP connections from localhost
xhost -localhost    # disallow TCP/IP connections from localhost
xhost               # show current status
```

If in doubt check if X11 is listening on port 6000 (`nmap -p 6000 localhost`)

### X over SSH

When connected to a remote machine over SSH it is often useful to redirect the graphical output to the local machine. This can be done with the parameter `-X`.

```bash
ssh -X remotehost
```

However, the remote host must allow this: `X11Forwarding yes` must specified in `/etc/ssh/sshd_config`. [(more explanations)](http://unix.stackexchange.com/a/12772)

### Terminal Server

The X display manager control protocol (XDMCP) is used by display managers (e.g. xdm) to look for X servers where the client can log in. However XDMCP is insecure (no encryption) and slow (no compression) - alternatives are recommended. ([Ubuntu documentation](https://wiki.ubuntu.com/xdmcp))


## Fonts

On Linux systems fonts typically can be found in `/usr/share/fonts` or `~/.fonts`.
In `/etc/fonts/fonts.conf` these locations are defined.

Fonts must reside in one of the font directories and can only be used after `mkfontdir` and `mkfontscale` have been used on the respective directory. These tools create the helper files `fonts.dir` and `fonts.scale`.

Another option to build the cache is `fc-cache -v -f`


### X-Font-Server

With the X-Font-Server `xfs` a central server can be used to maintain a collection of fonts served to its clients. This server can be configured in `/etc/X11/xfs.conf`.

Clients must add an entry for the server into `/etc/X11/xorg.conf`:


	Section "Files"
	    FontPath "tcp/192.168.0.2:7100"
	EndSection


## Accessibility

To provide better accessibility for the physically disabled you can use tools like on-screen keyboards, text to speech (e.g. [Orca](https://wiki.gnome.org/Projects/Orca), emacspeak), high contrast desktop themes with large fonts, magnifiers, output for Braille displays, or adapted keyboard sensitivity.

Most of the use cases are covered by various programs of your favourite desktop environment, but for adapting the keyboard we can use core functionality of X: AccessX.

With `xkbset` keyboard-related features can be turned off (with `-`) or on (without `-`):

```bash
xkbset -repeatkeys        # pressing a key once (no matter how long) counts as one key press
xkbset slowkeys 500       # a key counts as pressed after 500ms
xkbset bouncekeys 500     # pressing a key twice within 500ms only counts as one key press
```

## Tools

Some useful tools in an X environment:

```bash
xwininfo         # prints details about a program after klicking it (or supplying its id)
xkill            # kills a program after klicking its window (or supplying its id)
xdpyinfo         # prints information about the X server
xrandr           # configure the X-server on the fly
```



