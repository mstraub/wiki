.. that don't fit in any other category

# FreeRDP

> [!tldr] A Remote Desktop Protocol Implementation

To connect to my Windows VM I use:
```bash

xfreerdp /v:$HOST /d:d01 /u:USER /p:PASSWORD /size:1920x1018 /dynamic-resolution /kbd:0x00000407 /drive:home,/home/mstraub /t:HOST +sound +clipboard +fonts +grab-keyboard
```

Note that `Ctrl+Alt+Enter` toggles fullscreen mode