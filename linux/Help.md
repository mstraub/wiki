Apart from the tools mentioned here check out [official documentation of the GNU Coreutils](https://www.gnu.org/software/coreutils/manual/html_node/index.html)

# tldr

*too long, didn't read* is a crowdsourced quickstart
for everybody that does not want to deep-dive into man pages.
It shows the most frequently used invocations instead of a lot of text :)

```bash
tldr
```

# man, whatis, apropos

[man](http://www.mcmcse.com/linux/man_pages.shtml) shows you the man(ual) page for nearly every Linux program but also config files or system calls. Man pages are divided in sections, the most important ones are programs (1), config files (5) and sysadmin tools (8).

Man pages are preprocessed with the program `groff` and displayed using the program `less`. Use the arrow keys to navigate, type `/searchphrase` and enter to search, and type `q` to quit.

```bash
man man        # view man page of man itself
man passwd     # view man page of programm passwd (section 1)
man 5 passwd   # view man page of /etc/passwd (section 5)
```

`whatis` shows shows man pages whose names contain your search phrase.

```bash
whatis
man -f  # equivalent to whatis
man -aw # show full path to man file instead of name only
```

`apropos` shows man pages relevant to a topic by searching through both the "Name" and "Description" sections of the man page database.

```bash
apropos
man -k
```

# info

`info` shows info page for many GNU programs. Sometimes `info` gives more in-depth information than `man` (e.g. info ls). Type `h` in `info` to get a help on how to navigate with the keyboard.

```bash
info
```

# type

`type` is a builtin command of bash, that shows the type of a command, e.g. binary, script, alias, builtin command,..
Depending on the result you must use the respective help system.

```bash
type ls        # show type of ls
type -a kill   # show all commands with the name kill (=one program & one builtin!)
```

# help

`help` is a bash builtin that gives you help for bash builtins (try `help help`).

```bash
help           # list all builtin bash commands
help type      # get help for builtin command 'type'
```

# whereis, which

`whereis` shows the paths of a program, its config files and man pages.
`which` only shows the full path of a program

```bash
whereis ls
which ls
```
