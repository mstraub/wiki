# Scheduling Execution of Jobs

## One-Shot Jobs

`at` and `batch` schedule the one-time execution of a command at a given time (`at`) or when the system load is low enough (`batch`). The job is read from stdin (or a file provided with -f).

```bash
at `<time>` (`<day>`)  # interactively schedule execution for a given time
batch              # interactively schedule execution when system load allows (same as at -b)
atq                # view queue of user (root sees all jobs)
atrm `<jobid>`       # remove scheduled job
```

##### Schedule a job

Start `at` with a time (and optional day) and interactively type commands (may span several lines) and then Ctrl+d when finished.

Various time and date formats can be used, e.g.

```bash
at 18:10 2014-10-13
at midnight
```

To see the contents of a job, which are stored in `/var/spool/cron/atjobs`, use

```bash
at -c `<jobid>`
```

##### Permissions

With `/etc/at.allow` and `/etc/at.deny` you can configure who is allowed to schedule jobs. Add one user per line (without whitespace).

## Regular Jobs (Cron Jobs)

`cron` is a deamon used to schedule the regular execution of programs / scripts / commands. A scheduled task is typically called (cron) job.
The maximum time resolution is minutes, a job can therefore be executed at maximum once a minute.

> [!warning] note, that cron is not GUI aware and simply calling e.g. `notify-send` or `sm` to show a GUI warning won't work (without a bit of [trickery](https://askubuntu.com/questions/298608/notify-send-doesnt-work-from-crontab))

Cron jobs can be configured in two ways:

### (1) Jobs in /etc/cron.*

The system-wide approach is to put an executable (script) in one of these self-explanatory directories:

```bash
/etc/cron.hourly
/etc/cron.daily
/etc/cron.weekly
/etc/cron.monthly
```

### (2) Jobs in /etc/crontab

The second way is via crontrab-entries into the global crontab file `/etc/crontab` with the following format (must end with a newline!):


	  0   2   12  *   *   user /usr/bin/false
	# ┬   ┬   ┬   ┬   ┬   ┬    ┬
	# │   │   │   │   |   │    └ command
	# │   │   │   │   |   └───── user executing the command
	# │   │   │   │   └───────── day of week (0 - 7) (0 to 7 are Sunday to Sunday, or use names such as MON-FRI)
	# │   │   │   └───────────── month (1 - 12) (or names such as JAN-DEC)
	# │   │   └───────────────── day of month (1 - 31)
	# │   └───────────────────── hour (0 - 23)
	# └───────────────────────── min (0 - 59)


[nifty online editor](https://crontab.guru)


#### More crontab

Also possible, but not encouraged in order to not put cron jobs into too many different places and overcomplicate administration, are the following options:

Put files in the same format as `/etc/crontab` into `/etc/cron.d`.

Or use the program `crontab`, which edits a special crontab file for each user and validates the file at each save. The cron job will be executed by the respective user (with the according rights).

```bash
crontab -l     # view (list) crontab file for current user
crontab -e     # edit crontab file  for current user
crontab -r     # remove crontab file for current user
```

The crontab files created by the command `crontab` for each user reside in `/var/spool/cron/crontabs` (and omit the user-column!).

##### Intervals, Repetitions,...

Minutes, days,... can be specified as follows:

| format               | example                                               | further description                 |
| -------------------- | ----------------------------------------------------- | ----------------------------------- |
| single value         | 1                                                     |                                     |
| comma-separated list | 0,3,6                                                 |                                     |
| dash-separated range | 0-3                                                   |                                     |
| */`<nr>`             | */5 (e.g. every 5 minutes starting at 0: 0,5,10,...)  | every `<nr>` beginning at 0         |
| `<start>`/`<nr>`     | 2/5 ( e.g. every 5 minutes starting at 2: 2,7,12,...) | every `<nr>` beginning at `<start>` |

**Gotcha:** when specifying e.g. 2/5 in minutes and the system boots at 18:03 the first job is executed at 19:03 because 18:02 was missed!

##### Special Strings

The following special strings can be used instead of the first five fields of a row:

| string             | replaces  | meaning                                       |
| ------------------ | --------- | --------------------------------------------- |
|                    |           |                                               |
| @reboot            | -         | run once, at startup                          |
| @yearly, @annually | 0 0 1 1 * | run every first minute of a year              |
| @monthly           | 0 0 1 * * | run every first minute of a month             |
| @weekly            | 0 0 * * 0 | run every first minute of a week (on Sunday!) |
| @daily, @midnight  | 0 0 * * * | run every first minute of a day               |
| @hourly            | 0 * * * * | run every first minute of an hour             |

##### Permissions

To configure which user can use `crontab` add respective entries (one user name per line) to

```bash
/etc/cron.allow # takes precedence
/etc/cron.deny
```

##### Further Resources

[Ubuntu Tutorial](https://help.ubuntu.com/community/CronHowto), [In-depth explanations](http://www.pantz.org/software/cron/croninfo.html)
### Disable a cron job

To disable a cron job in `/etc/cron.*` either remove the executable flag from the script / program (`chmod -x script`) or move the script out of the folder.

Jobs in crontab are disabled by prepending the line with a hash (#).

### Gotchas

Cron passes only a minimal set of environment variables to your jobs. E.g. the `PATH` may be different than what you expect, things set in `.bashrc` may not be present,...

`[More info.](http://askubuntu.com/questions/23009/reasons-why-crontab-does-not-work)` `[Ubuntu CronHowTo](https://help.ubuntu.com/community/CronHowto)`

### Anachron

For computers not running 24/7 or in cases when missed jobs due to downtime must be run as soon as the server is up again use `anacron`.

Jobs for anacron are specified in `/etc/anacrontab`, which only root can edit (there is no equivalent to the command `crontab` usable by every user as for regular cron jobs)


	# environment variables
	SHELL=/bin/sh
	PATH=/sbin:/bin:/usr/sbin:/usr/bin
	RANDOM_DELAY=30
	START_HOURS_RANGE=3-22

	# jobs
	  1   15  backup  /usr/local/bin/my_backup.sh
	# ┬   ┬   ┬       ┬
	# │   │   │       └── command (may span several lines if line is terminated with '\'
	# │   │   └────────── job-identifier string (should be unique)
	# │   └────────────── delay in minutes (e.g. after system startup)
	# └────────────────── period in days (>=1) or @period_name (e.g. @daily,..)


Two environment variables can be used by putting them before the job definition in `/etc/anacrontab`:

- START_HOURS_RANGE=3-22 (job may be run between 03:00 and 22:00)
- RANDOM_DELAY=30 (a random delay up to 30 added to 'delay in minutes')

The timestamp (YYYYMMDD) of the last execution for each job is stored in `/var/spool/anacron/`<job-identifier>`.

