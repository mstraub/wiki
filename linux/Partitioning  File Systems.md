# Partitioning & File Systems

In order to use a hard disk to install an operating system or store files it has to be

1. [partitioned](http://www.tldp.org/HOWTO/Partition/intro.html#explanation) and
2. each partition must have a [file system](http://tldp.org/LDP/intro-linux/html/sect_03_01.html)

A special case are [swap partitions](http://www.tldp.org/HOWTO/Partition/setting_up_swap.html) (or files), which is basically very very slow RAM (memory) on the hard disk.

## Filesystem Hierarchy Standard (FHS)

The [Filesystem Hierarchy Standard (FHS)](http://www.pathname.com/fhs/pub/fhs-2.3.html) defines a common directory structure for Unix based OSes, which is important to be kept in mind during partitioning.

FIXME outline the basics:

- /bin, /etc, /dev, /lib, /proc, /sbin: must be contained in one partition (used as root)
- /boot, /home, /opt, /root, /tmp, /usr, /usr/local, /var: can be on different partitions

## Creating Partitions & Filesystems

##### Partitioning

The programs `fdisk` and `cfdisk` can create a / alter the partition table in PCDOS format for disks up to 2TB.

Common partition IDs are: 5=extended, 7=ntfs, 82=swap, 83=linux.

```bash
fdisk /dev/sda                  # start partitioning of /dev/sda
fdisk -l /dev/sda               # list all partitions of /dev/sda
cfdisk /dev/sda                 # use a curses GUI for paritioning
```

For disks bigger than 2TB typically GPT - the [GUID_Partition_Table](http://en.wikipedia.org/wiki/GUID_Partition_Table) - is used, which can be created with GNU `parted`. Read [this good tutorial about disks with 4096 byte sectors](http://www.ibm.com/developerworks/linux/library/l-4kb-sector-disks/)

##### Create a file system

```bash
mke2fs      # create an ext2/3/4 file system
mkfs        # create a file system (ext and non-ext file systems)
mkfs.ext4   # mkfs.* are frontends that call the relevant programs to create a specific file system
```

The following two commands can be used to create an `ext4` file system.
The parameter `-n` can be used to simulate the action without actually changing anything.

```bash
mke2fs -t ext4 /dev/sda1
mkfs.ext4 /dev/sda1
```

##### tune2fs - adjusting filesystem parameters for ext2/3/4

```bash
tune2fs -l `<device>`             # show stats & current value of options
tune2fs -L `<name>` `<device>`      # set volume name
tune2fs -i `<interval>` `<device>`  # interval between checks n[d|w|m], by default in days, e.g. 6m
tune2fs -c `<count>` `<device>`     # maximum mount-count between checks
```


## Mounting

The `findmnt` command and various files can be used to inquire the currently mounted devices and swap spaces.

```bash
lsblk                  # list block devices (i.e. no network or virtual devices)
findmnt                # show all currently mounted devices
findmnt -t ext4,cifs   # show mounted devices with a certain file system type
mount                  # show all currently mounted devices (deprecated!)
cat /etc/mtab          # ..
cat /proc/mounts       # ..
cat /proc/swaps        # show currently used swap spaces
```

The main configuration file is `/etc/fstab`:

```bash
cat /etc/fstab         # configuration of filesystems to mount (specifies location, file system type,..)
mount -a               # mount all devices in /etc/fstab except the ones set to 'noauto'
umount -a              # unmount all devices in /etc/fstab except the ones set to 'noauto'
```

The previously mentioned invocations and config files cover most of the daily use cases. More advanced use cases follow.

##### Manually mount a CDROM

```bash
mount -t iso9660 /dev/cdrom /mnt/cdrom
```

##### Manually mount an ISO file

```bash
mount -o loop -t iso9660 foo.iso /mnt/mountpoint
```

##### Manually mount a partition

Mount an ext4 partition with some restrictive mount options. See FILESYSTEM INDEPENDENT MOUNT OPTIONS in `man mount` for an explanation of all mount options.

```bash
mount -t ext4 -o nodev,noexec,noatime,nosuid /dev/sda1 /mnt/mountpoint
```

##### Remount a partition read-only

This is e.g. useful for using `fsck` on a partition. Note, that this overwrites all mount options if the partition has been mounted manually - if there is an entry in `/etc/fstab`, ro will be added to the mount options specified there.

```bash
mount -o remount,ro /dev/sda1 /mnt/mountpoint
```

##### Mount certain file systems only

When combining `-a` with  `-t` all file systems of a certain type in `/etc/fstab` can be selected (except if they are noauto of course). In this example we force (-f) unmount all cifs filesystems (Windows / Samba shares) and then mount them again.

```bash
umount -a -f -t cifs
mount -a -t cifs
```


## Check & Monitor File Systems

The family of `[fsck.*](http://www.thegeekstuff.com/2012/08/fsck-command-examples)` (e.g. `fsck.ext4`) commands is used to check & monitor filesystems.

**Note:** file systems must be unmounted or at least mounted read-only, because `fsck` does not only check but also repairs the file system! Repairing a mounted file system can lead to severe problems.

```bash
fsck -N `<device>`                # dry run (only shows you what would be done) on a device, e.g. /dev/sda1
fsck -ARM                       # check all file systems in /etc/fstab, except root, and except all mounted ones
fsck -t ext4 -p `<device>`        # check a device with an ext4 filesystem and repair errors automatically (p=preen); same as fsck.ext4 -p
```

These two tools can be used to further inspect your ext file systems, but be careful with `debufs`!

```bash
debugfs `<device>`                # ext2/ext3/ext4 interactive file system debugger (use with care!)
dumpe2fs `<device>`               # dump ext2/ext3/ext4 filesystem information (super block & group information of blocks)
```


## Disk Health

### badblocks

Badblocks is a utility to check disk health with either the non-destructive read-mode or the destructive (all data will be erased) write-mode.

```bash
sudo badblocks -wvs /dev/sdb
```

### S.M.A.R.T. Monitoring

With `smartctl` (contained in the package `smartmontools`) you can check the health status of a hard disk.

```bash
sudo smartctl /dev/sda -H     # quick summary to see if HDD is OK or not
sudo smartctl /dev/sda -a     # print all SMART information
```

When printing all information the attributes are shown. VALUE and WORST should be **above THRESH** for the disk to be OK.


	Vendor Specific SMART Attributes with Thresholds:
	ID# ATTRIBUTE_NAME          FLAG     VALUE WORST THRESH TYPE      UPDATED  WHEN_FAILED RAW_VALUE
	  1 Raw_Read_Error_Rate     0x002f   200   200   051    Pre-fail  Always       -       1305
	  3 Spin_Up_Time            0x0027   141   141   021    Pre-fail  Always       -       3916
	  ...

## Swap

```bash
swapon -s      # see currently active swap partitions
```

```bash
mkswap         # set up swap (on a device or in a file, which must be created with dd)
```

##### (De)activate swap

For 'mounting' swap partitions or files extra commands are used:

```bash
swapon -a      # activate all swap partitions in /etc/fstab (except those with noauto set)
swapoff -a     # deactivate ..
```

Hint: use `swapoff` to move all memory back into RAM and to get a responsive system quickly (after the process that caused swapping is closed).


##### How to use a swap file

Create a 64M swap file in the current directory & check that it's activated

```bash
dd if=/dev/zero of=swapfile bs=1024 count=65536
chmod 600 swapfile
mkswap swapfile
swapon swapfile
cat /etc/swaps
```


