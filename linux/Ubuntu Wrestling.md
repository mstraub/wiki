Notes on installing \*buntu on various machines and how to fix annoyances.

[TOC]

# General

## Microsoft Teams

Either install Chromium with `snap install chromium` and use it as a progressive web app.
Or: use the great (but unofficial) `snap install teams-for-linux` which wraps the PWA.

## KDE

### Force default KDE titlebar

Annoying apps hide the default titlebar and have different locations for the x button etc. Let's rectify this:

Go to `System Settings > Window Management > Window Rules` and add something like this:

![](kde-window-rules.png)

Use `Detect Window Properties` to find out the "Window class".

### Hanging Shell

Restart a hanging plasma shell in KDE 5 - this will keep all windows / apps opened!
```bash
kstart plasmashell --replace
```

Before Plasma 5.13:
```bash
kquitapp5 plasmashell
# sudo killall plasmahell # (if the above did not help)
kstart plasmashell
```

[reddit](https://www.reddit.com/r/kde/comments/a5d2ly/how_do_you_properly_restart_kwin_and_plasmashell/)

## Kubuntu Menu

Instead of using kmenueditor a quick way to add new entries to the start menu is by creating a `.desktop` file here:

	~/.local/share/applications/


E.g. `anaconda-navigator.desktop`:

```
[Desktop Entry]
Name=Anaconda
Comment=anaconda-navigator
Exec=/usr/bin/env PATH=/home/mstraub/anaconda3/bin/:$PATH /home/mstraub/anaconda3/bin/anaconda-navigator
Icon=/home/mstraub/anaconda3/lib/python3.7/site-packages/anaconda_navigator/static/images/logos/continuum_twitter.jpg
Terminal=false
Type=Application
StartupNotify=true
```

# Kubuntu 24.04 @ Dell Latitude 5410

Dist-upgrade went well. Only needed to remove the snap versions of Thunderbird and Firefox and replace them with the ones from the Mozilla PPA.

```
sudo add-apt-repository ppa:mozillateam/ppa

echo '
Package: *
Pin: release o=LP-PPA-mozillateam
Pin-Priority: 1001
' | sudo tee /etc/apt/preferences.d/mozilla
```

## Bugs

Annoying problem that sometimes after opening an app a screen gets black (but I can still move the mouse cursor there).
Fixed with either changing the display settings or switching to a terminal session and back to the UI session: `Ctrl+Alt+F3`+`Ctrl+Alt+F2`

# Kubuntu 22.04 @ Dell Latitude 5410

Dist-upgrade worked well.

Disable touchscreen
```bash
xinput
  ⎡ Virtual core pointer                          id=2    [master pointer  (3)]  
  ⎜   ↳ MELF0410:00 1FD2:8002                     id=11   [slave  pointer  (2)]
xinput disable 11
```

# Kubuntu 20.04 @ Dell Latitude 5410

Install stuff from Ubuntu repos:

    sudo apt install htop ncdu ranger tldr tree vim
    sudo apt install cifs-utils
    sudo apt install yakuake gimp kubuntu-restricted-extras
    sudo apt install maven gnupg2 openjdk-11-jdk git-cola meld postgresql

Then install:
- QGIS (ppa)
- AppImageLauncher (ppa) https://github.com/TheAssassin/AppImageLauncher
- Nextcloud (ppa)
- Signal (ppa)
- Java WebStart (https://openwebstart.com) .. to easily run JOSM
- Anaconda (this includes spyder)
- dolphin meld service menu: https://github.com/ivavis/dolphin-meld-service-menu/releases

## Bugs

### Random Reboots / Crashes

November 21: X server sometimes crashes (I am dropped to the login screen)

bisher nur im IDLE-Zustand.
- 2.3.21: Laptop reagiert nicht, alles schwarz, nach oftmaligem Drücken der Powertaste schaltet er sich wieder ein
- 3.3.21: Laptop hat neugestartet, wartet auf PIN zur Festplattenentschlüsselung

### Laptop screen not turned off when lid is closed

a forum post suggested to turn off auto-login.. yes now it works.

### Task Manager

`Show only tasks from the current screen` is broken in many cases.
E.g. when closing the laptop lid the remaining screen shows no open tasks at all :/

This bug is fixed but not backported https://bugs.kde.org/show_bug.cgi?id=414743 :(

### Global Shortcuts

Global Shortcuts not working: https://bugs.kde.org/show_bug.cgi?id=421329

For some shortcuts it worked to add them in system settings > global shortcuts instead of kmenueditor. but for speedcrunch it didn't work anymore.

Hm.. seems like I need to log out and in for the shortcuts to work, even when setting them in the global shortcuts.

### Applications start on wrong screen

Setup: left=laptop, right=monitor (primary)

Applications tend to start on the left screen, even if they have previously been started and manually moved to the right sreen and then closed.

https://forum.kde.org/viewtopic.php?f=111&t=166975

### Audio

When plugging in headphones (without mic) the kde audio settings switch to the non-existing headphone-mic automatically.

I can manually change to the internal mic - this works. But it's tedious.

https://bugs.launchpad.net/ubuntu/+source/pulseaudio/+bug/1876056
https://pointieststick.com/2020/06/16/laptop-update-3-fixed-the-audio-jack-input-source/

**Pulseaudio commands**

change output: speakers / headphones

    pacmd list-sinks
    pacmd set-sink-port 0 analog-output-speaker
    pacmd set-sink-port 0 analog-output-headphones

set input to internal mic

    pacmd list-sources
    pacmd set-source-port 1 analog-input-internal-mic
    
# Ubuntu 24.04 @ Lenovo Thinkpad T450s

> [!NOTE] my experiences after upgrading from Ubuntu 22.04

- use mozilla deb package for thunderbird: https://askubuntu.com/questions/1513445/how-to-install-latest-version-of-thunderbird-as-a-traditional-deb-package-withou (since snap was automatically installed and thunderbird replaced :/)


# Ubuntu 22.04 @ Lenovo Thinkpad T450s

> [!NOTE] my experiences (and setup) with GNOME-based vanilla Ubuntu

## Wifi does not work out of the box

apply the same fix as for Ubuntu 20.04

## retain configs and data
- ~/.local/bin (at least my custom scripts)
- ~/.config/systemd/user (selected files)
- ~/.config/JOSM
- ~/.gnupg
- ~/.ssh
- ~/.mozilla
- ~/.thunderbird
- ~/.gitconfig
- ~/.scummvm
- ~/.quakespasm
- ~/.openra

## remove
- [snapd](https://haydenjames.io/remove-snap-ubuntu-22-04-lts/) (and all snaps)
- gnome-calculator
- gedit
- totem

## install
```bash
sudo apt install vim htop ncdu ranger tree plocate curl fonts-lato
sudo apt install ipython3
sudo apt install meld gimp celluloid evolution speedcrunch gnome-text-editor
sudo apt install ubuntu-restricted-extras gnome-tweaks gdebi gnome-shell-extensions dconf-editor
```

then install:
- firefox (through mozilla ppa so we can get rid of snap)
- nextcloud (ppa)
- signal (official ppa)
- qgis (ppa)
- openwebstart (.deb from website)
- vscode (.deb from website, autoconfigures a ppa)
- [gnome extensions, e.g. todo.txt](https://linuxconfig.org/top-10-best-gnome-extensions-for-ubuntu-22-04-desktop)


flatpak support:
```bash
sudo apt install flatpak
# skip the snap plugin, otherwise snapd is installed again!
sudo apt install gnome-software gnome-software-plugin-flatpak gnome-software-plugin-snap-
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak install flatseal # gui for handling program permissions
```

How to use flatpak:
```bash
flatpak search <programname>
flatpak install <program name or id>
flatpak list
flatpak run <program id>
```


## configure
- disable system sounds but not volume up/down pop:
  dconf-editor - org > gnome > desktop > sound: disable `event sounds`
- posteo events in gnome `calendar`: install evolution, add caldav calendar, then they show up and work in gnome calendar as well!
- edit gnome shell extensions via firefox @ https://extensions.gnome.org (after installing the firefox addon **GNOME Shell integration**)

## annoyances
- totem could not play mp4 files even with restricted extras (replaced it with celluloid)
- gnome-text-editor has lacking .md syntax highlighting
- nautilus meld integration not possible (or very hacky)
- nautilus keine automatische suche (type-ahead: https://gitlab.gnome.org/GNOME/nautilus/-/issues/1157)
- foto viewer does not even allow cropping etc (like gwenview)
- frequent crashes of ubuntu-base applications
- blank screen: wiggle of windows when waking up, takes a few seconds (was instant with ubuntu mate)
- could not find a way to force window decorations for annoying apps that don't have one (e.g. obsidian.md, ms teams)
- terminal color scheme by default unreadable.. dark blue on black :/
- notifications tend to stay around
	- even after installing an extension to configure the timeout
	- even in do not disturb mode the low battery warning pops up and never times out
- alt-tab focus troubles with JOSM (does not get focus immediately)

## works like a charm
- image transfer from phone (via mtp) - fast and responsive
- bluetooth connection to external speakers
- todo.txt integration
- posteo calendar integration for the calendar widget (via evolution)
- general look and feel


## Fixing the brittle Nextcloud client

The Linux nextcloud client is not great.. since months syncing stops when resuming the laptop from sleep. [bug report](https://github.com/nextcloud/desktop/issues/4331)
Only stopping and starting the client helps, it does not recover over time bit shows the red x symbol until the end of time (or a restart)

Therefore I needed a workaround to automatically restart it after resuming the laptop from sleep:
**make nextcloud a systemd user service** and **restart that after every sleep/hibernate** ([inspired by this blog post](https://aiguofer.medium.com/systemd-sleep-target-for-user-level-10eb003b3bfd))


`~/.config/systemd/user/nextcloud.service`
```
[Unit]
Description=nextcloud
After=default.target

[Service]
ExecStart=/usr/bin/nextcloud
LimitNOFILE=65535:65535

[Install]
WantedBy=default.target
```

`~/.config/systemd/user/nextcloud_restart.service`
```
[Unit]
After=sleep.target

[Service]
Type=oneshot
ExecStart=/bin/bash -c nextcloud_restart

[Install]
WantedBy=sleep.target
```

`~/.local/bin/nextcloud_restart`
```bash
#! /bin/bash
# wait a few seconds until wifi connection is up..
# that's also the reason why we need this extra shell script
sleep 10
systemctl --user restart nextcloud
```

`~/.config/systemd/user/watch_sleep.service`
```
[Unit]
Description=watch for sleep signal to start sleep.target

[Service]
ExecStart=/bin/bash -c watch_sleep
Restart=on-failure

[Install]
WantedBy=default.target
```

`~/.local/bin/watch_sleep`
```bash
#!/bin/bash
dbus-monitor --system "type='signal',interface='org.freedesktop.login1.Manager',member=PrepareForSleep" | while read x; do
    case "$x" in
        *"boolean false"*) systemctl --user --no-block stop sleep.target;;
        *"boolean true"*) systemctl --user --no-block start sleep.target;;
    esac
done
```

`~/.config/systemd/user/sleep.target`
```
[Unit]
Description=User level sleep target
StopWhenUnneeded=yes
```

Copy these files to the according location and `systemctl --user enable <SERVICENAME>` the respective services.
Also disable the autostart feature in the nextcloud client itself.


Finding the solution was not that trivial, here is what I tried before (just a small braindump):

1. Put a bash script to kill & start nextcloud into `/usr/lib/systemd/system-sleep` ([stackoverflow](https://askubuntu.com/questions/1313479/correct-way-to-execute-a-script-on-resume-from-suspend)). I could not get it to work - even though I used `sudo su`  to start it as my user.. problems with undefined variables and/or X server I guess.
2. Write a systemd *user* service listening to a custom-defined `sleep.target` (because the system-wide one is not available) to launch a bash script that kills and restarts nextcloud if it runs. This was not a good approach because
    - I defined a *restart* service to call the bash script restarting nextcloud. However when the restart service calls the scripts, forks a new nextcloud process, and then ends .. it also kills the nextcloud process (manually running the script works.. but systemd is very good at tidying up child processes)



# Ubuntu Mate 20.04 @ Lenovo Thinkpad T450s

## Wifi does not work

My Thinkpad (Intel Wireless 7265) and the bob cube (Alcatel HUB40, software version HH40_ER_02.00_29) don't work together.
According to the [Arch Linux docs](https://wiki.archlinux.org/title/Network_configuration/Wireless#iwlwifi) *n* support in `iwlwifi` is flaky and in many cases (such as mine) the only solution is to disable *n* in the `iwlwifi` driver. (Disabling *n* on the router does not help.)

Temporary solution: `sudo modprobe -r iwlwifi; sudo modprobe iwlwifi 11n_disable=1`

Permanent solution: add the line `options iwlwifi 11n_disable=1` to `/etc/modprobe.d/iwlwifi.conf`.

See [my Ubuntu bug report](https://bugs.launchpad.net/ubuntu/+source/backport-iwlwifi-dkms/+bug/1849891), [another Ubuntu bug report](https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1451246) or [this Ubuntu form thread](https://askubuntu.com/questions/1181844/wifi-disabled-ubuntu-19-10-intel-wireless-7265).

> This is not Ubuntu's fault I fear, e.g. Arch Linux booted from a USB stick has the same problem.

## Suspend

I'm not exatly sure why, but my understanding is that `UPower` by default tries
to *hybrid sleep* which failed and caused the laptop to simply consume the buttery until is completely empty.

`UPower` is configured in `/etc/UPower/UPower.conf` to take an action when the battery level is critical.
I set this to `CriticalPowerAction=Hibernate` (the default is `HybridSleep`).

The action taken for *hibernate* is configured in `/etc/systemd/sleep.conf`,
which I set to the following:

```
[Sleep]
#AllowSuspend=yes
#AllowHibernation=yes
#AllowSuspendThenHibernate=yes
AllowHybridSleep=no
#SuspendMode=
SuspendMode=suspend
SuspendState=mem standby freeze
#HibernateMode=platform shutdown
HibernateMode=suspend
#HibernateState=disk
HibernateState=mem standby freeze
#HybridSleepMode=suspend platform shutdown
HybridSleepMode=suspend
HybridSleepState=mem standby freeze
#HibernateDelaySec=180min
```

[This blog entry](https://www.ctrl.blog/entry/linux-laptop-low-battery-suspend.html)
explains the upower subsystem quite nicely.


# Kubuntu 18.04 @ Lenovo Thinkpad T450s

## Batteries

My https://thinkwiki.de/T450s has two batteries
- one that I can change: most probably the 3-cell-battery #68 (red dot): 0C52861
- an internal deeply buried one - quite annoying :/

## Troubles

I ended up putting the following scripts in `/lib/systemd/system-sleep`.

This script fixes that touchpad scrolling with a two finger gesture does not work anymore after resume:

`fix-touchpad.sh`:

```bash
#!/bin/bash

case "$1" in
    post)
        modprobe -r psmouse
        modprobe psmouse
        echo "modprobe script finished @ `date`" >> /tmp/modprobescript
        ;;
esac
```

And this script is an ugly workaround for notifications and global shortcuts not working anymore after resume (see also https://bugs.kde.org/show_bug.cgi?id=396682)


`fix-global-shortcuts-and-notifications.sh`

```
#!/bin/bash

sleep 6
export DISPLAY=:0
sudo -u markus kcmshell5 khotkeys &
sleep 1
killall kcmshell5
```

## Inkscape Tooltips

To avoid nearly unreadable (white on white) tooltips in Inkscape disable `Apply colors to non-Qt applications` in the KDE System Settings under Colors.

https://askubuntu.com/questions/725642/inkscape-tooltips-unreadable

## Eclipse & SVN

Using Eclipse 2018.09 and Kubuntu 18.04:

Subclipse by default uses JavaHL - this does not work well, for each svn update it takes Gigabytes of RAM and a long time. What works: switch to the pure Java implementation (SVNKit) in Preferences > SVN!

Subversive is no longer maintained, don't use it!


# Even older Kubuntu versions..

## File Associations / Default Applications

> Kubuntu 15.10 using KDE 5

Nowadays MIME types and .desktop files are involved in deciding which programs are used to open a certain file. See these wiki entries for [Arch Linux (recommended in-depth explanation)](https://wiki.archlinux.org/index.php/Default_applications) and [Debian](https://wiki.debian.org/MIME).

KDE uses `ktraderclient5` to determine the default program. Also `xdg-open` uses this program (see the function defapp_kde() in xdg-mime or the output of `xdg-mime query default application/pdf`). It returns useful defaults such as using Okular to open pdfs even if Gimp is installed (`ktraderclient5 --mimetype application/pdf`). However, programs not relying on `ktraderclient5` (such as Firefox) won't get these defaults as they are not stated in any `mimeapps.list` file. (FIXME: where do the defaults actually come from? [Maybe dig into the code...](http://api.kde.org/frameworks-api/frameworks5-apidocs/kservice/html/kmimetypetrader_8cpp_source.html))

As soon as you explicitly configure program preferences with Dolphin the defined program order is written to `~/.local/share/applications/mimeapps.list`. The order of entries displayed in Dolphin is reflected in the section [Added Associations]. The section [Default Applications] is filled in as well, but it seems that KDE ignores the default set there and sticks to the first program in [Added Associations].

The most useful way I identified to get consistent for all applications (KDE and non-KDE) is to explicitly set defaults in `~/.local/share/applications/mimeapps.list`, which will be updated for all programs when using the GUI method in Dolphin.

	[Default Applications]
	application/pdf=kde4-okularApplication_pdf.desktop;
	application/postscript=kde4-okularApplication_ghostview.desktop;
	image/bmp=org.kde.gwenview.desktop;
	image/gif=org.kde.gwenview.desktop;
	image/jp2=kde4-okularApplication_kimgio.desktop;
	image/jpeg=org.kde.gwenview.desktop;
	image/png=org.kde.gwenview.desktop;
	image/svg+xml=inkscape.desktop;
	image/tiff=org.kde.gwenview.desktop;


## Missing Ark Context Menus

	sudo ln -s /usr/share/kde4/servicetypes/konqpopupmenuplugin.desktop /usr/share/kservicetypes5/

http://www.linuxgnut.com/dolphin-extract-menus-missing-in-kubuntu-1510


## Eclipse Freezes when Debugging GUIs

> Kubuntu 14.04 using KDE 4.11

The whole workspace freezes when debugging GUIs, e.g. Java FX. Only switching to the console and killing the started program makes the desktop responsive again.

Solution: pass the VM argument `-Dsun.awt.disablegrab=true`.

Source: https://bugs.eclipse.org/bugs/show_bug.cgi?id=20006

Alternative solution: System Settings > Application Appearence > GTK > use "Raleigh" as GTK2 theme

The xfce gtk2 engine should also work (and is less ugly): apt install gtk2-engines-xfce
