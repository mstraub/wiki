# Exploring the File System

## cd - change directory

```bash
cd /etc/init.d                  # change into directory /etc/init.d
cd  or  cd ~                    # change into home directory
cd ..                           # change into parent directory
cd -                            # change into last directory you visited before
```

Note, that this is a **shell builtin**, not a program.

## ls - list directory contents

```bash
## list files, check free / used disk space
ls [`<dir>`]                      # list files (in dir)
ls -lah [`<dir>`]                 # list *all* files in *long format* (permissions, owner, size in *human readable format*,..)
ls -ld [`<dir>`]                  # info for directory itself
ls -l --time-style=long-iso       # by default no year is printed, use a full iso timestamp for dates
                                  # alternative: put export TIME_STYLE=long-iso into your ~/.bashrc
```

By default `ls` sorts files by name in ascending order, this can of course be changed using either short or long forms:

```bash
ls -lr                          # reverse order
ls -lS                          # ls -l --sort=size (biggest first)
ls -lX                          # ls -l --sort=extension (ascending)
ls -lt                          # ls -l --sort=time (file content modification time, newest first)
ls -lut                         # ls -l --sort=time --time=atime (last access time, newest first)
ls -lct                         # ls -l --sort=time --time=ctime (file attribute change time, newest first)
```


`ls` can also be used to search in the current directory. You can of course make use of bash wildcards here.

```bash
ls *.txt                        # show all text files in current directory
ls -d *dir*                     # show all directories in current directory containing 'dir'
```

Show full time and date of creation fime
```bash
ls -l --time=creation --time-style=full-iso
```

## tree - print file/directory tree

```bash
tree
```



## Disc Space

### du - disc usage

`du` always calculates the size of a directory recursively. The options limit verbosity of how many lines are printed, but the totals do not change.

```bash
du                              # recursively print disc usage of all directories below the current directory (+ the current directory)
du -sh <file>                   # human readable summary of total disk usage, or current directory if no file is given
du -c <file1> <file2>           # grand total is displayed in addition to usage for two directories
du -h -d <depth>                # total disk usage dir and subdirs (show usage for each subdir as well)
du --exclude=*.mp3              # disk usage except for mp3s
du -a                           # also print usage for files
du -s                           # only print a summary-line (disc usage of current directory)
du --max-depth 1                # only go N levels deep, 0 is the same as --summarize (-s)
```

### df - disc free

`df` is (kind of) the opposite of `du`, it shows remaining size (or inodes) on a partition.

```bash
df                              # show free space on all mounted filesystems
df -h                           # use human-readable format
df -i                           # show used / free nr of inodes
df -h <file>                    # show free space on the file system where file lies
```

### GUI approaches

Great graphical insights into disk usage are given by programs that analyze a whole directory tree and let you easily identify files or directories that take up the most space:

`ncdu` is a text-based (ncurses) program, `filelight` a KDE-based GUI program and `baobab` aka `Disk Usage Analyzer` is the GNOME equivalent.


# File Handling

## stat - display file or filesystem status

Show filesystem related information for a certain file:
- access permissions
- timestamp of last access / modification / status change
- ...

```bash
stat myfile
```

## file - determine file type

This tool determines the file type (and encoding for text files) by analyzing its contents.

```bash
file myfile                # print file type
file -i myfile             # print MIME type
file -bi myfile            # print MIME type (without prepending file name)
```

## touch, mkdir - create files and directories

```bash
touch file.txt             # creates an empty file using touch
> file.txt                 # creates an empty file using bash redirection
mkdir directory            # creates an empty directory
mkdir -p parent/subdir     # create the whole directory tree (p = parent)
```

Actually, `touch` was written to change times (by default: atime and mtime - access & modification time) of a file. By default it also creates files if they do not exist yet, the parameter `-c` suppresses file creation.

```bash
touch file.txt                                      # set atime and mtime of file.txt to current time
touch --date="2012-05-01 18:12" file.txt            # set atime and mtime of file.txt
touch --time=atime --date="2012-05-01" file.txt     # only set atime of file.txt
```

## cp - copy

```bash
cp srcfile targetfile      # copy files (dereferences symlinks automatically!)
cp -r srcdir targetdir     # resursively copy directories (dereferences symlinks automatically!)
cp -a  or cp -Rpd          # archive (backup) files: recursive, preserve symlinks, owner, timestamps, mode
```

Some important parameters:

- `-L`: always dereference symlinks
- `-P`: do not dereference symlinks
- `-d`: do not dereference and preserve symlinks
- `-p`: preserve owner, timestamps, mode (permissions)
- `-R` or `-r`: recursive copy
- `-u`: update - copy only if source file is newer than destination or missing

## mv - move (or rename)

The program `mv` moves files or directories from one location to another. If both locations are on the same partition, it is a simple rename, otherwise the content is copied and deleted.

```bash
mv src target              # move a file or directory.
```

When moving a file to an existing target that is also a file the target is overwritten without any notice. If the existing target is a directory, the file or directory to be moved is moved into the directory. It is therefore advisable to be careful when using `mv` with files.

```bash
mv -i srcfile targetfile   # interactive mode (asks if a file should be overwritten)
mv -n srcfile targetfile   # noclobber mode (does not overwrite files - but also does not print a message)
```

## rename (using regexes)

A practical tool for bulk renaming is the perl program `rename`.
There you can use the full power of perl regular expressions. [More examples.](http://tips.webdesign10.com/how-to-bulk-rename-files-in-linux-in-the-terminal)

```bash
rename -n s/old/new/ *           # dry-run of what would happen when renaming all files and directories in the current directory
rename -v s/old/new/ *           # actually rename and print all changes
```

**practical examples:**
```bash
rename 's/\.htm$/\.html/' *.htm  # rename all .htm files to .html
rename 's/\.bak$//' *.bak # remove .bak suffix from all files matching *.bak
rename "s/(.*)string-to-be-removed(.*)/$1$2/" * # remove string from middle of filename
```

change date format of files: `20211024_115409.mp4` to `2021-10-24_11-54-09.mp4`

	rename "s#(\d\d\d\d)(\d\d)(\d\d)_(\d\d)(\d\d)(.*)#\1-\2-\3_\4-\5-\6#" *.mp4

## ln - link

To make it easier to remember: as with `cp`, the first argument is the part that already exists.

```bash
ln src target  or  cp -l src target      # create a hardlink (both file reference the same inode)
ln -s src target  or  cp -s src target   # create a symbolic link (symlink)
```

## rm, rmdir - remove

```bash
rm file.txt                 # remove a file
rm -r directory             # recursively remove a directory
rmdir directory             # remove an empty directory
rmdir -p parent/sub         # remove an empty directory tree
```

## Secure File Handling

### scp - secure copy

`scp` can copy files between hosts (also between two remote hosts)

Recursively copy `localDir` to a remote host:


	scp -r localDir user@host:remoteDir


To copy between two remote hosts either the source host must have credentials to log into the target host or you use the issuing host as third party via `-3` (recommended).


	scp -3 -r user1@host1:remoteDir2 user2@host2:remoteDir2


### rsync - copy / synchronize directories locally and remotely

`rsync` can keep two directories (or single files) synchronized intelligently by only syncing changes, which makes it useful for e.g. remote backups.

>[!TIP] Note: Trailing slashes count
> A trailing slash after a directory denotes the _content_ of the directory.
> No trailing slash denotes the _directory itself_.

Simple local usage, where `localDir` and all its contents (and attributes) will be copied recursively:
```bash
rsync --archive localDir otherLocalDir # contents of localDir to otherLocalDir/localDir
rsync -a localDir otherLocalDir
rsync -a localDir/ otherLocalDir # contents of localDir to otherLocalDir
```

By default even the archiving preset does not delete data. Do do so:
```bash
rsync -a --delete localDir/ otherLocalDir
```

Resolve symlinks, i.e. copy contents behind symlinks
```bash
rsync -a --copy-links localDir/ otherLocalDir
rsync -aL localDir/ otherLocalDir
```

Dry-run and show what would be done
```bash
rsync -a --dry-run --itemize-changes localDir/ otherLocalDir
rsync -ani localDir/ otherLocalDir
```

When copying remotely by default `ssh` is used. Quick setup:
```bash
# @destination (where files are copied to)
# set up ssh server (listens for incoming connections)
sudo apt-get install openssh-server

# @source
ssh-keygen
ssh-copy-id user@remotehost
rsync -av localdirectory/ user@remotehost:directory
```

[More on](https://help.ubuntu.com/community/rsync) [this topic](https://wiki.ubuntuusers.de/rsync)

# Watching Files

Apart from watching a file's content with `tail -f`:

## watch

`watch` executes a program **periodically** (default: 2 seconds), you can e.g. use this for `head`:
```bash
watch head logfile.log
watch -n 60 head logfile.log # only update each 60 seconds
```

## entr

`entr` is smarter and can run arbitrary commands **when files change**.
The files to watch are given via `stdin`.

Print a message on first start and each time a file in the directory changes:
```bash
ls | entr -p echo ahoi
```

The path of the changed file can be retrieved with `/_` (don't put it in quotes).
Also the initial run can be omitted with `-p`.

Print the name of a file that changed:
```bash
ls | entr -p echo /_ changed
```

Run unit tests now and as soon as a source file changes:
```bash
find . -name "*.py" | entry pytest
```

