# System Logs

Log files, which are of great of importance for troubleshooting, are typically found in `/var/log`. Some very common log files and their content (apart from log files or directories created by some applictions like `/var/log/apache2` are:


- `/var/log/auth.log` authorization log: use of sudo, ssh,...
- `/var/log/daemon.log` daemon log
- `/var/log/debug.log` syslog messages with priotity DEBUG (& Ubuntu debugging messages)
- `/var/log/kern.log` kernel log
- `/var/log/messages.log` syslog messages with priority INFO
- `/var/log/syslog` most syslog messages

The **kernel ring buffer** is important, because it allows logging even in the startup phase when it is not yet possible to log to regular files in `/var/log`. It can be read directly with the command `dmesg`. Ubuntu also writes these messages to a log file if possible:

- `/var/log/dmesg` logs from kernel ring buffer

[More Ubuntu specific info.](https://help.ubuntu.com/community/LinuxLogFiles)


## syslogd

The original `syslogd` is nowadays (2014) mostly replaced by `rsyslogd` (e.g. Debian) and `syslog-ng` (e.g. SuSe).

The original config file for logging rules was `/etc/syslog.conf`. For rsyslogd it is `/etc/rsyslog.conf`, in Ubuntu configuration is further split into several files: logging rules are moved to `/etc/rsyslog.d/`.

### Configuration with Selectors

Original syslogd-style selectors (are still supported in rsyslogd) and consist of a **facility** (e.g. auth, cron, syslog, user1,..) and **priority** (e.g. debug, warning, err, alert,..), which together form a **selector**, and an **action**, e.g. logfile.

The selector is written in the form 'facility.priority', where either can be replaced by a star (*) - meaning all. The meaning of a selector is, that all logs from the facility with the given priority or higher will be logged. To select exactly one priority you can add an equals sign as such: 'facility.=priority'.

The action (or target) can be a regular file, named pipes, terminals or consoles, remote machines,.. Prepending the target with a minus (-) will make it buffered.


	# print kernel warnings (and worse) on a konsole
	kern.warn               /dev/tty10

	# print kernel warnings (and only warnings) on a konsole
	kern.=warn               /dev/tty10

	# log warnings (or worse) of several facilities
	local0,local1.warn      /var/log/localwarnings.log
	local0,local1.warn      -/var/log/bufferedlocalwarnings.log

	# log everything but auth & authpriv to syslog file

	*.*;auth,authpriv.none  -/var/log/syslog

	# print emergency messages on the terminals of all users

	*.emerg                 :omusrmsg:*


**Note**, that if syslog should write to terminals or consoles it must have root privileges. Therefore no privilege drops must be activated in the config file, e.g. `$PrivDropToUser syslog` (which is default in Ubuntu).

See also [rsyslog documentation on filters](http://www.rsyslog.com/doc/master/configuration/filters.html).

## klogd

`klogd` is similar to `syslogd` but only logs messages from the kernel and can do so in more detail. [See this page for more info (German!)](http://de.linwiki.org/wiki/Linuxfibel_-_System-Administration_-_Protokollierung#Der_klog-D.C3.A4mon)

## logger

`logger` can be used to conveniently log with syslogd, rsyslogd,.. e.g. in bash scripts. In addition to syslog it can also log to stderr, files and more.

```bash
logger "message"                        # log to syslog (user.noticy by default)
logger -p local0.warn "message"         # explicitly specify facility
logger -s "message"                     # also log to stderr
logger -f /tmp/extralog "message"       # also log to file (which must exist!)
```

## logrotate

`logrotate` allows automatic rotation, compression, removal, and mailing of traditional log files. Each log file may be handled daily, weekly, monthly, or when it grows too large. It is started regularly (typically daily) via a cron job and should help to avoid running out of disk space due to old log files.

Which logs should be rotated is configured in `/etc/logratate.conf` (or separate files in `/etc/logrotate.d`)

An example taken from `/etc/logrotate.d/rsyslog`:


	/var/log/debug
	/var/log/messages
	{
	        rotate 4
	        weekly
	        missingok
	        notifempty
	        compress
	        delaycompress
	        sharedscripts
	        postrotate
	                reload rsyslog >/dev/null 2>&1 || true
	        endscript
	}


## syslogd

`syslogd` is part of `systemd` and changes the way logging works drastically. `syslogd` collects logs from the early boot process, the kernel, the initrd, and even application standard error and out. Configuration is done in `/etc/systemd/journald.conf`.

All logs are stored in binary format (typically below `/var/log/journal`) and can be viewed in syslog-style or exported in other formats with `journalctl`:

```bash
journalctl --utc              # view logs with UTC timestamps
journalctl -k                 # view kernel messages (similar to dmesg)
journalctl --since "2015-01-10" --until "2015-01-11 03:00"
journalctl -b -o json-pretty  # get log since last boot in JSON format
journalctl -f                 # print logs and follow (like tail -f)
```

The `--since` switch accepts the following formats:
- `YYYY-MM-DD HH:MM:SS` strings (time or date part can be omitted, today / 0:00 will be assumed)
- `yesterday` `today` `tomorrow` (assuming 0:00)
- `1 day ago` `3 hours ago`
- `-1d` `-24h` `-5m`

[Check out this tutorial for more.](https://www.digitalocean.com/community/tutorials/how-to-use-journalctl-to-view-and-manipulate-systemd-logs)
