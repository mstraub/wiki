# Process Information

Via `procfs` exhaustive information about each process is available in `/proc/<process_number>`. 

However, for must use cases these programs are commonly used:
##### ps

`ps` extracts relevant information about current processes. It is more useful in scripts than the interactive programs `top` or `htop`, which provide about the same information.

```bash
ps                     # print all processes with same effective current user in current terminal
ps axu                 # print all processes (a=for all users, x=also if no tty, u=user format)
ps -l ax               # print all processes in long format (with e.g. nice value)
ps -u user -U user u   # print all processes for user in user format
ps axu --sort %mem     # print all proceses sorted by memory consumptin (cpu: %cpu)
ps -o nwlp <PID>       # print nr of light weight processes (=threads) for a process
```

Process trees give a good overview of parent-child relationships:

```bash
ps axjf                # print process tree
pstree                 # print even nicer process tree
```

##### pgrep

This is a good replacement for using first `ps` and then piping its output into `grep`.

```bash
pgrep java             # prints all ids of processes with java in its name
pgrep -f java -u user  # prints all ids of processes with java in its full command line and belong to user
```

# Starting & Stopping Processes

Bash normally executes commands in the foreground. Commands ending with an ampersand (&) are started in the background. A currently executed foreground-process can be stopped (SIGSTOP) by pressing CTRL-z.

Apart from that the [following bash builtins](http://web.mit.edu/gnu/doc/html/features_5.html) can be used for execution control:

```bash
jobs                   # list jobs (and their numbers)
bg                     # put last process into background (highest nr in 'jobs')
bg `<nr>`                # put process into background (same as start with &)
fg                     # put last process into foreground (highest nr in 'jobs')
fg `<nr>`                # put process into foreground
kill -s TERM %`<nr>`     # send signal to job (-s 15 and -s TERM are equivalent)
```
In addition to the bash builtins `kill` is also a program (see `type -a kill`). It can only work with process ids and not job ids. Actually kill can send any signal to a process. The most used signals are

- `SIGTERM` (15): politely ask a process to terminate gracefully and clean up all resources (default for `kill`, `killall`)
- `SIGINT` (2): ask a process to terminate (ungracefully) - often used for programs running from the terminal (CTRL-c)
- `SIGQUIT` (3): same as SIGQUIT, but also writes a core dump on some systems (CTRL-\)
- `SIGKILL` (9): force-kill unresponsive processes - only use if less drastic signals had no effect
- `SIGSTOP` (17,19,23): stop (pause) a process (CTRL-z)
- `SIGHUP` (1): tell a program to re-read its configuration files

In cases where the job or process id is unknown or multiple instances of the same program must be killed (or supplied with a signal) these two (more or less equivalent) programs can be used:

	killall -s KILL command  # kill all processes of command (exact name required, or use -r to interpret as regex)
	pgrep -l under           # list all processes whose *program* contains the string under, e.g. thunderbird
	pkill under              # send all matching processes SIGTERM


##### nohup

`nohup` starts commands and protects them from signals sent when the shell it was created within is terminated (e.g. when the remote session on a server is closed when you log out). Output is automatically redirected to nohup.out.
In combination with the ampersand to execute a command in the background this is quite useful for long-running tasks on remote machines (when not using 'screen' or 'byobu'):

```bash
nohup command            # start command detached from console
nohup command &          # start command detached from console (in background)
```


# Process Priorities

nice values represent the niceness of a process from from -20 (most favorable scheduling) over 0 (default) to 19 (least favorable).

```bash
nice -n -20 program      #start program with a nice-value of -20 (values < 0 may require root!)
nice --20 program        #same result, but not very readable
renice -n -20 `<pid>`      #give a process a new nice-value
```

