System information can be gathered from many places. Apart from various programs the two virtual filesystems `procfs` and `sysfs` are quite important.
They allow us to peek into the kernel by providing `/proc` and `/sys`.
There information about processes and other system information is presented in a hierarchical file-like structure - meaninging we can use it like every other file in Linux with our favorite tools.

Get help for `procfs` with `man proc` or on [Wikipedia](http://en.wikipedia.org/wiki/Procfs).

`Sysfs` is a successor of procfs and exports kernel data structures, their attributes, and the linkages between them to userspace. Documentation is [here](https://www.kernel.org/doc/Documentation/ABI/testing).

Modern one-stop-shops are `neofetch`, which also displays the distribution's logo as ASCII art, and `inxi`.

# Hardware

With `procfs` we can get info about the CPU, Interrupts (IRQ) and IO ports of devices can be viewed via `procfs` - the latter two only if the kernel module for devices is loaded.

```bash
cat /proc/cpuinfo    # CPU
cat /proc/scsi/scsi  # available SCSI devices
cat /proc/interrupts # IRQs
cat /proc/ioports    # IO ports
```

These programs also find devices without working or loaded kernel module by live probing the hardware.
If the standard output of these programs is not informative enough, try the verbose flag (-v or even -vv).

```bash
lspci                # list PCI devices - live information about PCI buses in the system and devices connected to them.
lsusb                # list USB devices
lshw                 # extract detailed information on the hardware configuration
                     # (basically every possible piece of hardware)
```

Advanced `lshw`

```bash
lshw -html > /tmp/html # generate a nice html to view it in a browser
lshw -short            # quick overview
lshw -class network    # filter by class (see -short for classes)
```

The tool `get-edid` (in the package read-edid) can help to identify monitors (which are not included in lshw or lspci):

```bash
sudo apt-get install read-edid
sudo get-edid
```


GUI applications available include `kinfocenter` or `hardinfo`.

## Memory

```bash
free -m              # quick overview of total, free and used memory (and swap) in megabytes
cat /proc/meminfo    # in-depth look at memory stats via procfs
```

## Sensors

Install the package `lm-sensors` to view sensor values like temperature or fan rpm:

```bash
sensors-detect       # set up which sensors to use
sensors              # view all sensore values
```

## Batteries

```bash
upower -e # enumerate devices (and their path)
upower -i # show info (for a path)
```
This can tell you e.g. your battery's `model`.
Check `capacity`, `energy-full` and `energy-full-design` for your battery's health.

# OS / Distro

```bash
uname -a             # print kernel version, platform (i.e. 32/64 bit),..
lsb_release -a       # print distribution information, e.g. release name and version
cat /etc/os-release  # text file containing about the same information as in lsb_release
cat /etc/issue       # text file containing login greeting (typically the release name)
```

# Kernel

Get kernel-related information:

```bash
cat /var/log/dmesg   # kernel messages (also the ones during boot)
dmesg                # print all kernel messages in kernel ring buffer (look for error messages there; and local printers)
cat /proc/cmdline    # kernel boot time arguments
lsmod                # show the status of modules in the Linux Kernel
cat /proc/modules    # loaded kernel modules (see also: lsmod, /etc/modules)
modinfo              # get information about kernel modules (e.g. parameters, version)
cat /lib/modules/`<kernelversion>`/modules.dep   # available kernel modules
lspci -v             # show hardware and which kernel driver they use
```

Programs & config files to load or unload kernel modules:

```bash
modprobe             # program to add and remove modules from the Linux Kernel (handles dependencies)
modprobe -r iwlwifi  # remove a module
modeprobe iwlwifi 11n_disable=1 # load a module and pass it a parameter
insmod               # simple program to insert a module into the Linux Kernel
rmmod                # simple program to remove a module from the Linux Kernel
depmod               # program to generate modules.dep
cat /etc/modules.conf   # config file specifying modules loaded at startup
```

Note: in Ubuntu 12.04 the file `/etc/modules.conf` seems to be replaced by the combination of `/etc/modules` and `/etc/modprobe.d/*`.

Other system information:

```bash
cat /var/log/messages   # system log - after start of the logging daemon (since Ubuntu 11.04: /var/log/syslog)
cat /var/log/boot.log   # system log - also before start of the logging daemon (written by dmesg after boot?)
```

# Filesystem

```bash
cat /proc/filesystems  # supported filesystems (currently loaded kernel modules)
cat /proc/swaps      # current swap partitions
cat /etc/mtab        # currently mounted partitions (the same output as when calling mount without parameters)
cat /etc/fstab       # config file for mountpoints
```

# Users

These programs give (slightly different) information about the currently logged in users (and more)

```bash
w                    # currently logged in users & what they are doing (+uptime as first line)
who                  # currently logged in users
```

# Uptime & Boot Date

```bash
who -b               # date+time of last system boot
uptime               # current time, how long the system has been running, how many users are currently logged on
                     # and the system load averages for the past 1, 5, and 15 minutes
cat /proc/uptime     # uptime of the system (seconds), and the amount of time spent in idle process (seconds)
```

# Date & Time

`date` is a versatile tool for formatting dates / times and setting the system time ([see also](linux/installation#date_timezone)).

Print the current time in different formats

```bash
date                      # default - a human readable format
date -I                   # only the date in ISO 8601
date -Iseconds            # date and time in ISO 8601
date +%s                  # seconds since 1970-01-01 00:00 UTC (i.e. unixtime)
date +%Y-%m-%dT%H%M       # custom format useful for scripts, e.g. 2019-08-14T1129
```

Instead of the current time a given time can be used with `-d`

```bash
date -d @1278923870              # unix timestamps must be prefixed with @
date -d "2021-01-01 09:00"       # ISO 8601 alike date/times are parsed as well
date -d "next monday 08:00 UTC"  # human readable format is supported as well
```

See `info date` for documentation on the date input formats.

Custom input date and formatting can of course be combined:

```bash
date -d @1278923870 -Iseconds
```
