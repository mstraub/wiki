> [!INFO] [Slurm](https://slurm.schedmd.com) is a cluster management and job scheduling system for large and small Linux clusters


## Overview

Get an overview of the cluster
```bash
sinfo
```

See the currently running jobs (and their ids, owners,..):
```bash
squeue
```

Customize the output format of `squeue` with something like this in your `~/.bashrc`
```bash
export SQUEUE_FORMAT="%.18i %.9P %.30j %.15u %.8T %.10M %.10l %.6D %R"
```

## Job Management

Write a small `script.sh` to start your job like this
```bash
#!/bin/bash  
  
#SBATCH --job-name myjob            # a good job name makes it easier to find your job in the queue  
#SBATCH --nodes 1                   # required  nodes (=servers)  
#SBATCH --ntasks 1                  # required processes  
#SBATCH --cpus-per-task=32          # max cpus  
  
# after configuring the SBATCH variables (yes the lines need to start with '#SBATCH') comes the job:  
  
jdk17/bin/java -jar job1.jar
jdk17/bin/java -jar job2.jar

# The steps above will be run on the allocated node (once resources have been granted by SLURM).  
# The commands are executed sequentially.
```

Then submit it
```bash
sbatch script.sh
```

Monitoring
```bash
scontrol show job ID
```

Kill a job
```bash
scancel ID
```
