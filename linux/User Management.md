# User Management

##### user and group ids - uid & gid

Root always has uid 0, uids from 1 until e.g. 499 (RHEL) or 999 (Debian) are reserved for system use. Uids above that are used for regular users, e.g. 1000 for the first user on Debian.

The same goes for gid: root has gid 0,..

Every user has at least one group, the first group being the primary group.

##### /etc/passwd

User details are stored in `/etc/passwd`. The columns (separated by colons) are:
 1.  login
 2.  optional encrypted password (or 'x')
 3.  uid (numerical user ID)
 4.  gid (numerical group ID)
 5.  comment (e.g. user name or daemon description)
 6.  home
 7.  shell (optional, set to `/bin/false` if no login should be possible)

Example:

	markus:x:1000:1000:markus,,,:/home/markus:/bin/bash


##### /etc/shadow

Normally the password is not stored in `/etc/passwd`, but in `/etc/shadow`, which should only be readable by root.
`/etc/shadow` consists of these colon-separated fields:

1. login
2. encrypted password (prepend with '!' to disable login)
3. date of last password change (days since epoch)
4. minimum password age (days)
5. maximum password age (days)
6. password warning period (days)
7. password inactivity period (days) (how many days after a password change is necessary is the old password still accepted)
8. account expiration date (days since epoch)
9. reserved field

All fields except the first two can be left blank, thereby disabling password aging.

Example (with shortened password hash):

	markus:$6$d0uFLcE...AMuZRV0:16560:0:99999:7:::

Get a human-readable representation of the expiration information with `chage`:

```bash
chage -l user
```

##### /etc/group

`/etc/group` consists of these colon-separated fields:

1. group name
2. encrypted password
3. gid
4. members (comma-separated list of users)

##### /etc/gshadow

As for users the group password is normally not stored in `/etc/group`.
Note, that the members in these two files are stored redundantly and must match.

`/etc/gshadow` consist of these colon-separated fields:

1. group name
2. encrypted password
3. administrators (comma-separated list of users that can change the group password)
4. members (comma-separated list of users)

##### vipw

Preferrably edit these important files with these tools to avoid concurrent editing and broken files (e.g. due to forgetting a colon).

```bash
vipw         # edit /etc/passwd
vipw -s      # edit /etc/shadow
vigr         # edit /etc/group
vigr -s      # edit /etc/gshadow
```

##### getent

With `getent` you can "get entries from Name Service Switch libraries", e.g. the passwd, group, shadow, gshadow databases. (quite similar to just greping the respective files)

```bash
getent passwd username
```

##### /etc/skel

This directory serves as a template for home directories. When a new user account is created, the home directory will be populated with the contents of `/etc/skel`.


## Add, remove, change users

The three standard tools are `useradd`, `userdel` and `usermod`.

##### Create a user

```bash
useradd name                # create a new user (without password; home directory is set, but not created)
useradd -m name             # also create a home directory (with contents of /etc/skel)
useradd -s /bin/bash name   # assign a shell
useradd -r name             # create a system user (uid <1000)
useradd -g gid -u uid name  # override default gid and uid (-o even allows uid-duplicates)
```

The password can be set with `passwd` or the `-p` option (but `-p` already requires the encrypted form to be stored in /etc/shadow)

##### Delete a user

```bash
userdel name                 # delete a user
userdel -f -r name           # force delete (even when logged in) a user and delete its home directory (-r)
```

##### Change basic user account details

`usermod` allows to change the parameters also available in `useradd` (and more):

```bash
usermod -u uid -g gid name      # change uid and gid for an existing user (ownerships in home directory are adapted as well)
usermod -d /home/newhome name   # change home directory (created if it does not exist)
usermod -md /home/newhome name  # change home directory and move all files from the old home
usermod -l newname name         # rename / change login of a user (does __not__ change the home directory)
```

## Add, remove, change groups

The tools `groupadd`, `groupdel` and `groupmod` work similar to the user-counterparts:

##### Create a group

```bash
groupadd gname               # create a new group
groupadd -r gname            # create a system group (uid <1000)
groupadd -g gid gname        # override default gid (-o even allows uid-duplicates)
```

##### Delete a group

```bash
groupdel gname               # delete a group
```

##### Modify a group

```bash
groupmod -g gid -n newgname gname  # change gid and name of a group
```

#### Managing groups of a user

View and add groups for a user:

```bash
groups user                          # show all groups a user is part of
usermod -a -G group1,group2 user     # add user to comma-separated group(s)
```

Remove or exlicitily set groups for a user:

```bash
gpasswd -d user gname                # remove user from group
usermod -G "gname1,gname2" user      # explicitly set comma-separated group(s) for a user
vigr                                 # open /etc/groups in vi (locks the file & enforces correct format on save)
```

### Group password

Groups can be assigned a password, which is stored in `/etc/gshadow` (or `/etc/group`). Group membership can then be temporarily acquired.

```bash
gpasswd gname                  # assign password to a group
newgrp gname                   # temporarily get membership for group
```

Group administrators are allowed to change/remove the group password and add or remove users to the group. They can be assigned as follows:

```bash
gpasswd -A "user1,user2" gname # assign administrator users to group
```

## (Un)locking Users

##### Lock a user

Locking is done (by convention) by prepending a '!' to the password in either `/etc/passwd` or `/etc/shadow`, or one of these two commands:

```bash
usermod -L user
passwd -l user
```

Or by deleting the currently set password (this can not be undone)

```bash
passwd -d user
```

##### Unlock a user

```bash
usermod -U user
passwd -u user
```

## User Account Limits

### Account Expiration

The fields in `/etc/shadow` regarding passwords age or account expiration can be manipulated with `chage`.

After a password has been expired the user is forced to chage it at the next login. If an inactivity period is specified accounts with expired passwords will be locked after this period.

```bash
chage -m min -M max user        # set minimum and maximum nr of days between password changes
chage -I days user              # set inactivity period in days
chage -d 2014-01-01 user        # manually set date of last passwort change
```

An account can also have a fixed expire date:

```bash
chage -E 2014-01-01 user        # set expiration date of account, afterwards no login is possible (accuracy = days)
```

Passing -1 as value will erase the limits.

### Limits

With the bash built-in `ulimit` resources allocated to a user can be limited for the current shell.

```bash
ulimit -a            # show all limits for the current user
```

With '-S' soft and '-H' (or no extra parameter) hard limits can be set. Soft limits can be raised up to the hard limit by the user. The hard limit can only lowered by the user.

```bash
ulimit -S -u 100     # set a soft limit for the maximum nr of processes
ulimit -n 50         # set soft & hard limit for the maximum nr of open files
```

A good place to set `ulimit`s is `/etc/profile`, or in the config file `/etc/security/limits.conf`, where the configuration can be specific to a user or group (prepend @):


	#`<domain>`      `<type>`  `<item>`         `<value>`
	user           hard    nproc          100
	@group         hard    nofile         50

### sudo & su

`sudo` allows a normal user to execute commands as other user (also super user), with `su` the current user id can be changed to become super user (a root shell).

```bash
sudo passwd otheruser          # set password for otheruser usint root privileges
sudo -u www-data mkdir dir     # create a directory as user www-data
```

Who (or which groups) are allowed to do so is specified in `/etc/sudoers`, which must be edited with `visudo` and roughly follows the format `user hostlist=(userlist) commands`:

- user: which user the permissions are for
- hostlist: on which host (when sharing the sudoers on several hosts)
- userlist: which identities the user may take
- commands: which commands may be executed

Simple example:


	# User privilege specification
	root    ALL=(ALL:ALL) ALL
	# Members of the admin group may gain root privileges
	%admin  ALL=(ALL) ALL
	# Allow members of group sudo to execute any command
	%sudo   ALL=(ALL:ALL) ALL


The format is quite complex, have a look in `man 5 sudoers`.


	#On the ALPHA machines, user john may su to anyone except root
	# but he is not allowed to specify any options to the su(1) command.
	john    ALPHA = /usr/bin/su [!-]*, !/usr/bin/su *root*


## User Monitoring

### Who is/was logged in?

The programs `w` and `who` can show who is currently logged into the system.

```bash
w               # print users who are logged in and their processes
who             # print information about users who are logged in
who -b          # time and date of last system boot
who -r          # print current runlevel
```

`last` prints the last log ins (user, time,..).

```bash
last            # all recent logins
last username   # last logins of user
last tty1       # last logins on tty1
```

### Who holds open files / ports?

`fuser` and `lsof` (list open files) can check which users or processes opened a file, file systems or ports.

**Note**, that these programs should be **run as root** to get a full report (especially when checking ports)!

```bash
lsof                        # list all files opened by active processes

fuser /tmp/file             # show process ids accessing a file
fuser -v /tmp/file          # show verbose info (including user, command,..)
lsof /tmp/file

lsof +D /tmp                # list all processes that have opened a file in /tmp

fuser -m -u /mnt/usb1       # show process ids (and the user name: -u) accessing a mount point / file system
lsof /mnt/usb1

fuser -n tcp 80             # show process ids accessing port 80 (equivalent: fuser 80/tcp)
lsof -i tcp@localhost:80

lsof -p 1000                # show files opened by process with id 1000

lsof -u markus              # show files opened by user markus
```

`fuser` can also kill the identified processes, which can be useful e.g. when trying to unmount a file system.

```bash
fuser -m -k /mnt/usb1          # send SIGKILL (default for -k) to all processes accessing /mnt/usb1
fuser -m -k -SIGTERM /mnt/usb1 # send SIGTERM (see fuser -l for supported signals)
```

## Users managing themselves

A normal user can not use `usermod`, but has the following possibilites.

##### Change password

```bash
passwd                   # interactively change password
```

##### Change login shell

```bash
chsh                     # start interactively
chsh -s /bin/bash        # change login shell
```

##### Get account & password information

Check e.g. the account or password expiry date with:

```bash
passwd -S                # oneliner
chage -l user            # more verbose
```


## Notifying Users

##### issue

Before login the contents of `/etc/issue` are printed.

##### motd

After login users can be greeted with a message of the day in `/etc/motd`.

##### wall

The command `wall` can send warning messages to all users logged into a machine.

```bash
wall
The system has to be shut down in 5 minutes, please save your work and log out.
`<Ctrl D>`
```

##### nologin

In case an administrator wants to temporarily forbid logins of other users the file `/etc/nologin` can be created. It allows no other users than root to log in (also via SSH,..). **Note**, that this is dangerous and not recommended if root has no password set (as per default in Ubuntu).
## Debian's Convenience Tools

Debian provides two convenience programs for adding and deleting users and groups.

However, for further tweaking of users and groups the default `usermod` tool must be used.

### Adding users and groups

By default `adduser` and `addgroup` automatically create fully usable accounts (e.g. by creating a new home directory), asks for a password and a detailed description and assures that the gid and uid conform to Debian standards. The defaults can be changed in `/etc/adduser.conf`.

These simple invocations create normal (non-system) users or groups:

```bash
adduser name
addgroup name
```

The description field in `/etc/passwd` is to sused tore these properties (comma-separated):
1. full name
2. room number
3. work phone
4. home phone
5. other


##### Create a system user that can not be used to log in

```bash
adduser --system --disabled-login name
```


### Deleting users and groups

These tools remove users and groups by removing the entries from the relevant config files, but leaves the home directory intact (by default - see `/etc/deluser.conf`).

```bash
deluser name
delgroup name
```

Home directories can however be removed or archived by providing a simple argument

```bash
deluser --remove-home name       # remove home directory and mail spool
deluser --remove-all-files name  # remove all files owned by user (system-wide)
```

When either the home or all files are removed a backup archive can be created as well:

```bash
deluser --backup --backup-to /tmp --remove-home name
```

### Remote login via Secure Shell (SSH)

[Log in](https://www.debian-administration.org/article/530/SSH_with_authentication_key_instead_of_password) to a remote host with `ssh username@host`.

To use authentication keys instead of passwords:

```bash
ssh-keygen -t ed25519                           # generate your public and private key (~/.ssh/id_rsa)
ssh-copy-id -i ~/.ssh/id_rsa.pub username@host  # copy it to the ~/.ssh/authorized_keys on the remote machine
```

