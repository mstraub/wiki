This document should outline a few steps that are useful after a fresh install of an Ubuntu Server - last updated for 22.04.

[TOC]

# Settings

```bash
sudo timedatectl set-timezone Europe/Vienna # sets /etc/timezone
```

# Install Useful Tools

```bash
sudo apt install plocate htop ncdu ranger tldr tree vim 7zip
```

# More Software

## Samba / CIFS

If you need to mount Windows network drives:

```bash
sudo apt install cifs-utils
```

## Java

Ubuntu provides multiple versions of OpenJDK, e.g.:

```bash
sudo apt install openjdk-17-jdk-headless
```

If you need other versions see the free and open source [Eclipse Temurin](https://adoptium.net) by Adoptium.


# Lighttpd

Their documentation is a little messy, here are the relevant pages:

- http://redmine.lighttpd.net/projects/lighttpd/wiki/TutorialConfiguration
- http://redmine.lighttpd.net/projects/1/wiki/HowToSimpleSSL
- http://redmine.lighttpd.net/projects/lighttpd/wiki/Docs_SSL

# OpenSSH

Disable root login in `/etc/ssh/sshd_config`:

	PermitRootLogin no

A good baseline is to only allow logins via public key authentication (disable password authentication), except for a fallback user with a very long and complex password.
See these lines in `/etc/ssh/sshd_config`:

```
PasswordAuthentication no

# exceptions must be placed at the very end of the config file:

Match User fallbackuser
	PasswordAuthentication yes
```

Ubuntu tutorials:
- https://help.ubuntu.com/community/SSH/OpenSSH/Keys
- https://help.ubuntu.com/community/SSH/OpenSSH/Configuring

Further harden OpenSSH according to the [secure secure shell guide](https://stribika.github.io/2015/01/04/secure-secure-shell.html)


# Enable Automatic Security Updates

Install unattended-upgrades:

```bash
sudo apt install unattended-upgrades
```

Or reconfigure it if it's already installed:

```bash
sudo dpkg-reconfigure -plow unattended-upgrades
```

This creates the file `/etc/apt/apt.conf.d/20auto-upgrades`.

In `/etc/apt/apt.conf.d/50unattended-upgrades` I typically set:

- `Allowed-Origins` to include updates (not only security updates)
- `Unattended-Upgrade::Remove-Unused-Dependencies "true";` to avoid filling up small hard drives with multiple kernel versions
- `Unattended-Upgrade::Automatic-Reboot "true";`
- `Unattended-Upgrade::Automatic-Reboot-Time "02:00";`

To see if a reboot is currently required check if the file `/var/run/reboot-required` exists.

## Also update custom PPAs

By default the `Allowed-Origins` for automatic updates only contain the Ubuntu repos.
In case you added any PPAs you must add them to be updated as well.

To find out the origin name see `less /var/lib/apt/lists/<THE_PPA>_InRelease`

In `/etc/apt/apt.conf.d/50unattended-upgrades` add the relevent `Allowed-Origins`, e.g.:

```
Unattended-Upgrade::Allowed-Origins {
    ...
    "packages.gitlab.com/runner/gitlab-runner:${distro_codename}";
    "LP-PPA-deadsnakes:${distro_codename}";
    "Rudder:${distro_codename}";
};
```

Then check with `sudo unattended-upgrade –-dry-run –d` if the expected packages will be upgraded
(i.e. a subset of `apt list --upgradable`)

Then run `sudo unattended-upgrade`.
Use `cat /run/systemd/shutdown/scheduled` to see if a reboot is scheduled.

See also:

-  `/etc/apt/apt.conf.d/20auto-upgrades` (and `man apt.conf`)
-  https://help.ubuntu.com/community/AutomaticSecurityUpdates
-  https://ubuntu.com/server/docs/package-management

# Swappiness

In case of problems with extensive swapping you may try to reduce the swappiness of the system.

Add a line to `/etc/sysctl.conf` and override the default swappiness of 60 with a much lower value, e.g.

	vm.swappiness=10

[http://wiki.ubuntuusers.de/Swap](http://wiki.ubuntuusers.de/Swap)

# Root Kit & Intrusion Detection

Have a look at at e.g. [chkrootkit](http://wiki.ubuntuusers.de/chkrootkit) and tiger [tiger](http://www.nongnu.org/tiger/)

# Greeting

When using `byobu` delete `~/.hushlogin` to still see the greeting (and all other info you usually get when logging in).

If you fancy an elaborate greeting message put something like this into `/etc/update-motd.d/99-greeting`

```bash
#!/bin/bash
# http://patorjk.com/software/taag/#p=display&h=1&v=1&f=ANSI%20Shadow&t=cities.ait.ac.at
# http://patorjk.com/software/taag/#p=display&h=1&f=Calvin%20S&t=my-server-name
# http://patorjk.com/software/taag/#p=display&h=1&v=0&f=ANSI%20Regular&t=my-server-name
echo ' ██████╗██╗████████╗██╗███████╗███████╗    █████╗ ██╗████████╗ █████╗  ██████╗    █████╗ ████████╗'
echo '██╔════╝██║╚══██╔══╝██║██╔════╝██╔════╝   ██╔══██╗██║╚══██╔══╝██╔══██╗██╔════╝   ██╔══██╗╚══██╔══╝'
echo '██║     ██║   ██║   ██║█████╗  ███████╗   ███████║██║   ██║   ███████║██║        ███████║   ██║   '
echo '██║     ██║   ██║   ██║██╔══╝  ╚════██║   ██╔══██║██║   ██║   ██╔══██║██║        ██╔══██║   ██║   '
echo '╚██████╗██║   ██║   ██║███████╗███████║██╗██║  ██║██║   ██║██╗██║  ██║╚██████╗██╗██║  ██║   ██║   '
echo ' ╚═════╝╚═╝   ╚═╝   ╚═╝╚══════╝╚══════╝╚═╝╚═╝  ╚═╝╚═╝   ╚═╝╚═╝╚═╝  ╚═╝ ╚═════╝╚═╝╚═╝  ╚═╝   ╚═╝   '

#or alternatively
figlet my-server-name
```

Don't forget to make the file executable. Also don't omit the shebang, otherwise it won't work!


# More Resources

<https://www.ubuntupit.com/best-linux-hardening-security-tips-a-comprehensive-checklist>
