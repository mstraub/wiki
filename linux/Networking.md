Networking is based on the [ISO/OSI model](http://en.wikipedia.org/wiki/OSI_model) consisting of seven layers.
 1.  Physical (network hardware)
 2.  Data Link (MAC)
 3.  Network (IP)
 4.  Transport (TCP, UDP)
 5.  Session (sockets)
 6.  Presentation (SSL)
 7.  Application (HTTP)

# IP, TCP, UDP

TCP (connection-based) and UDP (connectionless) are very common transport protocols based on the IP network protocol. Their differences are [explained here](http://www.diffen.com/difference/TCP_vs_UDP)

The internet protocol (IP) is currently in use in v4 and v6, see [this infographic](http://core0.staticworld.net/images/article/2014/10/irishtelecom-100478043-large.idge.jpg) for detailed comparison and the most important facts here:

|              | IPv4                                  | IPv6                                    |
| ---          | ----                                  | ----                                    |
| address bits | 32                                    | 128                                     |
| addresses    | ~4.3 billion (`10^9`)                 | ~340 undecillion (`10^36`)                |
| notation     | dot-separated numbers (8 bits: 0-255) | colon-separated hex (16 bits: 0-FFFF)   |
| localhost    | 127.0.0.1                             | ::1                                     |
| full example | 62.218.164.154                        | 2001:0db8:0000:08d3:0000:8a2e:0070:7344 |

For IPv6 addresses leading zeros can be omitted (000A = A or 0000 = 0) and one group of consecutive zeros can be shortened with a double-colon (::).
IPv4 requires [network address translation (NAT)](http://en.wikipedia.org/wiki/Network_address_translation) to map several hosts in a local network to one public IP. NAT is no longer required with IPv6.

## TCP/UPD Port Numbers

Port numbers (16 bit with 65536 possible ports) map to single services behind an IP. Conventions for port assignments are managed by IANA (Internet Assigned Numbers Authority) who provide an [up-to-date list of known ports](http://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.xhtml). On Linux systems `/etc/services` contains the known ports.

The port range of 0 - 65535 is split into three regions:
 1.  0 - 1023: system / well-known ports (ports registered with IANA, root required to open a port)
 2.  1014 - 49151: user / registered ports (ports registered with IANA, a normal user can open a port)
 3.  49152 - 65535: dynamic / private ports (no rules)

### Common Ports

| Port | Usage        | Full Name                                           |
| ---- | -----        | ---------                                           |
| 20   | ftp-data     | file transfer protocol                              |
| 21   | ftp-control  |                                                     |
| 22   | ssh          | secure shell                                        |
| 23   | telnet       |                                                     |
| 53   | domain       | domain name service (dns)                           |
| 80   | http         | hypertext transfer protocol                         |
| 123  | ntp          | network time protocol                               |
| 139  | netbios-ssn  | network basic input output system (session service) |
| 161  | snmp         | simple network management protocol                  |
| 162  | snmp traps   |                                                     |
| 389  | ldap         | lightweight directory access protocol               |
| 443  | https        |                                                     |
| 514  | rsh & syslog | remote shell & syslog                               |
| 636  | ldaps        |                                                     |

#### Email

| Port | Usage | Full Name                        |
| ---- | ----- | ---------                        |
| 25   | smtp  | simple mail transfer protocol    |
| 110  | pop3  | post office protocol             |
| 143  | imap4 | internet message access protocol |
| 465  | ssmtp |                                  |
| 993  | imaps |                                  |
| 995  | pop3s |                                  |


## IPv4 Netmasks

IP4v netmasks are 32 bits long. They consist of a consecutive number of set bits and then only unset bits. The Classless Inter Domain Routing (CIDR) notation gives the number of set bits. The number of hosts in a network is defined as 2^nr-of-0-bits minus 2. The lowest (network address) and highest (broadcast address) addresses have to be deducted.

List of class C networks (with only the last 8 bit block available):

| Dot Notation    | CIDR Notation | Bitmask Notation                    | Number of Hosts |
| ------------    | ------------- | ----------------                    | --------------- |
| 255.255.255.0   | /24           | 11111111.11111111.11111111.00000000 | 254             |
| 255.255.255.128 | /25           | 11111111.11111111.11111111.10000000 | 126             |
| 255.255.255.192 | /26           | 11111111.11111111.11111111.11000000 | 62              |
| 255.255.255.224 | /27           | 11111111.11111111.11111111.11100000 | 30              |
| 255.255.255.240 | /28           | 11111111.11111111.11111111.11110000 | 14              |
| 255.255.255.248 | /29           | 11111111.11111111.11111111.11111000 | 6               |
| 255.255.255.252 | /30           | 11111111.11111111.11111111.11111100 | 2               |
| 255.255.255.255 | /32           | 11111111.11111111.11111111.11111111 | 1               |

Note, that /31 is missing because it would only consist of a network and a broadcast address but no host. /32 is a special case identifying a single address.

# Network Configuration

Physical network interfaces on systems using `systemd` use [predictable network interface names](https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames) in the form `en` (ethernet), `wl` (WLAN), or `ww` (WWAN), e.g. `enp0s25`. The advantage of this naming schema is, that the names will not change on reboots, which could happen with the old scheme using `ethN` and `wlanN`, e.g. `eth0`.

As of 2024 on Ubuntu you should use the tools `ip`, `nmcli`, etc.. see https://www.cyberciti.biz/faq/howto-linux-bsd-unix-set-dns-nameserver/

Basic networking status with the `NetworkManager` CLI `nmcli`:
```bash
nmcli general status # summary for all interfaces
nmcli networking connectivity
nmcli # 

nmcli device status # device overview
nmcli device show # device details
nmcli connection show # show connections (e.g. saved WIFIs)
nmcli -f NAME,DEVICE,FILENAME connection show # also show the files where the connection is stored
```

Get available WIFI networks and connect to one
```bash
nmcli device wifi list
nmcli device wifi connect SSID password PASSWORD
```

What is my (external) IP  - use this website from the command line:
```bash
curl ifconfig.me
```

> [!WARNING] content below here is quite outdated


Inspection of the current state and temporary configuration can be done with `ifconfig`, `route` and `ip`.

## Dynamically Configure IP, Netmask, Broadcast

```bash
ifconfig -a      # view all interfaces (also disabled ones)
netstat -i -e    # same..
```

Basic configuration:

```bash
ifconfig eth0 192.168.0.1  # minimum
ifconfig eth0 192.168.0.1 netmask 255.255.255.240 broadcast 192.168.0.16
```

Turning interfaces on and off:

```bash
ifconfig eth0 up
ifconfig eth0 down
```

Virtual interfaces can be simply created if the underlying physical interface exists

```bash
ifconfig eth0:0 192.168.0.1  # create
ifconfig eth0:0 down         # disable / remove
```

## Dynamically Configure Gateway

The gateway address is the address packets not destined for our current subnet are sent (and then e.g. sent into the internet through a router). It is set via `route` in the kernel's IP routing table.

```bash
route            # view current routing table
route -n         # same, but with numeric IP addresses instead of names
```

Set a default route (to a gatewy) for a network

```bash
route add default gw 10.0.0.138
```

Add route for a specific network (network address and netmask must match!) and a specific device:

```bash
route add -net 10.0.1.0 netmask 255.255.255.0 gw 10.0.0.138 dev eth0
```

For removing routes the exact same command has to be executed, but with `add` replaced by `del`:

```bash
route del -net 10.0.1.0 netmask 255.255.255.0 gw 10.0.0.138 dev eth0
```

## Statically Configure the Network

To permanently configure the network configuration (also after a reboot) use `/etc/network/interfaces`. All interfaces configured here can be en/disabled with `ifup` and `ifdown`.

Use DHCP:

```bash
auto eth0
iface eth0 inet dhcp
```

Manual configuration:

```bash
auto eth0
iface eth0 inet static
address 192.168.0.100
netmask 255.255.255.0
gateway 192.168.0.1
```

Note, that since at least Ubuntu 12.04 desktop versions use [NetworkManager](https://help.ubuntu.com/community/NetworkManager) instead of `/etc/network/interfaces`. Use the respective GUI tools or `nmcli` and `nm-tool` for configuration.

## Name Resolution (Static)

Simple static name resolution can be configured in `/etc/hosts`, which contains a simple name to IP mapping.

Usage of the Domain Name Service (DNS) is configured in `/etc/resolv.conf`. The keyword `nameserver` specifies DNS servers:

```bash
nameserver 8.8.8.8
nameserver 8.8.4.4
```

[In Ubuntu 14.04](http://unix.stackexchange.com/questions/128220/how-do-i-set-my-dns-on-ubuntu-14-04) or later you should edit `/etc/resolvconf/resolv.conf.d/head` and then run `resolvconf -u`, since `/etc/resolv.conf` is generated.

In `/etc/nsswitch.conf` you can configure if and in which order local files (`/etc/hosts`) and DNS should be used:

```bash
hosts:    files dns
networks: files dns
```


DNS (reverse) lookups can be done with `host` or the 'domain information groper' `dig` (`nslookup` is obsolete due to security issues!).

```bash
host -a `<hostname>`   # look up DNS entry for a hostname
dig `<hostname>`       # same, but more verbose (and more options available)
```

# Network Troubleshooting

## Check State of Network Hardware / Connection

```bash
ethtool              # query or control network driver and hardware settings
nm-tool              # report NetworkManager state and devices
```

## Check Routes

```bash
tracepath `<host>`     # trace the path to a network host (which servers / hops are on the way)
tracepath6 `<host>`    # for IPv6
traceroute `<host>`    # same as tracepath, but requires root
traceroute6 `<host>`   # for IPv6
```

## Check Open Ports & Connections

`ss` (short for socket statistics I guess) can provide very detailed information about open connections and statistics and is a modern replacement for `netstat`:
```bash
ss -tulpn       # print listening sockets and the respective programs (i.e. local servers)
```

As of 2024 `netstat` is obsolete. Use `ss` instead. 
```bash
netstat -a           # print open & listening internet connections and Unix domain sockets
netstat -a -t -p     # print only tcp connections (-u for udp) and the associated pid
netstat -s           # print statistics about e.g. nr of received packets
netstat -r           # print the kernel routing table
netstat -tulpn       # print listening sockets and the respective programs (i.e. local servers)
```

`nmap` is an advanced port scanner ([see some examples here](http://www.cyberciti.biz/networking/nmap-command-examples-tutorials/)). A simple use case is to check which services are available / ports are open on a server:
```bash
nmap localhost       # print open ports on current machine (only a selection of common ports)
nmap -p 1-9999 host  # print open ports on a host (with a defined port range)
nmap -Pn -p 1-9999 host  # as above, but skip host discovery (helpful if above probe fails)
nmap -A -T4 host     # print open ports with enabled version detection (-A) and don't wait too long (-T4)
```


[Advanced guide for Ubuntu network configuration](https://help.ubuntu.com/14.04/serverguide/network-configuration.html)

## DNS cache

Inspect and flush (if necessary) the DNS cache
```bash
sudo resolvectl show-cache
sudo resolvectl flush-caches
```

# Download

Use either `wget` or `curl`.
```bash
wget -O /tmp/downloaded.html example.com
curl -o /tmp/downloaded.html example.com
```

Download from URL & print directly to stdout (this is the default behavior of `curl`)
```bash
curl example.com
wget -q -O - example.com
wget -q -O - example.com | /usr/share/vim/vim74/macros/less.sh # same but with syntax highlighting
```

See debugging info of HTTP request
```bash
curl -v example.com # verbose mode
curl --trace - example.com # for even more info: full trace dump of all incoming and outgoing data to stdout
```

Nextcloud upload
```bash
curl -T modestats.png -u StraubM:THEPASSWORD "https://nextcloud.ait.ac.at/remote.php/dav/files/StraubM/modestats.png
```
# Network Services with (x)inetd

The deprecated `inetd` and the modern (and icompatible) `xinetd` are super-daemons that listen for incoming requests on several ports and start the according services only on demand. This can be useful to save resources by running rarely used services only when needed.

`inetd` was configured via one entry for each service in `/etc/inetd.conf` (where the service name should be present in `/etc/services` or a port number):


	# service     type     protocol  wait    user    programm            arguments
	telnet        stream   tcp       nowait  root    /usr/sbin/telnetd   telnetd -a
	pop3          stream   tcp       nowait  root    /usr/sbin/tcpd      ipop3d
	3000          stream   tcp       nowait  nobody  /bin/echo           echo Hello World


`xinetd` provides additional features such as access control mechanisms (e.g. with TCP Wrapper ACLs), extensive logging capabilities, the ability to make services available based on time or the maximum number of started services (to counter DoS attacks). It is configured via `/etc/xinetd.conf` and one file for each service in `/etc/xinetd.d/`.
An example configuration for the TCP version of daytime that prints the current date and time:


	service daytime
	{
	        disable         = yes
	        type            = INTERNAL
	        id              = daytime-stream
	        socket_type     = stream
	        protocol        = tcp
	        user            = root
	        wait            = no
	}


## TCP wrapper

`tcpd` is a TCP wrapper which first performs security checks and then starts a program (see pop3 service in the inetd-example above). `tcpd` checks if the incoming request is allowed through the settings in `/etc/hosts.allow` and `/etc/hosts.deny`. The check if access is granted works as follows: first allow is checked. If not explicitly allowed it is checked if it is explicitly denied. If not it is allowed. The lines in each are processed in order of appearance and search terminates when a match is found (so put more specific rules first).

Syntax: `daemon : client [:option1:option2:...]` (see `man hosts_access`), here are some simple examples:


	vsftpd: 192.168.1.,.abc.com
	sshd: ALL EXCEPT 192.168.0.15
	ALL: ALL
