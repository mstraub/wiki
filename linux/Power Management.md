Suspend / Hibernate with the package `pm-utils`

```bash
pm-suspend           # suspend to RAM
pm-hibernate         # suspend to disk
```

Turn off screen

```bash
xset dpms force off
```
