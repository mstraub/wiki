# Working with Text

```bash
more file.txt                       # page through contents of file
less file.txt                       # page through contents of file more conveniently
```

```bash
paste file1 file2                   # view lines of two files next to each other, separated by a tab
paste -d ';' a.csv b.csv            # merge the columns of two .csv files line by line
```

`paste` allows viewing files next to each other i.e. vertically concatenate them.  This is useful to e.g. merge .csv files or as a simple drop-in for `diff`. The delimiter can be changed with -d.

```bash
nl file.txt                         # view content of file with line numbers
nl -i 5 -s '|--|' sort.txt          # increment line number by 5 and display the string |--| between line number and line
```

Add line numbers with [`nl`](http://www.thegeekstuff.com/2013/02/wc-nl-examples) (**n**umbers **l**ines).
The width of the line-number can be adjusted (-w N).

```bash
fmt lorem.txt                       # reformat text to 75 characters per line
```

`fmt` does the same as dynamic word wrap in modern editors. Paragraphs are reformatted to a character width (-w N), optionally with a different indentation for the first line per paragraph (-t).

```bash
rev lorem.txt                       # reverse each line and print it to stdout
```

`rev` reverses lines of text

```bash
pr lorem.txt                        # create pages with 66 lines including a header (in one text file9
pr -2 -l 50 lorem.txt               # 2-column layout, only 50 lines per page
```

`pr` converts text files for printing and should be combined with `fmt` because it takes lines as-is.

```bash
tee                                 # write stdin to stdout
echo 'hello' | tee file.txt         # writes 'hello' to stdout and into file.txt
```

`tee` writes it input to stdout and as many files as desired. Files can be appended (-a).

```bash
wc -l file1.txt file2.txt           # show word count for both files
```

`wc` aka 'word count' counts lines (-l), word (-w), (UTF-8) characters (-m), bytes (-c).

## Filtering & Selecting

##### tr

`[tr](http://www.thegeekstuff.com/2012/12/linux-tr-command)`, aka translate characters, is a good supplement to `sed` when it comes to special characters. With `tr` newlines, spaces, and non-printing characters can easily be replaced, deleted or squeezed. Important arguments are delete (-d), squeeze (-s) and complement (-c).

```bash
tr -d '[:space:]' < lorem.txt    # delete all white-space from lorem.txt and print it to stdout
tr -s '\n' ' ' < file.txt        # join all lines of a file into a single line
tr '[A-Za-z]' '[N-ZA-Mn-za-m]'   # rot13
```

##### cat & tac

`cat` and `tac` concatenate files (opposite of `split`). `cat` is also often used to simply print a file to stdout to view its content or pipe it into other programs.

```bash
cat file1 file2    #print file1 and then file2 to stdout
cat -A file1       #show non-printing characters, tabs, line ends. ugly results for non-ASCII characters.
tac file1 file1    #print file1 and then file2 to stdout, but reverse the line order for each file
```

##### sort

`sort` can be used to sort lines in one (or more) files. The ordering can be alphanumeric (default), numeric (-n), human-readabe numeric e.g. 2K 1M (-h),... The input can also be sorted reverse (-r) or randomized (-R).

```bash
sort file1.txt file2.txt     #print sorted lines from all input files
sort -k 1.3 file.txt         #sort file by the line-content starting with the third character
sort -t ';' -k 3 file.csv    #print lines of csv, sorted by third the column
sort -u file.txt             #sort file and only print unique lines (see also: `uniq`)
```

##### shuf

Shuffle - the opposite of `sort`.
People on StackOverflow claim it's faster than `%%sort --random-sort%%`.

##### uniq

With `uniq` you can filter (omit) or report adjacent repeated lines.

```bash
uniq file.txt                 #filter adjacent repeated lines
uniq -i -u file.txt           #filter while ignoring case and only print unique lines
uniq -c -d file.txt           #filter, and only print duplicates and their count
uniq -s 2 -w 4 file.txt       #filter, but only compare 4 chars not including the first 2 for each line
```

##### head & tail

`head` and `tail` allow you to print the first / last lines or bytes of a file.

```bash
head file.txt             # print first 10 lines
head -n 5 file.txt        # print first 5 lines
head -c 10 file.txt       # print first 10 bytes (no word-option)
tail -n 10 file.txt       # print last 10 lines
tail -n +2 file.txt       # print everything from line 2 on (everything except first line)
```

`tail` can also be used to "follow" a file, i.e. print changes to the screen in real time:

```bash
tail -f file.txt    # print last 10 lines and all lines that are appended to the file in the future
tail -F file.txt    # same as --retry -f (realizes, when the file to be tailed is deleted and created again)
```


### Regular Expressions

Regular expressions are a very powerful tool to match strings of text. It comes in various flavours, the one of concern here are POSIX.2 regexes, which are documented in `man 7 regex`.
A great resource that also lists the slight differences between regular expressions in different programming languages,.. is [Rex Egg](http://www.rexegg.com)

#### Tools making heavy use of regular expressions

##### awk

`awk` .. [1liners](http://www.pement.org/awk/awk1line.txt)

##### sed

`sed` aka 'stream editor' is commonly used to **replace strings** in lines of text files. See [this list of one-liners](http://sed.sourceforge.net/sed1line.txt) for more inspiration.

```bash
sed
sed s/abc/xxx/ file.txt    #simple use-case: replace the first 'abc' in each line of file.txt with 'xxx' and print it to stdout
sed s/abc/xxx/g file.txt   #global replace: replace all 'abc' per line
sed -i -e 's/a c/x x/g' -e 's/a/b/' file.txt    # replace in-line (overwrite file.txt) with -i, and several expression with -e
```

Two good tricks to create readable regexes are to (1) escape the whole regex with single quotes and (2) use a different separator than `/` when appropriate. See for yourself in this example of replacing all backslashes in a line with slashes.

```bash
sed s/\\\\/\\//g count.txt   #wtf
sed s#\\\\#/#g count.txt     #use any character as separator-char
sed 's#\\#/#g' count.txt     #avoid escaping for the shell. we still must escape the backslash for the regex.
```

Advanced usage examples:

```bash
sed -n 1~2p file.txt         #only print odd lines (=print line 1 and then print each line at step 2)
```

By default `sed` regular expressions are limited, e.g. do not support matching groups. Use the switch `-E` to activate extended regular expressions and enjoy matching groups:

```bash
sed -E 's#(.*),(.*)#\1;\2#g' #replace a single comma with a semicolon
```

However, even with extended expressions `sed` does not support non-greedy expressions as it is possible with PCRE (perl compatible regular expressions). A good resort is to simply use `perl` itself:

```bash
perl -pe 's/.*thevalue="(.*?)".*/\1/g' file.txt
```

##### grep

Search for patterns, i.e. regular expressions, within files with `grep`.

> Its name comes from the ed command `g/re/p` (globally search for a regular expression and print matching lines)

`grep`  is used to **print selected lines** that match the regex. It can be used on a single file:

```bash
grep regex file.txt              #print lines containing the string 'regex' in file.txt
grep '^[[:digit:]]' file.txt     #print lines starting with a digit
grep '[[:alpha:]]$' file.txt     #print lines ending with an alphanumeric character
grep '[1-3]a' file.txt           #print lines containing 1a, 2a or 3a
```

But an also be used recursively to e.g. search in all files in or below the current directory:

```bash
grep -r searchString .
grep -R searchString . # same, but also follows symlinks
```

Flavours of grep:

```bash
rgrep         #same as grep -r: recursively execute grep for each file under a directory
fgrep         #same as grep -F: search for fixed strings, do not interpret the search string as regex
egrep         #same as grep -E: use extended regular expressions
egrep '([1-3]a){2}' file.txt   #print lines containing 1a, 2a or 3b exactly twice after each other
egrep 'one|two' file.txt       #print lines containing either 'one' or 'two' (using extended regular expressions)
```

More examples:

```bash
grep --include "*.py" -R regex . # recursive grep filtered by file extension
grep -i regex . # ignore case
grep -n regex . # also print line nr (and not only the file where the line is found)
```

#### Regular expression tipps

##### Capture groups

If you want to insert text with regular expressions, [groups](http://www.rexegg.com/regex-capture.html) come in handy. A group is defined by parenthesis `()` in the find-expression. All that is matched by the expression within the parenthesis can be added to the replace-expression with `${n}` or `\{n}`, where n is the group number. For example:

```bash
rename 's/(^\d*)/${1}_insert/' 1234_abc.txt     # renames file to: 1234_insert_abc.txt
```

##  Split & Merge

`split` can split one file into several files. Splitting can be done by lines (-l N) or into a nr of equal chunks (-n N). By default files are named x__: x plus two suffixes iterating through the alphabet, from xaa to xzz. The resulting names can changed: The prefix is an optinal argument after the file name, the nr of suffixes is set with -a.

```bash
split file.txt                      # split file.txt in chunks of 1000 lines and name them xaa, xab,..
cat xa* > merged.txt                # merge split file again
split -a 3 -l 1 big.csv splitfile   # create files with the names 'splitfile***', containing one line each
split -b 700M archive.tar.gz        # split an archive into 700MB pieces
```

`csplit` splits files according to a context (regex), hence the c.

```bash
csplit file.txt "/regex/"           # split file at first occurrence of regular expression
csplit file.txt "/regex/" "{*}"     # split file at all occurrences regular expression
```

Merging files is handled by `cat` (concatenate)

```bash
cat file1 file2 file3 > mergedfile
```


## CSV handling

[csvkit](https://csvkit.readthedocs.io) is a suite of CLI tools to work with CSV written in Python.
It is much more powerful than the pure unix tools since it handles quoting!

```bash
csvcut -n file.csv                  # print detected colum names
csvcut -d ";" -c colB,colA file.csv # extract two columns (in that order) from the csv
```

Or if you are a unix tool purist:

```bash
expand file.txt           #replace each tab with (up to) 8 spaces
expand -t 4 file.txt      #replace each tab with (up to) 4 spaces
expand -t 8,16 file.txt   #first replaced tab has last space at position 8, second tab at 16. Other tabs are replaced with a single space.
```
`expand` replaces tabs with spaces. `unexpand` does the opposite.

```bash
cut
cut -c 3-5 file.txt       #print characters 3 to 5 of each line
cut -d ';' -f 1,3 csv.csv #print columns 1 and 3 of a CSV
```

[`cut`](http://how-to.linuxcareer.com/learning-linux-commands-cut) can be used to cut bytes (-b), characters (-c) or fields of a tab-separated file (-f). Currently cut is buggy in Ubuntu 12.04, -c is treated as -b and so the tool breaks for UTF-8 characters.

```bash
join a.csv b.csv                   #join two files on first field
join -t ';' -j 2 a.csv b.csv       #join two semicolon-separated files on the second field
join -t ';' -1 1 -2 4 a.csv b.csv  #join first field of a.csv on fourth field of b.csv
join --header a.csv b.csv          #joins and treats first line as header
```

[`join`](http://how-to.linuxcareer.com/learning-linux-commands-join) is used to join e.g. two CSVs on a common column, which must be sorted. The default field separator is blanks, i.e. spaces or tabs.
By default the result for a non-successful join (no equal fields) is empty, because unpairable lines are omitted. These can be printed with -a.



## Internationalization

Text can come in various encodings. `recode`, `iconv` and `dox2unix` convert file contents from one encoding to another, `convmv` converts filenames from one encoding to another.

Common encodings encountered in Austria are:

- ASCII: 7 bit (128 characters) and base for all other encodings in the list
- ISO-8859-1 (latin1): old western european ASCII extension
- ISO-8859-15 (latin9): slightly updated version of latin1 (e.g. with €)
- CP-1252: code page used by German versions of Windows (superset of ISO-8859-1)
- UTF-8: Unicode

##### recode - in-place recoding & filtering

`recode` can operate in two modes:

- as in-place-recoding tool
- as filter

The 'request' typically looks like `from..to`, where from and to consist of charset/surface. Charsets are e.g. UTF-8 or ISO-8859-1 but also `HTML`,  `JAVA`. Common surfaces for line-ends are carriage returns `/CR` (Unix) or carriage return and line feed `/CL` (Windows), but you can also convert e.g. from/to base 64 (`/b64`), hexadecimal (`/x1`), decimal (`/d1`) or quoted printable (`/QP`).

When from or to or the surface are ignored, the default is used. The default charset depends on the system locale, the surface on the charset.

Some in-place examples:

```bash
recode HTML file.txt                # from html to default
recode ..HTML file.txt              # from default to hml
recode UTF-8..HTML file.txt         # from utf8 to hml
```

In-place line-end switching:

```bash
recode ../CR file.txt               # convert to Unix line endings
dos2unix file.txt                   # same
recode ../CL file.txt               # convert to Windows line endings
unix2dos file.txt                   # same
```

Filter example:

```bash
cat file.txt | recode /b64 > newfile.txt   # convert base64 encoded file to default encoding
```

##### iconv - stream-based recoding

```bash
iconv -f ISO-8859-1 -t UTF-8 in.txt > out.txt  # stream-based recoding of file contents to UTF-8
```

##### convmv - recoding of file names

By default this command only prints what it would do, use `%%--notest%%` when the results are as expected.

```bash
convmv -f ISO-8859-1 -t UTF-8 --notest file
```



