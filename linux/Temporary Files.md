Temporary files are typically stored in `/tmp`.

Automatic cleanup as of 2022 often happens via [systemd-tmpfiles](https://www.freedesktop.org/software/systemd/man/tmpfiles.d.html)

See related systemd tasks and timers:

	systemctl status systemd-tmpfiles*

Config (in Ubuntu 20.04) is located in `/usr/lib/tmpfiles.d/` and `/etc/tmpfiles.d`.
The latter folder is recommended for user configs, e.g. `/usr/lib/tmpfiles.d/tmp.conf`:

```
# See tmpfiles.d(5) for details
# Recursively clean files and folders accessed or created more than two days ago in the `/tmp` directory
d /tmp 1777 root root 2d
```

Run cleanup manually and log the reason why some files are not cleaned up:

	sudo env SYSTEMD_LOG_LEVEL=debug systemd-tmpfiles --clean

