Geographic Information Systems

# Projections

- [EPSG:4326 aka WGS84](https://epsg.io/4326) as known from GPS coordinates
- [EPSG:3857 Pseudo-Mercartor](https://epsg.io/3857) as used by tile servers (e.g. OSM)

## Austria

- Projections for all of Austria
    - [EPSG:3416 ETRS89 / Austria Lambert](https://epsg.io/3416)
    - [EPSG:31287 MGI / Austria Lambert](https://epsg.io/31287) (**deprecated** but used for some OGD)
- Three projections (that are more precise?) but only cover parts of Austria: 
    - [EPSG:31254 MGI / Austria GK West](https://epsg.io/31254) 
    - [EPSG:31255 MGI / Austria GK Central](https://epsg.io/31255)
    - [EPSG:31256 MGI / Austria GK East](https://epsg.io/31256) 


# Useful Open Data

## Austria
- [Adressen (BEV)][https://www.data.gv.at/katalog/dataset/31416a3d-5a44-4f78-8b2c-765a2738aacc]
- [Gemeindegrenzen (historisiert ab 2011)](https://www.data.gv.at/katalog/dataset/566c99be-b436-365e-af4f-27be6c536358)
- [GTFS](https://mobilitaetsverbuende.at/)

## Europe
- [NUTS 0-3 boundaries](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/nuts)
- [Geostat/Eurostat 1km² population grid](https://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/population-distribution-demography/geostat)

## Worldwide
- [world-wide country borders (world bank )](https://datacatalog.worldbank.org/search/dataset/0038272/World-Bank-Official-Boundaries)
- [Open source boundaries ](https://www.geoboundaries.org)

and more..
- https://freegisdata.rtwilson.com/

# General Bikeshare Feed Specification (GBFS)

Specs: https://github.com/MobilityData/gbfs
List of all systems (including URL for data): https://github.com/MobilityData/gbfs/blob/master/systems.csv

For example - **Wien Mobil Rad**: 
- https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_wr/gbfs.json
- https://gbfs.nextbike.net/maps/gbfs/v2/nextbike_wr/de/station_information.json

Since the GBFS JSONs can not directly be used by GIS software
we need to convert it to gejson.

Behold the [JMESPath CLI tool jp](https://github.com/jmespath/jp) (`sudo apt install jp`) 

Create a file named `expr.txt` : (adapted from [here](https://gist.github.com/dschep/c987bead83b9c0513c32344d38e4fdf4))

    {type: 'FeatureCollection', features: data.stations[*].{type: 'Feature', geometry: {type: 'Point', coordinates: [lon, lat]}, properties: @}}

Then convert with:

    jp --filename station_information.json --expr-file expr.txt > station_information.geojson


# GPSbabel

GPSBabel converts waypoints, tracks, and routes between popular GPS receivers and mapping programs. It also has powerful manipulation tools for such data. Install it

```bash
sudo aptitude install gpsbabel
```

## General Information

gpsbabel understands lots of formats and supports lots of filter functions like removing duplicates, sorting waypoints etc. Look for the -x option in the man page.

It is written in C.

## Routes,Tracks,Waypoints

First a general note: GPSBabel distinguishes between routes, tracks and waypoints. So typically you will want to pass one of the following switches on the commandline

- -r Process route information
- -t Process track information
- -w Process waypoint information [default]

If you do a conversion and the output file is empty, it is most likely that you need to use a different of the above switches!

## Examples

### Convert GPX to CSV

```bash
gpsbabel -t -i gpx -f input.gpx -o unicsv -x track,course,speed -F output.csv
```

### Add Speed and Course

If you are processing `track information` use the switch

	-x track,course,speed

### Converting GPS NMEA to GPX

```bash
gpsbabel  -i nmea,date=20090601 -f input.nmea -x transform,wpt=trk -o gpx -F output.gpx
```

Notes:

 * a date (see example above) must be given if your GPS data doesn't contain one already.
 * wpt=trk creates waypoints out of tracks.

### Download tracks from QStarz BT-Q1000

These tracking devices connect via USB. In Ubuntu 10.04 the device should be accessible at /dev/ttyACM0 by default.

Data can then be downloaded (details [here](http://www.cyrius.com/debian/gps/bt-q1000x/)):

```bash
gpsbabel -t -w -i mtk -f /dev/ttyACM0 -o gpx -F out.gpx
```
More documentation in gpsbabel's manpage  and [here](http://www.gpsbabel.org/htmldoc-development/fmt_mtk.html).



# More tools

## mtkbabel

The [OSS mtkbabel](http://www.rigacci.org/wiki/doku.php/doc/appunti/hardware/gps_logger_i_blue_747) (made using Perl) works with Mediatek mtk chipsets, which are eg. part of Qstarz tracking devices. Besides downloading data, allows for scriptable configuration of the device (control settings, change log file layout etc.)

```bash
sudo apt-get install mtkbabel
```

## bt747

This [Java tool](http://www.bt747.org/) aims to provide a GUI for GPS logging device configuration and log data handling. It is available for desktops and mobile devices.

