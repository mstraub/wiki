# Processing Models

## Gotchas

### Input Variables

As of QGIS 3.32 changing the name of input variables breaks your formulas because the formulas are not changed automatically (e.g. in `Field calculator`)!

## Performance

### Spatial Indices

Watch out for `No spatial index exists for points layer, performance will be severely degraded` warning when running your models. Indices are not created automatically - you have to manually add `Create spatial index` in your model (quite often) - at least until this [feature request](https://github.com/qgis/QGIS/issues/38859) is implemented.

## Debug Output

When running models directly from the `Model Designer` the log is more detailed - this allows you to see e.g. which part of the model error messages originate from!

# PyQGIS

Get version (it's not `qgis.__version__`)
```python
import qgis.core
qgis.core.Qgis.QGIS_VERSION
```

## Setup

I did not find a perfect way yet.

### System QGIS

The [PyQGIS Cookbook](https://docs.qgis.org/3.28/en/docs/pyqgis_developer_cookbook/intro.html#running-custom-applications) tells you to simply install QGIS and use the installed version.

Note, that this **does not work with the default system-python of Ubuntu (at least 22.04)** due to 
[issues with package names](https://github.com/python-poetry/poetry/issues/6013).

1. Install QGIS for your platform: https://qgis.org/en/site/forusers/download.html
2. Create the virtual environment:
   - On Windows:
     ```bash
     poetry env use <QGIS installation directory>\apps\Python3x\python.exe
        set PYTHONPATH=<QGIS installation directory>\apps\qgis-ltr\python
        set MYPYPATH=%PYTHONPATH%
     ```
   - On Linux:
     ```bash
     python3 -m venv --system-site-packages .venv
     ```

### Conda

QGIS is available in `conda-forge` and this seems to work on both Linux and Windows.

Create an `environment.yml` like this:
```yml
name: the_name
channels:
  - conda-forge
  - nodefaults
dependencies:
  - python=3.11
  - qgis=3.32.3
  - pyqt=5.15.9
```

And then run `conda env create -f environment.yml`


