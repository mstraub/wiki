Our favorite open map of the world :)

# Download
## Planet Dumps

Check download possibilities at [planet.osm.org](https://planet.osm.org/). Typically we don't need data for the whole planet but just a small part of it.

A reliable source for pre-cut country-sized files is [geofabrik.de](https://download.geofabrik.de/openstreetmap/), check out [BBBike](https://download.bbbike.org/osm/) for getting an extract of custom polygons.

## Statistics

```bash
wget -O - http://m.m.i24.cc/osmfilter.c |cc -x c - -O3 -o osmfilter # found at http://wiki.openstreetmap.org/wiki/Osmfilter
```

```bash
./osmfilter estonia.osm --out-count
```

# Overpass API

As of 2023 this is the go-to-way for querying data.
Doc: [Overpass Query Language](https://wiki.openstreetmap.org/wiki/Overpass_API/Overpass_QL), https://dev.overpass-api.de/output_formats.html

## Directly use the API

Example on how to query via `curl`:
```bash
curl -o monaco-shops.json -d "[out:json][timeout:30][bbox:43.737,7.42,43.74,7.43];node[shop];out;" https://overpass.kumi.systems/api/interpreter
```

For complex queries put it in an external file and call `curl` like this:
```bash
curl -o wien-ubahn.json -d @wien-ubahn.overpassql https://overpass.kumi.systems/api/interpreter
```

Contents of `wien-ubahn.overpassql`:
```
[out:json][timeout:120][bbox:48.11,16.23,48.29,16.53];
(
  nwr["railway"="station"]["station"="subway"];
);
out center;
```


## Overpass Turbo

The simplest way to use the API is with the browser-based **Overpass Turbo**: [Github](https://github.com/tyrasd/overpass-turbo) [stable (outdated?)](http://overpass-turbo.eu/), [current dev version](https://tyrasd.github.io/overpass-turbo).

It's quite easy to use but has these drawbacks:
1. you always query the current OSM database (what if you want to query older data?)
2. the public servers have easy-to-reach quotas
3. since results are first displayed in the browser before you can even download them this can only be used for small datasets

These drawbacks can be solved by using your own instance of the Overpass API.

## DIY Overpass

To automate and scale up a manual data gathering task involving the wonderful Overpass API we need to 
1. host our own Overpass server
2. query it with a script
3. convert OpenStreetMap JSON to GeoJSON

### Self-hosting Overpass

There is a convenient docker image
https://github.com/wiktorn/Overpass-API/ on https://hub.docker.com/r/wiktorn/overpass-api
for a local overpass instance.

It is easily set up for hosting an up-to-date file directly from geofabrik.
But we want to specify a specific file:

#### Initialize with local file

To use a local file we have to

1. make the file available via http locally
2. allow the docker container access to localhost

So.. let's start:

1. Create an on-the-fly webserver in the director containing the `.osm.pbf`

   ```bash
   python -m http.server 8000
   ```

2. Optionally disable the firewall - otherwise from within the container the request timed out

   ```bash
   sudo systemctl stop ufw
   ```
  
3. Get the docker image

   ```bash
   docker pull wiktorn/overpass-api 
   ```

4. Run the image. In this case we
   - added the "magic" internal host to access localhost from within the container (which is normally not possible)
   - convert the `.osm.pbf` to `osm.bz2` (because Overpass expects this format)
   - disable updates by setting MAX_INT as update rate

```bash
docker run \
    --add-host host.docker.internal:host-gateway \
    -e OVERPASS_MODE=init \
    -e OVERPASS_META=yes \
    -e OVERPASS_PLANET_URL=http://host.docker.internal:8000/austria-2023-10-03.osm.pbf \
    -e OVERPASS_PLANET_PREPROCESS='mv /db/planet.osm.bz2 /db/planet.osm.pbf && osmium cat -o /db/planet.osm.bz2 /db/planet.osm.pbf && rm /db/planet.osm.pbf' \
    -e OVERPASS_RULES_LOAD=1 \
    -e OVERPASS_UPDATE_SLEEP=2147483647 \
    -v overpass-austria-db:/db \
    -p 8080:80 \
    -i -t \
    --name overpass-austria \
    wiktorn/overpass-api
```

   For the Austria export this took ~40 minutes on my laptop.
   
5. Start the container again with `docker container start overpass-austria` and start querying.

## Convert OpenStreetMap JSON to GeoJSON

For a dataset only containing nodes this might be overkill, but in case of polygons and relations it is worthwhile.

- JavaScript / CLI (the library used in Overpass Turbo): [osmtogeojson](https://github.com/tyrasd/osmtogeojson)
- Python: [osm2geojson](https://github.com/aspectumapp/osm2geojson)

# Tools
## osmium

[osmium](https://osmcode.org/osmium-tool/) is a multipurpose CLI tool based on the Osmium library(C++) to work with raw OSM data

Simply convert `PBF` to `XML`
```bash
osmium cat in.pbf out.xml
```

Filter by id (**n**ode, **w**ay, **r**elation)
```bash
osmium getid in.pbf n9906763597 w123 r99 -o filtered.xml
```

Filter by tag (everything you need for routing)
```bash
# only keep geometries for the road, rail and waterway network + turn restrictions + public transport relations
osmium tags-filter in.pbf "nw/highway" "nw/barrier" "nw/railway" "nw/waterway" "nw/aerialway" "r/restriction" "r/route=bus,tram,light_rail,train,railway,ferry" -o out.pbf
```

Filter by bounding box
```bash
osmium extract -b 2.25,48.81,2.42,48.91 in.pbf -o within_bbox.pbf
```


## osmosis

[osmosis](https://wiki.openstreetmap.org/wiki/Osmosis) is a Java-based CLI tool for working with raw OSM data

> [!WARNING] **outdated**! Use `osmium` if possible.

Cut a bounding box from a pbf file:

```bash
osmosis --read-pbf file=austria-2017-08-22.osm.pbf \
		--buffer --bounding-polygon file="resources/vienna-synarea2.poly" clipIncompleteEntities=true \
		--write-pbf file="osm_vienna-synarea2_2017-08-22.pbf"
```

## other ways..

[ogr2ogr](ogr2ogr) can also convert OSM data.