> [!tldr] `DiscreteModeChoice` is a powerful mode innovation module that can be used instead of `SubtourModeChoice`
 
Why this is more powerful than `SubtourModeChoice`, which changes the mode for **all legs of a subtour** (=start and end location with same link id):
- modes within a subtour can be mixed (e.g. for the subtour home-work-home an agent can use ride-pt)
- allow modes only within a polygon (useful e.g. for service-area based drt)
- flexible definition of what a subtour is

Great docs: https://github.com/matsim-org/matsim-libs/tree/master/contribs/discrete_mode_choice/docs

# Configuration

The first choice to make is to either use `modelType` `Tour` or `Trip`.

- in order to use `<param name="tourEstimator" value="MATSimDayScoring" />` pt must be routed (not teleported)!
- using route calculations in the estimator with `<param name="tourEstimator" value="MATSimDayScoring" />` and `<param name="selector" value="MultinomialLogit" />` quickly fills 128GB of RAM (at least for our Salzburg 2023.2 model) -> subtours with a lot of activities could be the culprit due to combinatoric explosion

> [!todo] TODO - try out the routing estimator:
> `<param name="tourEstimator" value="MATSimDayScoring" />` together with `<param name="selector" value="MultinomialLogit" />` 

# Gotchas

## Combinatorial explosion

The combination of **many modes** with **long subtours** quickly leads to combinatorial explosion because all possible mode combinations for all subtours are calculated before the filter phase.

E.g. for a model with 200k agents, 11 modes of transport and subtours with up to 10 activities 110 GB of RAM were quickly filled up and crashed:
- 377M `DefaultTripCandidate` instances
- 37M `DefaultTourCandidate` instances

After reducing the maximum number of activities per subtour to 6 (by editing the population) the whole `DiscreteModeChoice` innovation finished in a few seconds!

# Issues and Limitations

- simply filtering trips by distance and mode does not seem to be possible
    - e.g. I would like to limit walking trips to 5 km (beeline would be OK)
- The `subtourModeConstraint` seems buggy, I got an exception in `SubtourModeConstraint.validateBeforeEstimation` for a trivial test case with a HWHLH agent

# Literature

- Hörl, S., M. Balac and K.W. Axhausen (2019) [Pairing discrete mode choice models and agent-based transport simulation with MATSim](https://www.research-collection.ethz.ch/handle/20.500.11850/303667), presented at the 98th Annual Meeting of the Transportation Research Board, January 2019, Washington D.C.
- Hörl, S., M. Balac and K.W. Axhausen (2018) [A first look at bridging discrete choice modeling and agent-based microsimulation in MATSim](https://www.sciencedirect.com/science/article/pii/S1877050918304496?via%3Dihub), _Procedia Computer Science_, **130**, 900-907.

see https://github.com/matsim-eth/discrete-mode-choice/tree/develop for more related publications