The optimizer of the taxi module can be configured in different ways ("type" in the config):

* regular taxis: ASSIGNMENT, FIFO, RULE_BASED, ZONAL
* e-taxis: E_RULE_BASED, E_ASSIGNMENT

**Rule based**: in this context rule based means, that a "simple rule based algorithm" such as first come first serve is used to assign customers to taxis.

The following implementations ("goal" in the config) are available:

* MIN_WAIT_TIME
* MIN_PICKUP_TIME
* DEMAND_SUPPLY_EQUIL: mixture of assigning nearest-idle-taxi with nearest-open-request in undersupply situations (i.e. when not enough taxis are free). This goal should perform reasonably well.

**Assignment**: in comparison to the rule-based approach, which only works with local knowledge, the assignment approach uses global knowledge to achieve even better results.

* PICKUP_TIME
* ARRIVAL_TIME: less fairness, higher throughput
* TOTAL_WAIT_TIME: more fairness, lower throughput
* DSE: balance between demand (ARRIVAL_TIME) and supply (PICKUP_TIME)