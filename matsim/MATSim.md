> [!tldr] [MATSim](https://matsim.org/docs) - aka Multi-Agent Transport Simulation - is a [modular](https://matsim.org/extensions/) open source micro/meso-simulation written in Java.
> Its purpose is to solve the problem of traffic assignment, which means
adapting and optimizing a given set of traffic demand specifications to a
defined traffic supply and it's constraints. The demand is defined as
travel and activity plans of a population of agents.

Documentation can be found [here](https://matsim.org/docs/), most noteworthy:

* [MATSim book](https://matsim.org/the-book), [MATSim User Guide](https://matsim.org/docs/userguide) (i.e. the first chapters of the book, but always up to date)
* [MATSim Wiki](https://matsim.atlassian.net/wiki/spaces/MATPUB/pages/108612949/Past+Events) (e.g. slides from user meetups)
* [DTDs](http://www.matsim.org/files/dtd/) for config and input files
* [source code](https://github.com/matsim-org) (most importantly the [matsim-code-examples aka tutorial](https://github.com/matsim-org/matsim-code-examples))

**Glossary**
- **agent:** a simulated person as the elementary part of the population
- **activity:** an agent's activity divided into two types:
    1. "real" activity: e.g. home, work, shopping
    2. "stage" activity: dummy for switching the mode of transport
- **leg:** route between any two activities (including _stage_ activities)
- **trip:** route between two _real_ activities (excluding mode switches),
  consisting of 1 or more legs (see `TripStructureUtils.getTrips`)
- **subtour**: two or more trips that form a loop, i.e. the start facility/link/coordinate of the first trip is the end of the last trip (see `TripStructureUtils.getSubtours`, and note, that subtours can be *nested*!)
    - The [DiscreteModeChoice Module](matsim/DiscreteModeChoice%20Module.md) can be configured on what determines a tour - and none of the options are consistent with `TripStructureUtils`: https://github.com/matsim-org/matsim-libs/blob/master/contribs/discrete_mode_choice/docs/components/TourFinder.md
- **plan:** multiple trips representing the day of an agent
- **MATSim iteration:** Numerical equilibrium search methods, such as MATSim, are iterative. A MATSim run is thus composed of a configurable number of iterations. In an iteration the plans of all agents are simulated.
- **MATSim run:** a full simulation, i.e. a configurable *set of iterations*, typically ending with an equilibrium solution of transport supply and demand.

# Extensions

## Contribs

The source for all open source contributed modules (or extensions) is here: https://github.com/matsim-org/matsim/tree/master/contribs. Use this repo and the [javadoc overview](https://matsim.org/javadoc) to get a feeling for the activity / recentness of these modules.

The [contribution guidelines](https://matsim.org/docs/contributing/extensions) say that a module should provide an example script with named `RunXXX`. This is usually a good starting point to investigate how it works.

Note: the officially recommended way to use contrib modules is from within a maven project / Java main method - and no longer the MATSim GUI.

Here is an overview of potentially interesting modules (as of MATSim v13):

| name       | author                               | description                                                                                                                                                                                    | documentation                                                                                                                                                                                |
| ---------- | ------------------------------------ | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ~~av~~     | Joschka Bischoff                     | autonomous vehicles (based on taxi and drt module), provided fare calculation for taxi and drt, as of MATSim v13 this is integrated into drt and taxi and only a collection of examples remain | paper: [Simulation of City-wide Replacement of Private Cars with Autonomous Taxis in Berlin](https://www.sciencedirect.com/science/article/pii/S1877050916301442)                            |
| ev         | Michal Maciejewski                   | electric vehicles ([since 2019](https://github.com/matsim-org/matsim/pull/466), made transEnergySim obsolete)                                                                                  |                                                                                                                                                                                              |
| taxi       | Michal Maciejewski                   | taxi service as dynamic transport service (with within-day replanning), fare calculation, supports electric vehicles                                                                           | paper: [An Assignment-Based Approach to Efficient Real-Time City-Scale Taxi Dispatching](https://www.depositonce.tu-berlin.de/bitstream/11303/9172/1/maciejewski_etal_2016.pdf)              |
| drt        | Michal Maciejewski                   | **d**emand-**r**esponsive **t**ransport; supports ride-sharing, diversions (picking up new customers on the way), stop-based and door2door operation, fare calculation, and electric vehicles  |                                                                                                                                                                                              |
| dvrp       | Michal Maciejewski                   | base module for solving **d**ynamic **v**ehicle **r**outing **p**roblems such as taxi tours                                                                                                    | MATSim book: chapter 23 - Dynamic Transport Services                                                                                                                                         |
| carsharing | Milos Balac                          | simulate station based (one-way, round-trip) or freefloating carsharing; no support for ride-sharing                                                                                           |                                                                                                                                                                                              |
| bicycle    | Dominik Ziemke                       | respect slope, cycling infrastructure,.. when calculating bicycle routes                                                                                                                       | paper: [Modeling bicycle traffic in an agent-based transport simulation](https://www.researchgate.net/publication/317549541_Modeling_bicycle_traffic_in_an_agent-based_transport_simulation) |
| minibus    | Andreas Neumann                      | takes demand and infrastructure to create suggestions for minibus lines                                                                                                                        | dissertation: [A paratransit-inspired evolutionary processfor public transit network design](http://nbn-resolving.de/urn/resolver.pl?urn:nbn:de:kobv:83-opus4-53866)                         |
| multimodal | Christoph Dobler                     | focuses on correctly modelling walking/cycling speeds based on age, slope and trip purpose. no development since 2014                                                                          | MATSim book: chapter 21 - The "Multi-Modal" Contribution                                                                                                                                     |
| parking    | Rashid A. Waraich, Joschka Bidschoff | simulate car parking, i.e. modeling choice of parking spot and simulating                                                                                                                      | MATSim book: chapter 13 - Parking                                                                                                                                                            |


### Module dependencies 

as of MATSim v13 modules depend on each other like this. (number = lines of code)

```mermaid
graph TD;
    analysis((analysis<br/>9.4k)) 
    av((av<br/>0.9k)) 
    bicycle((bicycle<br/>2.8k)) 
    cadytsIntegration((cadytsIntegration<br/>4.8k)) 
    carsharing((carsharing<br/>9.9k)) 
    drt((drt<br/>18.7k)) 
    dvrp((dvrp<br/>19.1k)) 
    emissions((emissions<br/>14.4k)) 
    ev((ev<br/>6.6k)) 
    freight((freight<br/>17.6k)) 
    locationchoice((locationchoice<br/>8.8k)) 
    minibus((minibus<br/>27.8k)) 
    multimodal((multimodal<br/>5.2k)) 
    parking((parking<br/>12.4k)) 
    roadpricing((roadpricing<br/>4.6k)) 
    sbb-extensions(("sbb-extensions<br/>4.4k")) 
    signals((signals<br/>28.6k)) 
    taxi((taxi<br/>9.9k));

    av --> dvrp;
    av --> drt;
    av --> taxi;
    drt --> dvrp;
    drt --> ev;
    taxi --> dvrp;
    taxi --> ev;
    ev --> dvrp;
    dvrp --> locationchoice;
    dvrp --> sbb-extensions;
    carsharing --> dvrp;
    parking --> multimodal;
    parking --> dvrp;
    emissions --> analysis;
    locationchoice --> analysis;
    cadytsIntegration --> analysis;
    analysis --> roadpricing;
    freight --> roadpricing;
```

Use this code for future updates:
```bash
for dir in `ls -1 -d */`; do count=`find $dir -name "*.java" | xargs wc -l | tail -1 | awk '{$1=$1};1'`; echo "${count} in ${dir}" >> contrib_loc.txt; done;
sort -t " " -k 1 -n  contrib_loc.txt
```


# Useful External Tools

## PT2MATSim

> [!tldr] PT2MATSim is a package to convert public transit data from GTFS, HAFAS or OSM to a completely mapped MATSim schedule.

https://github.com/matsim-org/pt2matsim

## SUMO netconvert

Can also extract a MATSim network from OSM (but allows many more source data and output formats)
used by the [Open Berlin scenario](https://github.com/matsim-scenarios/matsim-berlin/blob/6.0/Makefile)

https://sumo.dlr.de/docs/netconvert.html

## OSMOX

> [!tldr] A tool for extracting facility locations and features from OpenStreetMap (OSM) data

https://github.com/arup-group/osmox

- geht (zumindest für home) von Gebäudeumrissen aus
- distance to nearest * wird mit euklidischer Distanz berechnet

Unser Ansatz mit BEV Adressen etc ist deutlich genauer.

## PAM (population synthesis)

> [!tldr] PAM is a python library for population **activity sequence** modelling (creation and modification of agents' daily plans)

https://github.com/arup-group/pam


## Simunto VIA

[VIA](https://www.simunto.com/via/download) is a commercial standalone application with a free version that is **very** limited in functionality.
It comes with good [docs](https://docs.simunto.com/via/). [Download beta versions](https://simunto.com/via/download-beta).

MATSim only features a minimalistic GUI to start the simulation, which produces an `output` folder with several xml files, most notably `events.xml`.

To view the results there are currently two options: OTFVis (open source but limited) and Simunto VIA (commercial, free limited version).

### Show Simulation Results

* In the `Data Sources` tab click the cog in the bottom left and configure the `Automatically Create Layers` options.
* Drag and drop the simulation results `./output/output_*.xml.gz` into VIA.
* Now these are loaded as `Data Sources` as well as `Layers` depending on your configuration.
* In the `Layers` tab and click `Load Data` to load agents and vehicles. Optionally click the `+` button on the bottom left to add layers that were not automatically created.

Now the time line on the bottom should show times. With the slider on the bottom you can replay the simulation.

### Follow a Specific Person

You can follow agents, which are either agents from your simulation, or drivers of public transport vehicles.

* In `Layers` click on the `Identify agents..` button.
* On the right side a `Queries` panel should pop up.
* There type the id of your agent into the text field (below `Show Agent Plan`)

To view the plans of several agents at once use the favorite function.

### Scripting

VIA supports scripting with JavaScript. You can use this to quickly view the current simulation results on startup.

Check out the \*.via.js scripts in our playground repo.

## Simunto Tramola

https://www.simunto.com/tramola/
Non-free Java-based tool with a web-based UI with these features:

- Network editor (car + transit)
- Scenario run orchestration (but not yet for SLURM clusters)
- Analysis (aka dashboards, but until now without defaults, really tedious to set up)

## Replan.city

similar to Tramola, also non-free (actually quite expensive)

Cloud-first **non-free** [tool](https://replan.city/) that aims to provide a complete simulation pipeline (from scenario editing to result analysis).
Some of the features in detail:
- import existing scenarios (drag & drop xml files)
- interactive editing of road and transit network/schedule (including versioning of variants of the networks)
- run simulations on the google cloud (Slurm login node)
- **population generation** (only requires a road network + traffic zones shapefile. pois (osm or custom), matrices (tomtom or custom)) including **calibration** (car volumes, transit volumes, or modal split)

# Getting Started

## Transport Graph Creation

The MATSim core provides some network readers to create the transport graph `network.xml`. An example is `org.matsim.core.network.io.NetworkReaderTeleatlas` for Teleatlas maps.

For our use cases OpenStreetMap is the most relevant source. The MATSim ecosystem features several relevant implementations:

* The MATSim-core OSM converter `org.matsim.core.utils.io.OsmNetworkReader` can extract a unimodal car network ignoring turn/access restrictions (see also https://matsim.atlassian.net/browse/MATSIM-723)
* The contrib-module `bicycle` extends the core converter: `org.matsim.contrib.bicycle.network.BicycleOsmNetworkReaderV2` and extracts a unimodal bicycle network respecting cycling infrastructure, road surface and slope (from DEM) but also ignores turn/access restrictions
* The JOSM MATSim plugin allows for direct extraction of a unimodal car network from within JOSM and implements its own converter `org.matsim.contrib.josm.model.NetworkModel.` (Note: development seems to have halted)
* [pt2matsim](https://github.com/matsim-org/pt2matsim/) can create a multi-modal graph from OSM (road network) + GTFS (public transport) + HAFAS (public transport). The road network extraction is different from the core OSM converter.

The best option (also the one with the most recent development activity as of 2019) is to use pt2matsim. However, no option for creating a fully multimodal graph (including bicycle and foot traffic) is available!

### pt2matsim

See the [github repsitory](https://github.com/matsim-org/pt2matsim/) for code and issues.

Note, that the `org.matsim.pt2matsim.osm.OsmMultimodalNetworkConverter` has some deficiencies ([see this issue on Github](https://github.com/matsim-org/pt2matsim/issues/75)):

* it only extracts the car-network (i.e. no bike, walk)
* access restrictions are not handled at all
* turn restrictions are not handled at all

#### (1) Get OSM Extract

Since the MATSim simulation uses the number of car driving lanes to estimate the maximum flow you should check the coverage of lane information in your area of interest. Using the following Overpass query either in [JOSM](https://josm.openstreetmap.de/) or [Overpass Turbo](http://overpass-turbo.eu) shows you where no [lane tag](https://wiki.openstreetmap.org/wiki/Key/lanes) is set on the most important roads. If you want to check even more roads consider adding tertiary to the query.

```
[out:xml][timeout:90][bbox:{{bbox}}];
(
way
["highway"~"motorway.*|trunk.*|primary.*|secondary.*"]
[!"lanes"];
);
(._;>;);
out meta;
```

For an even more detailed view use [JOSM](https://josm.openstreetmap.de/) and the map paint style 'Lane and road attributes' (`Edit > Preferences > Third icon from top > Map Paint Styles`) to see the number of lanes (and even turn lanes).

If feasible fix missing or wrong information directly in OSM. Then download and cut relevant OSM data. You may need to wait one day after fixing things in OSM for the changes to be reflected in the Geofabrik extracts.

Now you have an .osm.pbf file containing a lot of things not required for MATSim.

Therefore we filter out the transport network and convert the result to an .osm (xml) file with [osmium](https://osmcode.org/osmium-tool/).

```bash
sudo apt install osmium
# only keep geometries for the road, rail and waterway network + turn restrictions + public transport relations
osmium tags-filter "$osm_pbf" "nw/highway" "nw/barrier" "nw/railway" "nw/waterway" "nw/aerialway" "r/restriction" "r/route=bus,tram,light_rail,train,railway,ferry" -o "$filtered_osm_xml" -v
```

#### (2) Get GTFS data

Get the relevant public transport schedules, see [GTFS](GTFS.md) 

#### (3) Create a MATSim network.xml

MATSim requires a `network.xml`, which can not only contain roads for cars but also roads for bicycles and public transport. Using the [pt2matsim](https://github.com/matsim-org/pt2matsim/) project we can convert the data sets from the previous steps into a MATSim network.

##### Simple use cases: via CLI

For very simple use cases simply download the latest `pt2matsim-*-shaded.jar`, i.e. the standalone version with all dependencies, and run the [command of your choice](https://github.com/matsim-org/pt2matsim/tree/master/src/main/java/org/matsim/pt2matsim/run) like this:

```bash
java -cp pt2matsim-*-shaded.jar org.matsim.pt2matsim.run.CreateDefaultOsmConfig `<CONFIG_FILE>`
java -cp pt2matsim-*-shaded.jar org.matsim.pt2matsim.run.Osm2MultimodalNetwork `<CONFIG_FILE>`
```


#### GEOJSON export

To convert a network to a GIS format usable in e.g. QGIS you can also use pt2matsim:

```bash
java -cp pt2matsim-*-shaded.jar org.matsim.pt2matsim.run.gis.Network2Geojson WGS84 `<NETWORK_FILE>` `<LINKS_GEOJSON>` `<NODES_GEOJSON>`
```

Use WGS84 if the network is in WGS84 coordinates or the appropriate EPSG string otherwise (e.g. EPSG:31256)


## Creating Scenarios

### config.xml

The heart of each scenario is the `config.xml`.

A good starting point to see current configuration options for the current MATSim version is to run

```bash
java -cp matsim-0.10.1.jar org.matsim.run.CreateFullConfig fullConfig.xml
```

Another very helpful feature is, that MATSim writes the full configuration built when running a scenario from your config and the provided defaults (1) to the standard output and (2) into `output/output_config.xml`.

See also MUG chapter 2.2.2 (Typical Input Data).

The MATSim documentation recommends to write a minimalistic `config.xml` and let the parser fill in the defaults. This works as expected for modules: modules not mentioned in your config are still loaded - and also for paramters of modules: if you do not set them they are set to default valules.

However, **be aware, that parametersets** **are replaced completely**! E.g. to add a scoring configuration for a new mode of transport to the `planCalcScore` module this does not work:

```xml
<!-- THIS WILL FAIL! -->
​<module name="planCalcScore">
    ​<parameterset type="scoringParameters">
        ​<parameterset type="modeParams">
            <param name="mode" value="myNewMode" />
            <!-- ... other parameters for your mode -->
        </parameterset>
    </parameterset>
</module>
```

Instead you have to copy the whole parameterset from the default config and then add your mode.

#### Coordinate Reference System (CRS)

In order to convert coordinates of the MATSim network to WGS84 coordinates we need to know which CRS is used.
The [best practice](https://github.com/matsim-org/matsim-code-examples/issues/63#issuecomment-451086182) is as follows:

The `global` module of your scenario config must specify the CRS as *lowercase* [epsg code](https://epsg.io):
```xml
<module name="global" >
    <param name="coordinateSystem" value="epsg:31256" />
</module>
```

In each file containing coordinates (such as `network.xml` and `population.xml`) add an attribute section:
```xml
<attributes>
    <attribute name="coordinateReferenceSystem" class="java.lang.String" >epsg:31256</attribute>
</attributes>
```

The `inputCRS` attributes in the `config.xml` (e.g. for the network module) do not have to be set.
The values specified in the respective files (e.g. `network.xml`) take precedence anyways.

> [!warning] Always make sure the link id and the x and y coordinates in your plan match!
> This is mostly an issue when hand-crafting test plans (i.e. forgetting to change x&y when changing the link id)

#### Vehicles

To define vehicle properties for all vehicles of the same mode use `modeVehicleTypesFromVehiclesData` in qsim
```xml
<module name="qsim">
    <param name="vehiclesSource" value="modeVehicleTypesFromVehiclesData" />
</module>
```

together with a vehicle definition file
```xml
<module name="vehicles">
    <param name="vehiclesFile" value="vehicleDefinitions.xml" />
</module>
```

that contains a definition for all vehicle based modes in the simulation:
```xml
<?xml version='1.0' encoding='UTF-8'?>
<vehicleDefinitions xmlns="http://www.matsim.org/files/dtd"
	xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.matsim.org/files/dtd http://www.matsim.org/files/dtd/vehicleDefinitions_v2.0.xsd">
	<!-- e.g. specify a speed limit-->
	<vehicleType id="car">
    	<maximumVelocity meterPerSecond="8.3"/>
	</vehicleType>
</vehicleDefinitions>
```


#### Changes from v15 to v16

In MATSim v16 config group naming has been overhauled, see https://github.com/matsim-org/matsim-libs/pull/2852

- controler → controller
- planCalcScore → scoring
- planscalcroute → routing
- strategy → replanning
- parallelEventHandling → eventsManager

### network.xml

This contains the street network and bus lines (and timetables) for route calculation

### population.xml

The (typically) synthesized population defines the agents, their attributes (which can be freely defined as needed - see also MUG Chapter 45.4 (Extension Points Related to Scenario)) and their original schedule, i.e. activities and trips/legs.

```xml
<?xml version='1.0' encoding='UTF-8'?>
<!DOCTYPE population SYSTEM "http://www.matsim.org/files/dtd/population_v6.dtd">
<population>
  <person id="4_10767_2_2">
    <attributes>
      <attribute class="java.lang.String" name="sex">f</attribute>
      <attribute class="java.lang.Integer" name="age">33</attribute>
      <attribute class="java.lang.Boolean" name="employed">false</attribute>
      ...
    </attributes>
    <plan>
      <activity end_time="10:15:00" facility="h278254" type="home" x="5941.5187869254" y="342214.520205923"/>
      <leg mode="walk"/>
      <activity end_time="12:00:00" facility="node/337947389" start_time="10:20:00" type="leisure" x="3573.90216766016" y="342310.4343706"/>
      <leg mode="walk"/>
      <activity end_time="16:00:00" facility="h278254" start_time="12:07:00" type="home" x="5941.5187869254" y="342214.520205923"/>
      <leg mode="walk"/>
      ...
    </plan>
  </person>
  ...
</population>
```


## Running the Simulation

The ways to download and run MATSim are described nicely on their [download page](https://matsim.org/downloads/).

From our (little) experience: using Eclipse to run MATSim is useful for (1) debugging, (2) simply using a recent snapshot version, and (3) for running scenarios that do not solely rely on the MATSim core but e.g. use contrib modules.

### Basic Run

1. Run the MATSim GUI (`org.matsim.run.gui.Gui`), e.g.
   - `java -jar matsim-0.10.1.jar`
   - `java -cp matsim-0.10.1.jar org.matsim.run.gui.Gui`
2. Select a `config.xml` file that must reference at least a transport network (`network.xml`) and a population/trips (`plans.xml`)
3. Run the simulation, which will produce an `output` folder, most importantly with `events.xml`, which is the documentation of the micro-simulation
4. Use a visualization tool (OTFVis or VIA) or check `events.xml`

The simulation consists of iterations of the following cycle, where initial demand is represented by the original `plans.xml`, and analyses are based on `events.xml`:

1. Mobsim (module: qsim): executes agent's plans in a synthetic reality
2. Scoring (module: planCalcScore): The actual performance of the plan in the synthetic reality is taken to compute each executed plan’s score
3. Replanning (module: strategy, TimeAllocationMutator, ChangeTripMode, ReRoute,…):
   - Remove a plan if maximum was exceeded (choice set reduction)
   - Create a new plan for some agents (choice set extension)
   - All other agents choose between their plans (choice)

![](matsim-mobsim-cycle.png)

See also: MUG chapter 5.1 (Good Plans and Bad Plans, Score and Utility) and MUG chapter 4.6.1 (Plans Generation and Removal (Choice Set Generation)).

### Details on Replanning

See also MUG chapter 4.6 (Replanning Strategies) and `org.matsim.core.replanning` in the code.

In short:

* one plan is always selected (that's the one to be used in qsim)
* the plan's score is the result of qsim (the higher the score, the better)
* the plan may contain non-matching times (e.g. an activity ends and the next leg starts earlier). In this case have a look into the events.xml to see the real result of the simulation!

```xml
<plan score="126.74108710738955" selected="no">
    <activity type="home" link="190" x="3700.4566132902282" y="343947.60646178015" end_time="09:11:56">
    </activity>
    <leg mode="car" dep_time="08:45:00" trav_time="00:01:25">
        <route type="links" start_link="190" end_link="1429" trav_time="00:01:25" distance="1108.9791877518649">190 191 373 371 1018 1019 1020 1455 1454 1999 717 1768 1463 447 160 161 453 451 449 1430 1429</route>
    </leg>
    <activity type="work" link="1429" x="3360.65769939492" y="344813.2302469881" start_time="09:00:00" end_time="16:27:45">
    </activity>
    <leg mode="car" dep_time="16:00:00" trav_time="00:01:16">
        <route type="links" start_link="1429" end_link="190" trav_time="00:01:16" distance="1063.0723137715">1429 448 1655 1656 873 444 1468 655 156 716 1459 1462 164 165 1456 1444 372 190</route>
    </leg>
    <activity type="home" link="190" x="3700.4566132902282" y="343947.60646178015">
    </activity>
</plan>
```

Some basic replanning strategies provided by MATSim are:

#### Time

The `TimeAllocationMutator` can change start and end times of activities. Note, that the activities' time frames can and will be violated! It is up to the scoring to get rid of these plans over the course of several simulation iterations.

#### Route

With the `ReRoute` strategy agents search for alternative routes when they encounter congestion.
Note, that this only applies to network modes (not teleported modes).

#### Mode of Transport

The modes of transport used in a plan can be changed as well.

- `ChangeTripMode`: changes the mode for all legs in the plan
- `ChangeSingleTripMode`: randomly changes the mode for one leg
- `SubtourModeChoice`: changes mode for all legs in a subtour (a loop: start and end location must refer to the same link id). Note, that intermodality is not supported!
- [DiscreteModeChoice](matsim/DiscreteModeChoice%20Module.md): the most advanced option

# Lessons Learnt

To see if model result is sane check:
1. Modal split (in general, but also by distance categories / purpose / subpopulation)
2. Network load (i.e. where do the cars go)
3. Total distances / travel time (in general but also by mode)

**Network**
- make sure to set custom speed limits in `pt2matsim`, the defaults are way too low (e.g. 15 kph for residential roads)

**Cordon agents**
- must start on motorways (otherwise they just cause traffic jams at the insertion point and do not reach their target)
- must have `ReRoute` and `SelectExpBeta` as their only strategies
- `pt` cordon points should also be reachable via road network (otherwise issues with matching coordinates to links may arise, e.g. in `PersonPrepareForSim`)

**Scenario scaling** (i.e. when simulating only 25% of the population the road and transit network need to be scaled down as well), see [these](https://github.com/matsim-org/matsim-code-examples/issues/441) [discussions](https://github.com/matsim-org/matsim-code-examples/issues/395)
- properties of cars stay the same
- reduce road network capacity
    - adjust `qsim.flowCapacityFactor` and `qsim.storageCapacityFactor` in `config.xml`
    - the network itself does not need to be changed
- reduce transit capacity 
    - adjust `passengerCarEquivalents`  and capacity (e.g. `<capacity seats="70" standingRoomInPersons="0">`) in `transit_vehicles.xml`
    - the transit timetable does not change, still 100% of all transit vehicles (and trips and lines) are simulated
    - potentially consider `config.qsim().setPcuThresholdForFlowCapacityEasing();`, which is a "secret" switch that not available in `config.xml`

**Model issue detection**
- Check how many agents get stuck (by mode) by having a look at `events.xml`  (or using the Stuck Agent Analysis in Via, which is more detailed: apparently for agents sitting in a transit vehicle that is stuck no event is emitted at the end!)
   ```xml
  <event time="66205.0" type="stuckAndAbort" person="xxx" link="yyy" legMode="car" />
  ```
- Vehicle teleporting, i.e. entries like this in the log: `teleport vehicle xxx from link yyy to link zzz`

# Exploring further..

## Modelling Bicycle Traffic

The simulation behavior regarding bicycle traffic can be configured. The options are

* beeline teleporting
* car-route teleporting
* ~~predefined distance/speed teleporting~~ NOT WORKING!
* full simulation on bicycle graph

Only modes defined in `config>qsim>mainMode` are simulated, by default this is `car`.

Other modes are teleported by default, see `config>qsim>vehicleBehavior` and `config>planscalcroute`. See MUG chapter 7.1 (Other Modes than Car - Routing Side). The default teleportation options are implemented in `org.matsim.core.config.groups.PlansCalcRouteConfigGroup`.

### Beeline teleporting

This is the default. No routes are calculated but instead the beeline between start and destination is used. See MUG chapter 7.1.1.2 (Teleportation …)

Required network: car only

Exemplary config for the `planscalcroute` module:

```xml
<module name="planscalcroute">
    <!-- All the modes for which the router is supposed to generate network routes (like car) -->
    <param name="networkModes" value="car" />
    <parameterset type="teleportedModeParameters">
        <param name="beelineDistanceFactor" value="1.3" />
        <param name="mode" value="bike" />
        <!-- Free-speed factor for a teleported mode. Travel time = teleportedModeFreespeedFactor * <freespeed car travel time>`. Insert a line like this for every such mode. Please do not set teleportedModeFreespeedFactor as well as teleportedModeSpeed for the same mode, but if you do, +teleportedModeFreespeedFactor wins over teleportedModeSpeed. -->
        <param name="teleportedModeFreespeedFactor" value="null" />
        <!-- Speed for a teleported mode. Travel time = (<beeline distance>` * beelineDistanceFactor) / teleportedModeSpeed. Insert a line like this for every such mode. -->
        <param name="teleportedModeSpeed" value="4.166666666666667" />
    </parameterset>
    ...
</module>
```

### Predefined distance/speed teleporting (NOT WORKING!)

It is easy to misread MUG chapter 7.1.2.2 (So-Called Teleportation). Although it is possible to define generic routes with a predefined distance and travel e.g. for non-main modes these travel times and distances won't survive for long. PersonPrepareForSim.java will eventually "reroute", i.e. calculate a new travel time with whichever teleportation type is configured.

```xml
<leg mode="walk">
    <route type="generic" trav_time="00:30:30" distance="3030" />
</leg>
```

See the discussion at https://github.com/matsim-org/matsim-code-examples/issues/97

### Car-route teleporting

Routes and travel times for car are calculated and then the travel time is adjusted with a factor. See MUG chapter 7.1.1.2 (Teleportation …)

Required network: car only

Exemplary config for the `planscalcroute` module:

```xml
<module name="planscalcroute">
    <!-- All the modes for which the router is supposed to generate network routes (like car) -->
    <param name="networkModes" value="car" />
    <parameterset type="teleportedModeParameters">
        <param name="mode" value="bike" />
        <!-- Free-speed factor for a teleported mode. Travel time = teleportedModeFreespeedFactor * <freespeed car travel time>`. Insert a line like this for every such mode. Please do not set teleportedModeFreespeedFactor as well as teleportedModeSpeed for the same mode, but if you do, +teleportedModeFreespeedFactor wins over teleportedModeSpeed. -->
        <param name="teleportedModeFreespeedFactor" value="3" />
        <!-- Speed for a teleported mode. Travel time = (<beeline distance>` * beelineDistanceFactor) / teleportedModeSpeed. Insert a line like this for every such mode. -->
        <param name="teleportedModeSpeed" value="null" />
        <param name="beelineDistanceFactor" value="null" />
    </parameterset>
</module>
```

**Caveat:** Since we can not set a speed cap this may be useful for bus routes, but not for cycling. There is no single factor that works for 30kph zones and 100kph roads at the same time. As long as it is not possible to define a maximum speed for a vehicle this possibility is not useful.

### Full simulation on bicycle graph

Cars and bicycles are fully simulated with qsim. A working example in the MATSim repository is the [equil-mixedTraffic scenario](https://github.com/matsim-org/matsim/tree/master/examples/scenarios/equil-mixedTraffic)

See also the discussion at https://matsim.atlassian.net/wiki/spaces/MATPUB/pages/84246576/Mixed+traffic

Required network: car + bike (Note: currently no way to create this graph from OSM → the current hack is to simply search/replace car with car,bike in the final network) **TODO:** how to properly extract multimodal graph (contributions to pt2matsim?)

For `qsim` it is mandatory to allow cars to overtake bicycles and vice versa, otherwise the simulation results will be completely unrealistic, e.g.:

```xml
<module name="qsim">
    <param name="linkDynamics" value="PassingQ" />
    ...
</module>
```

Read more about link dynamics:

* PassingQ: MUG chapter 7.2.2 (Vehicle Placement and Behavior)
* SeepingQ: Seepage of Smaller Vehicles under Heterogeneous Traffic Conditions (Agarwal + Lämmel 2015)

**Note:** due to a bug/inconsistency you can not use the (default) mode `bike`, but must use a different name such as `bicycle`. Otherwise you will get the following error because the default config contains an entry for `bike` in the section `planscalcroute`:

```plaintext
    1) A binding to org.matsim.core.router.RoutingModule annotated with @com.google.inject.name.Named(value=bike) was already configured at org.matsim.core.router.TripRouterModule.install(TripRouterModule.java:58) (via modules: com.google.inject.util.Modules$CombinedModule -> com.google.inject.util.Modules$CombinedModule -> org.matsim.core.controler.AbstractModule$4 -> com.google.inject.util.Modules$OverrideModule -> org.matsim.core.controler.Controler$1 -> org.matsim.core.controler.ControlerDefaultsModule -> org.matsim.core.router.TripRouterModule).
    at org.matsim.core.router.TripRouterModule.install(TripRouterModule.java:66) (via modules: com.google.inject.util.Modules$CombinedModule -> com.google.inject.util.Modules$CombinedModule -> org.matsim.core.controler.AbstractModule$4 -> com.google.inject.util.Modules$OverrideModule -> org.matsim.core.controler.Controler$1 -> org.matsim.core.controler.ControlerDefaultsModule -> org.matsim.core.router.TripRouterModule)

    1 error
        at com.google.inject.internal.Errors.throwCreationExceptionIfErrorsExist(Errors.java:470)
        at com.google.inject.internal.InternalInjectorCreator.initializeStatically(InternalInjectorCreator.java:155)
        at com.google.inject.internal.InternalInjectorCreator.build(InternalInjectorCreator.java:107)
        at com.google.inject.internal.InjectorImpl.createChildInjector(InjectorImpl.java:232)
        at com.google.inject.internal.InjectorImpl.createChildInjector(InjectorImpl.java:236)
        at org.matsim.core.controler.Injector.createInjector(Injector.java:73)
        at org.matsim.core.controler.Controler.run(Controler.java:189)
        at org.matsim.run.Controler.run(Controler.java:56)
        at org.matsim.run.Controler.main(Controler.java:60)
```

See the discussion of this problem in https://github.com/matsim-org/matsim-code-examples/issues/67 and https://matsim.atlassian.net/wiki/spaces/MATPUB/pages/84246576/Mixed+traffic

## Modelling Intermodal Traffic

Intermodal routing (i.e. legs with different modes for a single trip) are provided by the [SwissRailRaptor Module](matsim/SwissRailRaptor%20Module.md).

### Partial Support for Graph Creation

As of 2020 there is still no fully intermodal graph creation utility available (at least not open source). pt2matsim is already partially intermodal since it generates a graph for car and public transport, but lacks functionality to extract a proper foot and bicycle graph (OSM attribute handling is still lacking).

For most uses of MATSim this is not problematic because cycling and walking are mostly not simulated in detail but simply teleported (without any underlying graph).

MATSim can simulate intermodal plans, even if they only contain legs and no specific routes:

```xml
<population>
    <person id="person1">
        <plan>
            <act type="home" x="5826.522971510246" y="345633.1825425653" end_time="07:30:00" />
            <leg mode="bike" />
            <act type="pt interaction" x="5013.244193707316" y="346597.89143063035" start_time="07:50:00" end_time="07:55:00" />
            <leg mode="pt" />
            <act type="work" x="3407.819979282155" y="344207.6488325801" start_time="08:30:00" end_time="17:00:00" />
        </plan>
    </person>
</population>
```




# Understanding the Code, Issues & Post Mortems

## What are the predefined modes of transport?

Default modes are specified in `org.matsim.api.core.v01.TransportMode`: car, bike, walk, taxi, pt,..

## How does rerouting for main modes work?

The ReRoute strategy uses PlanRouter → TripRouter → RoutingModule, which for car usually uses NetworkRoutingModule → .. → Dijkstra. Dijkstra uses a TravelTime, which is by default fed with data from the TravelTimeCalculator (see the respective section in config.xml). By default during a simulation run the traffic state of the last 15 minutes is averaged and respected in the route calculations! (at least that's what it looks like to me)

## Are modules that are not explicitly added to the config automatically active?

This seems to be the case for all modules that are part of the MATSim core. Check the fully generated config available after the simulation run to see all used modules.

Contributed/external modules are not activated by default but must be added to the config manually. In addition they require explicit activation in the Java code:

```java
List<ConfigGroup> customModules = new ArrayList<>();
customModules.add(new AriadneConfigGroup());
customModules.add(....);
Config config = ConfigUtils.loadConfig(args.getString(Parameters.configXml.toString()), Iterables.toArray(customModules, ConfigGroup.class));
```

Some modules have an explicit switch in the config options to deal with the auto-activation problem, e.g. `multimodal.multiModalSimulationEnabled`

## PersonPrepareForSim

route calculation + link assignment for `plans.xml` as of `v14`

- `PersonPrepareForSim` -> does the actual calc/assignment with a given network by either
  - `PrepareForSimImpl` -> runs before iteration 0 (**complete network used!**)
  - `PrepareForMobsimImpl` -> called before *each* iteration (car network only)

> [!warning] if your module/code relies on leg attributes: rerouting a plan will remove all leg attributes!

## How are locations of activities mapped to the network?

Apparently to nodes in the routing graph. Agents enter the simulation at the end of a link going towards that node.

## How many vehicles can start on a link per minute? FlowCapacity!

This is e.g. relevant for cordon points where all through traffic must be generated on the same link.

Short answer: **it completely depends on the `capacity` of the link** as defined in `network.xml` (and is *not* influenced by the freespeed!)

> [!WARNING] When viewing vehicles in Via note, that they (red triangles) only appear on the **second** link of the route.
> So don't be surprised if the vehicles generated per minute don't match the attributes of the second link, the origin link is the relevant one!

To elaborate: QSim has a `QueueWithBuffer` for each link in the network. 
How many vehicle can enter a link (per time unit) is defined by the **flow capacity** of the link.
It is stored in `QueueWithBuffer.flowCapacityPerTimeStep` and calculated from `Link.getFlowCapacityPerSec()`

Some numbers:

| road type | capacity | agents / minute |
|-|-|-|
| OSM residential road, 1 lane (per direction) | 600 | 10 |
| OSM motorway link, 1 lane | 1500 | 25 |
| OSM primary road, 2 lanes (per direction) | 3000 | 50 |
| OSM motorway, 4 lanes | 8000 | 133 |

## Cadyts heap space

For calibration runs with cadyts we encountered `java.lang.OutOfMemoryError: Java heap space` for 332k agents (Vienna XL) after ~500 iterations with 64GB of RAM.

Analyzing the heap dump shows that apparently the whole memory is filled with ~8 million MATSim plans - even if the original number of plans is only 332k * 4 (four for every agent). Maybe that's because of cadyts? See `plansEverSeen` in https://github.com/matsim-org/matsim-libs/blob/14.0/contribs/cadytsIntegration/src/main/java/org/matsim/contrib/cadyts/car/PlansTranslatorBasedOnEvents.java (TODO confirm that instances of this class live longer than for a single iteration)

## FreeSpeedFactorRouting infinite loop

pt2matsim may create links with a freespeed of `Infinity` - this obviously leads to infinite loops when routing with Dijkstra.

```xml
<link id="pt_at:49:935:0:5" from="pt_at:49:935:0:5" to="pt_at:49:935:0:5" length="1.0" freespeed="Infinity" capacity="9999.0" permlanes="1.0" oneway="1" modes="tram,artificial,stopFacilityLink" >
```

The `ride` mode is by default configured to use this routing.

## Car mode for pt?

When simulating pt the vehicles actually seem to use cars (not only busses, also trams etc.).
Apparently when analyzing the `events.xml` you must not trust `legMode` but also have a look at the `driverId` to determine if a vehicle is actually a car or not.

```xml
<event time="0.0" type="TransitDriverStarts" driverId="pt_veh_5034_rail_rail" vehicleId="veh_5034_rail" transitLineId="2-RX8-W-j22-1" transitRouteId="140.TA.2-RX8-W-j22-1.18.H" departureId="139.TA.2-RX8-W-j22-1.18.H_00:00:00"    
/>  
<event time="0.0" type="departure" person="pt_veh_5034_rail_rail" link="277144" legMode="car"  />  
<event time="0.0" type="PersonEntersVehicle" person="pt_veh_5034_rail_rail" vehicle="veh_5034_rail"  />  
<event time="0.0" type="vehicle enters traffic" person="pt_veh_5034_rail_rail" link="277144" vehicle="veh_5034_rail" networkMode="car" relativePosition="1.0"  />  
<event time="0.0" type="VehicleArrivesAtFacility" vehicle="veh_5034_rail" facility="at:43:4132:0:3.link:277144" delay="0.0"  />  
<event time="0.0" type="VehicleDepartsAtFacility" vehicle="veh_5034_rail" facility="at:43:4132:0:3.link:277144" delay="0.0"  />  
<event time="1.0" type="left link" link="277144" vehicle="veh_5034_rail"  />  
<event time="1.0" type="entered link" link="132431" vehicle="veh_5034_rail"  />  
<event time="9.0" type="left link" link="132431" vehicle="veh_5034_rail"  />
```

See https://github.com/matsim-org/matsim-code-examples/issues/686:
> For some historical reason the network mode of pt vessels is changed to `car` within the mobism. This is why your pt-vehicles will have `car` as network mode in your visualization. It is possible to filter the pt-vehilces by driver id, which is usually pre-pended with `pt_`

## How are linkStats calculated?

LinkStats are configured via their own section that controls aggregation and output interval:
```xml
<module name="linkStats" >
    <param name="averageLinkStatsOverIterations" value="5" />
    <param name="writeLinkStatsInterval" value="50" />
</module>
```

.. and `TravelTimeCalculatorModule` where more details like binning can be configured:
```xml
<module name="travelTimeCalculator" >
	<param name="calculateLinkToLinkTravelTimes" value="false" />
	<param name="calculateLinkTravelTimes" value="true" />
	<!-- The lenght (in sec) of the time period that is splited into time bins; an additional time bin is created to aggregate all travel times collected after maxTime -->
	<param name="maxTime" value="108000" />
	<!-- How to deal with congested time bins that have no link entry events. `optimistic' assumes free speed (too optimistic); 'experimental_LastMile' is experimental and probably too pessimistic. -->
	<param name="travelTimeAggregator" value="optimistic" />
	<!-- The size of the time bin (in sec) into which the link travel times are aggregated for the router -->
	<param name="travelTimeBinSize" value="900.0" />
	<!-- How to deal with link entry times at different positions during the time bin. Currently supported: average, linearinterpolation -->
	<param name="travelTimeGetter" value="average" />
</module>
```

When only simulating cars and teleporting everything else this obviously represents car traffic.
But when using transit simulation (e.g. with rail raptor) it gets complicated:

> [!warning] `TravelTimeCalculatorModule` is implemented to differentiate between `routing.networkModes`. This means when simulating pt and drt you can not differentiate them (as they are no separate network modes)

Behind the scenes: `LinkStatsControlerListener` uses an injected `Map<String, TravelTime> travelTime` that only has an entry `car` even when simulating transit. This is provided by the `TravelTimeCalculatorModule`, which has some deprecated parameters and nowadays gives your different travel times per network mode (but pt and drt unfortunately use car as networkMode).

## FractionOfIterationsToDisableInnovation

After the fraction has been reached only selection strategies are used, e.g. `BestScore` and `SelectExpBeta`.
Yes, also `ReRoute` is turned off!