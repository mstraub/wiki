> [!tldr] SwissRailRaptor is a fast public transport router developed for SBB, integral part of MATSim since ~v13, and the default pt router as of v2024.0.

It is based on the RAPTOR algorithm (Delling et al, 2012, Round-Based Public Transit Routing). Actual performance gains vary by scenario, but are typically in the order of one to two magnitudes compared to the default pt router in MATSim. SwissRailRaptor can act as a drop-in replacement for the pt router included in MATSim by default when it is used without further configuration, re-using the configuration parameters from the default `transitRouter` config group.

It's not only faster but supports:
* **multiple access/egress modes** (feeder routes) - in comparison to only walk access/egress of the original transit router - with
    * definition of stops eligible for feeder modes (think availability of park and ride facility)
    * definition of persons eligible for feeder modes (think car / bike availability)
* range queries (search for possible connections within a time window)
* differentiating PT sub-modes
* person-specific routing-costs

Doc: https://github.com/SchweizerischeBundesbahnen/matsim-sbb-extensions (unfortunately slightly outdated)
See also: master thesis "Intermodal Routing in MATSim Applied to SBB Green Class" by Raphael Haslebacher https://ethz.ch/content/dam/ethz/special-interest/baug/ivt/ivt-dam/publications/students/601-700/sa615.pdf

# Configuration

Minimal variant - simply enable it as routing algorithm type without further config
```xml
<module name="transit">
    <param name="routingAlgorithmType" value="SwissRailRaptor" />
    <param name="transitScheduleFile" value="transit_schedule.xml" />
    <param name="useTransit" value="true" />
    <param name="vehiclesFile" value="transit_vehicles.xml" />
</module>
```

## Intermodal feeder routes

For intermodal routing either use the old `transitRouter` config group (restricted to walk!) or add an `intermodalAccessEgress` section for each mode you desire (but always add one for `walk` if you do so).

**Simple example** with `walk` and `bike` (assuming that bike is teleported):
```xml
<module name="transit">
    <param name="routingAlgorithmType" value="SwissRailRaptor" />
    <param name="transitScheduleFile" value="transit_schedule.xml" />
    <param name="useTransit" value="true" />
    <param name="vehiclesFile" value="transit_vehicles.xml" />
</module>

<module name="swissRailRaptor">
    <param name="useIntermodalAccessEgress" value="true" />
    <parameterset type="intermodalAccessEgress">
        <param name="mode" value="walk" />
        <param name="initialSearchRadius" value="500" />
        <param name="maxRadius" value="1000" />
    </parameterset>
    <parameterset type="intermodalAccessEgress">
        <param name="mode" value="bike" />
        <param name="initialSearchRadius" value="1000"/>
        <param name="searchExtensionRadius" value="1000"/>
        <param name="maxRadius" value="5000"/>
    </parameterset>
</module>
```

**Complex example** additionally with car (with person + stop facility filtering) and two drt modes (accessible for everyone)
```xml
<parameterset type="intermodalAccessEgress">
    <param name="mode" value="walk" />
    <param name="initialSearchRadius" value="500" />
    <param name="maxRadius" value="1000" />
</parameterset>
<parameterset type="intermodalAccessEgress">
    <param name="mode" value="bike" />
    <param name="initialSearchRadius" value="1000"/>
    <param name="searchExtensionRadius" value="1000"/>
    <param name="maxRadius" value="5000"/>
    <param name="personFilterAttribute" value="bikeAvailable" />
    <param name="personFilterValue" value="true" />
</parameterset>
<parameterset type="intermodalAccessEgress">
    <param name="mode" value="car" />
    <param name="initialSearchRadius" value="5000"/>
    <param name="searchExtensionRadius" value="5000"/>
    <param name="maxRadius" value="20000"/>
    <param name="linkIdAttribute" value="nearestCarLinkId" />
    <param name="stopFilterAttribute" value="carAccessible" />
    <param name="stopFilterValue" value="true" />
    <param name="personFilterAttribute" value="hasLicenseAndCarAvailable" />
    <param name="personFilterValue" value="true" />
</parameterset>
<parameterset type="intermodalAccessEgress">
    <param name="mode" value="drtA" />
    <param name="initialSearchRadius" value="5000"/>
    <param name="searchExtensionRadius" value="5000"/>
    <param name="maxRadius" value="20000"/>
    <param name="linkIdAttribute" value="nearestCarLinkId" />
</parameterset>
<parameterset type="intermodalAccessEgress">
    <param name="mode" value="drtB" />
    <param name="initialSearchRadius" value="5000"/>
    <param name="searchExtensionRadius" value="5000"/>
    <param name="maxRadius" value="20000"/>
    <param name="linkIdAttribute" value="nearestCarLinkId" />
</parameterset>
```


> [!note] Note, that different feeder modes can be used on a single trip (e.g. bike to pt and walk to destination).
I successfully tested this with multiple service-area-based drt modes.


### Data prep requirements

#### Stop facility attribute: linkIdAttribute for main modes

Intermodal routing works fine without further preparation steps for teleported modes (typically e.g. walk and bike).

If you try the same for main modes such as `car` you will be greeted with an exception like this:
```
Exception in thread "main" java.lang.RuntimeException: org.matsim.core.mobsim.qsim.pt.TransitQSimEngine$TransitAgentTriesToTeleportException: Agent 1890-0_1_2#1 tries to enter a transit stop at link 302716 but really is at 92796!
```

If the  `linkRefId` of one or more stop facilities of your `transitSchedule` do not allow the main mode in question you need to pre-calculate an attribute, e.g. `carAccessLinkId`. This can easily be done in a custom pt2matsim script. Then reference the attribute like this:
```xml
​<parameterset type="intermodalAccessEgress">
    ...
    <param name="linkIdAttribute" value="carAccessLinkId" />
</parameterset>
```

#### Stop facility attributes: eligibility for feeder modes

For more realism of intermodal routes it can be useful to restrict feeder routes,
e.g. allow cars only to park at high-ranking transit stops.

Add an according attribute (e.g. `carAccessible`) to the stop facilities of your `transitSchedule` (e.g. in your pt2matsim script).
Then reference the attribute like this:
```xml
​<parameterset type="intermodalAccessEgress">
    ...
    <param name="stopFilterAttribute" value="carAccessible" />
    <param name="stopFilterValue" value="true" />
</parameterset>
```

#### Population attributes: persons eligible for feeder modes

To define if persons are eligible for a feeder mode the combo of `personFilterAttribute` and `personFilterValue` must be used.
Internally only a simple equals comparison is done, so this works fine for trivial attributes only, such as `bikeAvailable=true/false`
```xml
​<parameterset type="intermodalAccessEgress">
    ...
    <param name="personFilterAttribute" value="bikeAvailable" />
    <param name="personFilterValue" value="true" />
</parameterset>
```

Car availability in MATSim is usually represented as follows:
```java
public static boolean canActuallyDriveACar(Person person) {
    return !"no".equals(PersonUtils.getLicense(person)) && !"never".equals(PersonUtils.getCarAvail(person));
}
```

So in order to properly check if an agent can use a car we have to pre-calculate an attribute (let's call it `hasLicenseAndCarAvailable`) using the logic above.

# Issues & Limitations

## broken CalcLeastCostModePerStop

> [!danger] Using the default setting `<param name="intermodalAccessEgressModeSelection" value="CalcLeastCostModePerStop" />` leads to completely different (and more unrealistic) results than with `RandomSelectOneModePerRoutingRequestAndDirection` when including the bike option in our Vienna model.

Apparently this is a known but unresolved problem: https://github.com/matsim-org/matsim-code-examples/issues/722

I therefore try to avoid intermodal access egress, but when I have to use it (e.g. for multiple drt fleets) I only add walking plus the drt modes (and not bike).

## Unreasonable trips

### Short access/egress

Many of the bike access/egress trips are much too short to be realistic (i.e. a few seconds) - and there is no way to configure either a minimum or a penalty (this is only possible between pt submodes).

### Bogus routes

Even with `<param name="intermodalLegOnlyHandling" value="forbid" />` the route can basically only consist of the feeder mode,
e.g. car access + egress with a single tram stop inbetween.
