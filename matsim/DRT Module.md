> [!tldr] demand responsive transport
> note: this module heavily relies on the `dvrp` module

The `drt` module does not plan the assignment of vehicles to people beforehand - this is done live during the simulation phase.

During simulation an empty drt leg of an input plan is expanded into three legs (and two drt interaction activities):
1. a (beeline) foot path to a place where the drt vehicle will (hopefully) pick up the agent
2. the drt route
3. a (beeline) foot path from the drop off point to the desired end location

An initially empty drt leg in the plan causes `PersonPrepareForSim` to request a route.
```xml
<person id="person_drt_one">
	<plan selected="yes">
		<activity type="home" link="368" x="3700.456" y="343947.606" end_time="00:00:00" />
		<leg mode="drt" />
		<activity type="work" link="152" x="3360.657" y="344813.230" start_time="07:40:00" end_time="17:30:00" />
	</plan>
</person>
```

Plan handled by the drt module with a prepended and an appended foot leg
```xml
<person id="person_drt_one">
	<attributes>
	</attributes>
	<plan score="105.66047724697107" selected="yes">
		<activity type="home" link="368" x="3700.456" y="343947.606" end_time="00:00:00" >
		</activity>
		<leg mode="walk" dep_time="00:00:00" trav_time="00:03:45">
			<attributes>
				<attribute name="routingMode" class="java.lang.String">drt</attribute>
			</attributes>
			<route type="generic" start_link="368" end_link="368" trav_time="00:03:45" distance="187.5701956306128"></route>
		</leg>
		<activity type="drt interaction" link="368" x="3646.6590622737117" y="343995.67264238046" max_dur="00:00:00" >
		</activity>
		<leg mode="drt" dep_time="00:03:46" trav_time="00:08:48">
			<attributes>
				<attribute name="routingMode" class="java.lang.String">drt</attribute>
			</attributes>
			<route type="drt" start_link="368" end_link="152" trav_time="00:08:48" distance="1044.2830300693608">1200.0 99.0</route>
		</leg>
		<activity type="drt interaction" link="152" x="3326.3212308344882" y="344788.7766430294" max_dur="00:00:00" >
		</activity>
		<leg mode="walk" dep_time="00:12:35" trav_time="00:00:00">
			<attributes>
				<attribute name="routingMode" class="java.lang.String">drt</attribute>
			</attributes>
			<route type="generic" start_link="152" end_link="152" trav_time="00:00:00" distance="9.64242869574758E-4"></route>
		</leg>
		<activity type="work" link="152" x="3360.657" y="344813.23" start_time="07:40:00" end_time="17:30:00" >
	</plan>
</person>
```

To create these foot legs `ClosestAccessEgressFacilityFinder` from the `dvrp` module is used.

> [!warning] The drt module will **completely replan (replace) trips** as soon as  `PersonPrepareForSim` deems a reroute necessary. Since MATSim does not allow legs with different `routingMode` in one trip we can not retain parts of the trip!

# Configuration

## Basic

Add a config section for the `multiModeDrt` module and at least one `drt` parameterset:
```xml
<module name="multiModeDrt">
	<parameterset type="drt">
		..
	</parameterset>
</module>
```

For each drt mode these parameters used for the optimization algorithm must be set.
They don't have default values but `stopDuration = 1 minute` and `maxWaitTime = 10 minutes`  seems sensible.
For alpha/beta a paper reviewer suggested these ranges:
- Alpha: 1.2 - 2
- Beta: 600 - 1800 (should be greater than `maxWaitTime`)

```xml
<parameterset type="drt">
    <param name="stopDuration" value="60"/>
    <param name="maxWaitTime" value="600"/>
    <param name="maxTravelTimeAlpha" value="1.3"/>
    <param name="maxTravelTimeBeta" value="900"/>
    ...
</parameterset>
```

Note, that `drt` must **not** be configured as  a `qsim.mainMode`.

### Vehicles

Each drt fleet needs a `dvrp` vehicles file:
```xml
<parameterset type="drt">
    <param name="vehiclesFile" value="vehicles.xml"/>
    ...
</parameterset>
```

We need to define an id, start link, operation hours, and capacity:
```xml
<?xml version="1.0" ?>
<!DOCTYPE vehicles SYSTEM "http://matsim.org/files/dtd/dvrp_vehicles_v1.dtd">
<vehicles>
	<vehicle id="drt_1" start_link="777" t_0="0.0" t_1="86400.0" capacity="2"/>
	<vehicle id="drt_2" start_link="777" t_0="0.0" t_1="86400.0" capacity="2"/>
</vehicles>
```


### Operational Schemes

#### Door to Door

Passengers can be picked up and dropped off on every link.

```xml
<module name="multiModeDrt">
	<parameterset type="drt">
		<param name="operationalScheme" value="door2door" />
		...
	</parameterset>
</module>
```

#### Stop Based

The drt fleet can be restricted to only stop at predefined stop positions.

```xml
<module name="multiModeDrt">
	<parameterset type="drt">
		<param name="operationalScheme" value="stopbased" />
		<param name="transitStopFile" value="drtStops.xml" />
		...
	</parameterset>
</module>
```

These stops are provided in form of a `transitSchedule` only containing stop facilities:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE transitSchedule SYSTEM "http://www.matsim.org/files/dtd/transitSchedule_v2.dtd">
<transitSchedule>
	<transitStops>
		<stopFacility id="193014" x="7138.67" y="308309.88" linkRefId="25593" name="abcde" />
	</transitStops>
</transitSchedule>
```

#### Service Areas

A drt fleet can be restricted to a service area. Simply provide a shapefile containing a single polygon and activate the behavior as follows:

```xml
<module name="multiModeDrt">
	<parameterset type="drt">
		<param name="operationalScheme" value="serviceAreaBased" />
		<param name="drtServiceAreaShapeFile" value="drtServiceArea.shp" />
		...
	</parameterset>
</module>
```

The drt vehicles themselves can drive outside of the polygon but the actual pickup and dropoff of passengers will be restricted to all links within this polygon.

## MultiMode DRT

The drt module supports multiple drt fleets since MATSim 12. In the configuration each drt fleet must get **its own mode name** and parameter set.  This means the different drt fleets are different modes of transport and each have their own router in the MATSim `TripRouter`.

```xml
<module name="multiModeDrt">
	<parameterset type="drt">
		<param name="mode" value="drt1"/>
		<param name="vehiclesFile" value="drt1_vehicles.xml"/>
		..
	</parameterset>
	<parameterset type="drt">
		<param name="mode" value="drt2"/>
		<param name="vehiclesFile" value="drt2_vehicles.xml"/>
		..
	</parameterset>
</module>
```

> [!note] It is required (by the underlying dvrp module) that the vehicles have globally unique ids (and not only within one fleet).

**Routing with multiple drt modes**

The drt module itself does not provide routing that combines all drt modes to an intermodal route.

When using `subtourModeChoice` each drt mode must be listed as separate mode, this means that two `drt` modes will never be used in a single trip:

```xml
<module name="subtourModeChoice">
    <param name="chainBasedModes" value="car" />
    <param name="modes" value="bike,car,drt1,drt2,pt,walk" />
</module>
```

## Underlying TT Matrix

Under the hood the `dvrp` module calculates a travel time matrix (class `FreeSpeedTravelTimeMatrix`.
This creates a sparse `SquareGridSystem` that only allocates square cells where a network node is found, which is quite efficient.
Unfortunately,
- the matrix is **always calculated for the whole grid system** (even if you only have a small service-area based drt)
- the matrix **can not be cached**

This can only be sped up by using more threads via `qsim.numberOfThreads` or tweaking the config by increasing the `cellSize`:

```xml
<module name="dvrp" >
	<parameterset type="travelTimeMatrix" >
		<!-- size of square cells (meters) used for computing travel time matrix. Default value is 200 m -->
		<param name="cellSize" value="200.0" />
		<!-- Max network distance from node A to node B for B to be considered a neighbor of A. In such cases, a network travel time from A to B is calculated and stored in the sparse travel time matrix. Typically, 'maxNeighborDistance' should be higher than 'cellSize' (e.g. 5-10 times) in order to reduce the impact of imprecise zonal travel times for short distances. On the other, a too big value will result in large neighborhoods, which may slow down queries. The unit is meters. Default value is 1000 m. -->
		<param name="maxNeighborDistance" value="1000.0" />
		<!-- Max network travel time from node A to node B for B to be considered a neighbor of A. In such cases, a network travel time from A to B is calculated and stored in the sparse travel time matrix. Typically, 'maxNeighborTravelTime' should correspond to a distance that are higher than 'cellSize' (e.g. 5-10 times) in order to reduce the impact of imprecise zonal travel times for short distances. On the other, a too big value will result in large neighborhoods, which may slow down queries. The unit is seconds. Default value is 0 s (for backward compatibility). -->
		<param name="maxNeighborTravelTime" value="0.0" />
	</parameterset>
</module>
```

For debugging purposes (where the quality of drt assignments is irrelevant) it may be useful to skip the matrix calculation with a config like this.
```xml
<module name="dvrp">
  <parameterset type="travelTimeMatrix">
    <!-- set cellSize to a large enough value to have only a few cells -->
    <param name="cellSize" value="40000"/>
    <!-- set both maxNeighbor values to 0 to skip computation of sparse matrix -->
    <param name="maxNeighborDistance" value="0" />
    <param name="maxNeighborTravelTime" value="0" />
  </parameterset>
</module>
```

## Vehicle Speed Limit

[VehicleType.getMaximumVelocity](https://github.com/matsim-org/matsim-libs/blob/91288bfd5dc98d30c238f6903ec555d8c1f894d7/matsim/src/main/java/org/matsim/vehicles/VehicleType.java#L104)

### A failed approach

My naive approach was to use `vehicleDefinitions` like this:

```xml
<?xml version="1.0" encoding="UTF-8"?>
<vehicleDefinitions xmlns="http://www.matsim.org/files/dtd"
 xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
 xsi:schemaLocation="http://www.matsim.org/files/dtd http://www.matsim.org/files/dtd/vehicleDefinitions_v1.0.xsd">
	<vehicleType id="car">
	</vehicleType>
	<vehicleType id="drt">
		<maximumVelocity meterPerSecond="5.5"/>
	</vehicleType>
</vehicleDefinitions>
```

in combination with

```xml
<module name="qsim">
    <param name="vehiclesSource" value="modeVehicleTypesFromVehiclesData" />
</module>
<module name="vehicles">
    <param name="vehiclesFile" value="path/to/vehicleDefinitions.xml" />
</module>
```

.. but I somehow I couldn't restrict the speed of my drt vehicles (even when setting the maximumVelocity for cars in general.)

Why? Apparently `vehicleType` is only parsed for main modes (and `drt` must not be defined as main mode): [see PrepareForSimImpl.getVehicleTypesForAllNetworkAndMainModes](https://github.com/matsim-org/matsim-libs/blob/91288bfd5dc98d30c238f6903ec555d8c1f894d7/matsim/src/main/java/org/matsim/core/controler/PrepareForSimImpl.java#L299).

Drt vehicles use the `defaultVehicleType` because the `VrpAgentSource` called in `QSim.createAgents` provides it.

`DrtModeModule` [calls a FleetModule constructor](https://github.com/matsim-org/matsim-libs/blob/91288bfd5dc98d30c238f6903ec555d8c1f894d7/contribs/drt/src/main/java/org/matsim/contrib/drt/run/DrtModeModule.java#L64) that simply instantiates the default vehicle type. There is no way I see via `config.xml` to call this constructor instead:  `public FleetModule(String mode, URL fleetSpecificationUrl, VehicleType vehicleType)`

# Issues & Limitations

## QUESTION: 

What is the zonalSystem for? After reading https://svn.vsp.tu-berlin.de/repos/public-svn/publications/vspwp/2021/21-06/ITS_Hamburg_2021_Lu.pdf I figure it's for rebalancing only?
```xml
<parameterset type="drt">
    ...
    <parameterset type="zonalSystem">
        <param name="zonesShapeFile" value="./SHOW-2024-02/drt/service-areas/Faistenau.shp"/>
        <param name="zonesGeneration" value="ShapeFile"/>
    </parameterset>
</parameterset>
```

The dvrp tt matrix must be calculated anyways?
```xml
<module name="dvrp">
    <parameterset type="travelTimeMatrix">
        <param name="cellSize" value="200"/>
    </parameterset>
</module>
```

## No prebooking (yet)

Agents will call the drt vehicle the moment they want to leave, i.e. *not in advance* (see `DefaultUnplannedRequestInserter`).
For some scenarios this is impractical, but as of 2023 the feature is [in active development](https://ethz.ch/content/dam/ethz/special-interest/baug/ivt/ivt-dam/events/2023/09/05/abstracts/Chouaki_Hoerl_MUM_2023.pdf)

Glimpses were already there earlier as I write in 2020: If we need this feature there seems to be experimental support by setting `DrtConfigGroup.advanceRequestPlanningHorizon`, which is not yet exposed via the config xml. Also it seems to be under active development as of May 2020, see https://github.com/matsim-org/matsim-libs/pull/987

## Exception if no drt vehicle is active but an agent wants to use drt

I encountered this problem (Vienna XL 2023.2 scenario): insertion in `DefaultRequestInsertionScheduler` fails.

```
2024-03-08T03:20:23,951  INFO AbstractQNetsimEngine:364 SIMULATION (QNetsimEngine) AT 23:00:00 : #links=3724 #nodes=1559
2024-03-08T03:20:23,951  INFO QSim:566 SIMULATION (NEW QSim) AT 23:00:00 : #Veh=29621 lost=0 simT=82800.0s realT=544s; (s/r): 152.2058823529412
2024-03-08T03:20:30,936  INFO AbstractQNetsimEngine:364 SIMULATION (QNetsimEngine) AT 24:00:00 : #links=1169 #nodes=342
2024-03-08T03:20:30,936  INFO QSim:566 SIMULATION (NEW QSim) AT 24:00:00 : #Veh=14724 lost=66 simT=86400.0s realT=551s; (s/r): 156.80580762250455
2024-03-08T03:20:32,706  INFO Gbl:196 Thread performance: Thread=class org.matsim.core.events.SimStepParallelEventsManagerImpl$ProcessEventsRunnable0  cpu-time=221.13212513sec
2024-03-08T03:20:32,706 ERROR AbstractController:225 Mobsim did not complete normally! afterMobsimListeners will be called anyway.
java.lang.IllegalStateException: null
	at com.google.common.base.Preconditions.checkState(Preconditions.java:496) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.dvrp.schedule.ScheduleImpl.failIfNotStarted(ScheduleImpl.java:205) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.dvrp.schedule.ScheduleImpl.getCurrentTask(ScheduleImpl.java:139) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.drt.scheduler.DefaultRequestInsertionScheduler.insertPickup(DefaultRequestInsertionScheduler.java:185) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.drt.scheduler.DefaultRequestInsertionScheduler.scheduleRequest(DefaultRequestInsertionScheduler.java:90) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.drt.optimizer.insertion.DefaultUnplannedRequestInserter.scheduleUnplannedRequest(DefaultUnplannedRequestInserter.java:148) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.drt.optimizer.insertion.DefaultUnplannedRequestInserter.scheduleUnplannedRequests(DefaultUnplannedRequestInserter.java:118) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.contrib.drt.optimizer.DefaultDrtOptimizer.notifyMobsimBeforeSimStep(DefaultDrtOptimizer.java:94) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.mobsim.qsim.MobsimListenerManager.fireQueueSimulationBeforeSimStepEvent(MobsimListenerManager.java:107) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.mobsim.qsim.QSim.doSimStep(QSim.java:390) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.mobsim.qsim.QSim.run(QSim.java:259) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.NewControler.runMobSim(NewControler.java:125) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController$8.run(AbstractController.java:214) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController.iterationStep(AbstractController.java:246) ~[matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController.mobsim(AbstractController.java:210) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController.iteration(AbstractController.java:157) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController.doIterations(AbstractController.java:124) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController$1.run(AbstractController.java:84) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.MatsimRuntimeModifications.run(MatsimRuntimeModifications.java:70) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.MatsimRuntimeModifications.run(MatsimRuntimeModifications.java:53) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.AbstractController.run(AbstractController.java:92) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.NewControler.run(NewControler.java:83) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at org.matsim.core.controler.Controler.run(Controler.java:259) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at at.ac.ait.matsim.salabim.Main.run(Main.java:462) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
	at at.ac.ait.matsim.salabim.Main.main(Main.java:496) [matsim-salabim-16.0-20240305.072341-24.jar:16.0-SNAPSHOT]
2024-03-08T03:20:32,712  INFO AbstractController$9:237 ### ITERATION 52 fires after mobsim event
```

Updating the vehicles files to have `t_1` equal to `qsim.endTime` (30:00) solved the problem. Before it was 24:00.

```
<!DOCTYPE vehicles SYSTEM "http://matsim.org/files/dtd/dvrp_vehicles_v1.dtd">
<vehicles>
  <vehicle id="drt1" start_link="100779" t_0="0" t_1="108000" capacity="3"/>
  ...
</vehicles>
```

## Rejected Passengers

In case too many agents' plans contain drt legs the drt system can get overwhelmed.
Have a look at the optimization parameters or simply set:

```xml
<param name="rejectRequestIfMaxWaitOrTravelTimeViolated" value="false" />
```

If no valid insertion can be found by `DefaultUnplannedRequestInserter.scheduleUnplannedRequests` a `PassengerRequestRejectedEvent` is created (see `PassengersRequest rejected` in the events file) and a `PersonStuckEvent` will follow immediately (see `stuckAndAbort` in the events file):

```xml
<event time="66205.0" type="PassengersRequest rejected" person="person_drt_six" mode="drt" request="drt_10" cause="no_insertion_found" />
<event time="66205.0" type="stuckAndAbort" person="person_drt_six" link="994" legMode="drt" />
```

## Unfulfilled DRT Trips

In case a drt request can not be fulfilled the person is stuck and can not complete the daily plan. Details about fulfilled and not fulfilled trips can be found for each iteration in the files `drt_trips_drt.csv` and `drt_rejections_drt.csv`

In the events.xml you find lines like:

	<event time="20701.0" type="stuckAndAbort" person="545_300_1_2" link="83234" legMode="drt"  />

And also the logfile.log contains warnings:

	2019-10-07 15:39:50,298  WARN DefaultUnplannedRequestInserter:99 No insertion found for drt request [id=drt_563][submissionTime=20700.0] from passenger id=545_300_1_2 fromLinkId=83234

In case you looked at the last iteration the plans.xml of this iteration won't have a negative score - you have to check `output_plans.xml`, where the score will be negative (for the not fulfilled day).

## Long Walks to Service Areas

> [!note] When plans with drt trip leading far outside of the service area ridiculously long foot walks will be created (and the scheduler will get problems when trying to find a drt ride): \
> ![](longwalk.png)


Interestingly enough these long foot segments are handled differently:

- after drt ride: does not seem to be punished
- before drt ride: drt rides are made (more or less) impossible because the desired arrival time of the drt request will be too early (or already in the past) after the long walk. And the request submission time is the time when the person finishes walking to the stop! (strange behavior, person should call latest when starting to walk...)
