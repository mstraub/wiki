>[!tldr] [Poetry - python packaging and dependency management made easy](https://python-poetry.org/)

> [!warning] as of late 2024 you should check out `uv` instead

Poetry is a tool for **dependency management** and **packaging** in Python.
It allows you to declare the libraries your project depends on and it will manage (install/update) them for you.

By default it uses [PyPI](https://pypi.org/) to fetch dependencies,
but can use other sources (e.g. GitLab registries) as well - see https://python-poetry.org/docs/repositories/

> Poetry in action @ AIT: https://gitlab-intern.ait.ac.at/energy/projects/asea-e-mob/asea-prototype

Hints and tricks (mostly Fabian Leimgruber)
- poetry funktioniert gut, v.a. mit linux, ABER kann anscheinend nur pure python-pakete importieren, d.h. keine 100% optimierten C libraries (für pandas, streamlit,..) https://github.com/python-poetry/poetry/issues/105#issuecomment-886236059 .. siehe auch https://numpy.org/install/ (Erklärung zu MKL und OpenBLAS,..)
	- https://stackoverflow.com/questions/62513005/how-does-poetry-work-regarding-binary-dependencies-esp-numpy
	- so apparently when using poetry you use wheels with OpenBLAS (and not Intel MKL, even if that would be faster on your platform)
	- TODO: benchmark what really happens
- https://pypi.org/project/python-dotenv/
- https://12factor.net/ the 12 factor app
- git submodules haben für ihn gut funktioniert

# Setup in Ubuntu 22.04

Ideally use the [official install script](https://python-poetry.org/docs/master/#installing-with-the-official-installer) to install poetry
isolated from the rest of your system.

It is installed to `~/.local/share/pypoetry`

## Mixing Poetry with conda

If you can avoid it do not mix `poetry` and `conda`, i.e. *do not run any Poetry commands in an activated conda environment*.
Poetry will install / change the environment which very quickly leads to situations where conda can not resolve requirements anymore.

However, I used this approach successfully for libraries that can not be installed via `poetry` (on at least one relevant platform),
e.g. PyQGIS.

Also this article here claims that the combination of conda + poetry works well: https://ealizadeh.com/blog/guide-to-python-env-pkg-dependency-using-conda-poetry

## Gotchas

- Poetry likes to call `python` - so maybe you must install `python-is-python3` on old systems where `python` still refers to python 2.
- In case your `~/.local/bin` did not exist before you installed Poetry you must then login/logout or `source ~/.profile` to have `poetry` on `$PATH`.

# Working with Poetry

In general: the docs are great! https://python-poetry.org/docs

`poetry` is not capable of installing python itself (`conda` can do that),
so a python version compatible to what you specify in `pyproject.toml` must be installed.

```
[tool.poetry.dependencies]
python = "^3.8"
```

If Poetry is not called from within a virtual environment it will automatically use / create one!
E.g. the first time you call `poetry add DEPENDENCYNAME` it creates a virtualenv!

## Common Tasks

### Show version
```bash
poetry --version
```

### Update Poetry
```bash
poetry self update
```

### Create new project
Create a `pyproject.toml` plus basic directory layout ([either flat or src](https://packaging.python.org/en/latest/discussions/src-layout-vs-flat-layout/))
```bash
poetry new PROJECTNAME # flat layout by default
poetry --src new PROJECTNAME # src layout
```

> [!NOTE] `poetry new` vs `poetry init`
> `poetry init` helps you interactively filling out a `pyproject.toml`,
> but for some reason this can not be combined with `poetry new` ([yet](https://github.com/python-poetry/poetry/issues/2563)).

### Install dependencies
(Re)install dependencies from the lock-file (`poetry.lock`) or the `pyproject.toml`
```bash
poetry install
```

> [!WARNING] Changing the required python version
> In case the required python version was changed you must **delete the relevant virtual environment** first,
> otherwise the change is ignored!

### List virtual environments
Shows virtual environments for the current project
(automatically created ones are put into `~/.cache/pypoetry/virtualenvs/`)
```bash
poetry env list --full-path
```

### Add dependencies
Add dependencies (to `pyproject.toml`) and immediately install them:
```bash
poetry add pandas
poetry add --group dev black # add as dev dependency only
```

### Build project
```bash
poetry build
```

### Run
Run e.g. tests, code formatter, etc. in the project's virtual environment (without activating it first)
```bash
poetry run pytest
poetry run black
```

### Activate the virtual environment
```bash
poetry shell
```

To leave the virtual environment again call `deactivate`.

# IDE integration

## Spyder

Go to `Tools > Preferences > Python interpreter` - conda environments are detected automatically, pypoetry environments must be selected manually

Note: environments are currently global for spyder (maybe this will be improved in Spyder 6): https://github.com/spyder-ide/spyder/issues/11362

To get the most out of Spyder the python environment you use must install spyder-kernels. (Note the exact version requirements of your spyder version!)

	poetry add -D spyder-kernels

Otherwise spyder greets you with something like this:

```
An error ocurred while starting the kernel
Your Python environment or installation doesn't have the spyder‑kernels module or the right version of it installed (>= 2.0.1 and < 2.1.0). Without this module is not possible for Spyder to create a console for you.

You can install it by running in a system terminal:

conda install spyder‑kernels=2.0

or

pip install spyder‑kernels==2.0.*
```

## VSCode

environment by conda or in `~/.cache/pypoetry/virtualenvs/` are detected - the proper one can easily be selected (bottom left).

> [!TIP] `pylance` could not switch to my `conda` environment. starting `code` from within the activated environment is a workaround.
