# Tidbits

## open port < 1024 as non-root

	sudo setcap cap_net_bind_service=+epi $JAVA_HOME/bin/java
	sudo setcap cap_net_bind_service=+epi $JAVA_HOME/jre/bin/java

# JDK choice

For Ubuntu it makes sense to use the provided builds of OpenJDK:
```bash
sudo apt install openjdk-XX-jdk
```

For all other platforms (or when a specific version is not available)
the free and open source [Eclipse Temurin](https://adoptium.net) by Adoptium is a good choice.

# Build Systems

As of 2021 for Java there are two widely used build systems: [Maven](Maven.md) and [Gradle](Gradle.md)
The latter is much more actively developed and might be the better choice (especially for new projects).

# Profiling

[VisualVM](https://visualvm.github.io/) offers GUI sweetness by quickly attaching to a running Java process.

>[!Hint] VisualVM only sees processes owned by the user you start it with!

**Memory**
```bash
jmap -histo:live <pid> # histogram of heap
jmap -dump:file=filename.hprof <pid> # save heap dump to file
```

Heap dumps can be opened with VisvualVM or [Memory Analyzer (MAT)](https://eclipse.dev/mat/)

**CPU**
```bash
java -Xprof ... (fast!)
java -agentlib:hprof=cpu=times (takes ages but hprof is more powerful if you need it..)
```

For even more low-level info: Flight Recorder + Mission Control
