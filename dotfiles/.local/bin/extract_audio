#!/bin/bash
# extract the audio part of a video file to a proper audio container without reencoding

# invoke e.g. with
# find . -name "*.webm" -print0 | xargs -0 -n 1 xxx_extract_audio.sh

if [ $# -ne 1 ]
then
	echo "exactly one argument expected"
	exit 1
fi

FILE=$1

IS_OPUS=`ffprobe -hide_banner "$FILE" 2>&1 | grep -i Audio | grep -i opus | wc -l`
IS_MP3=`ffprobe -hide_banner "$FILE" 2>&1 | grep -i Audio | grep -i mp3 | wc -l`

if [ $IS_OPUS -gt 0 ]
then
	EXTENSION="ogg"
fi
if [ $IS_MP3 -gt 0 ]
then
	EXTENSION="mp3"
fi

if [ -z $EXTENSION ]
then
	echo "could not figure out proper container for audio content"
	exit 1
fi

WITHOUT_EXTENSION=$(echo $FILE | rev | cut -f 2- -d '.' | rev)
OUT_FILE=$WITHOUT_EXTENSION.$EXTENSION
echo "copying audio stream from '$FILE' to '$OUT_FILE'..."
ffmpeg -hide_banner -i "$FILE" -vn -acodec copy "$OUT_FILE"
