alias a='cd /home/mstraub/projects/ariadne/ariadne2'
#alias m='cd /home/mstraub/projects/mped/trunk'
alias m='cd ~/projects/matsim-salabim'
alias mm='cd ~/projects/matsim-salabim/simulation/matsim-salabim'
alias mtmp='cd ~/tmp/matsim-salabim'
alias my='cd ~/projects/mytrips/rest-service'

alias o='xdg-open'
alias ppjson='python -m json.tool'
alias sl='svn log | less'
alias slv='svn log -v | less'
alias sd='svn diff | vim -R -'
alias demeter-output='for folder in `ssh demeter ls -1 -d output*`; do mkdir $folder; scp -r "demeter:$folder/*.{png,txt,log}" $folder; done'
alias gd='./gradlew clean build deploy'

alias gpgencrypt='gpg --symmetric --cipher-algo AES256'
# decrypt to stdout
alias show_server_access='gpg --decrypt < `ls -1 -t /home/mstraub/Nextcloud/AIT/server_access* | head -n 1`'
# decrypt from latest file to /tmp and open in vim
alias decrypt_server_access='gpg -o /tmp/server_access.md --decrypt < `ls -1 -t /home/mstraub/Nextcloud/AIT/server_access* | head -n 1` && vim /tmp/server_access.md'
# encrypt from /tmp and remove unencrypted file
alias encrypt_server_access='gpg -o "/home/mstraub/Nextcloud/AIT/server_access_$(date -u +%Y-%m-%dT%H-%M-%S%Z).md.gpg" --symmetric --cipher-algo AES256 /tmp/server_access.md && rm /tmp/server_access.md'

alias ws="sudo systemctl start wildfly"
alias wp="sudo systemctl restart wildfly"
alias wj="sudo journalctl -o cat -f -u wildfly.service | sed 's/EE-Managed.*Thread-//'"

alias disable_touchscreen="xinput disable `xinput | grep MELF0410 | awk '{print $5}' | sed 's/id=//'`"

alias wiki="find ~/Nextcloud/schnipsel.wiki/ -name '*.md' -print0 | xargs -0 kate"
