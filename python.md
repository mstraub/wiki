 >[!tldr] https://www.python.org

# Code Style

The [black](https://github.com/psf/black) style is widely used as of 2024.
For autoformatting either use black itself or [ruff](https://docs.astral.sh/ruff) for an extremely fast formatting+linting experience

Docstrings: popular formatting choices are [Google Style](https://google.github.io/styleguide/pyguide.html#38-comments-and-docstrings)and [NumPy Style](https://numpydoc.readthedocs.io/en/latest/format.html#docstring-standard) - see https://stackoverflow.com/a/24385103/1648538

# Installation

In Ubuntu the [deadsnakes PPA](https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa) provides older (as the name suggests) but also newer versions.

```bash
sudo add-apt-repository ppa:deadsnakes/ppa
sudo apt install python3.x python3.x-distutils
```

> [!warning] uninstall the deadsnake-pythons before doing a dist upgrade! (to avoid apt deadlocks)

# Inspection

```python
help() # start interactive help
help(classname) # built-in to view help e.g. for a method, class,..
type(object) # get object's type
vars(object) # get a dict of an object's variables (same as __dict__)
dir(object) 
```

# Logging

Sane setup for default logging

```python
import logging

# the next two lines only in the main entry point
logFormat = "%(asctime)s %(name)s %(levelname)s | %(message)s"
logging.basicConfig(format=logFormat, datefmt="%Y-%m-%d %H:%M:%S", level=logging.INFO)

logger = logging.getLogger(__name__)
```

# Profiling

- [py-spy](https://github.com/benfred/py-spy) - the recommended tool as of 2024, out of process sampling profiler that attaches to a running python program, can show a `top` like live view, flame graph,.. (also check out https://github.com/jlfwong/speedscope to view the flame graph!)
  ```bash
  py-spy record --format speedscope -o spy-output.speedoscope.json -- python run-the-script.py
  ```
- [pyinstrument](https://github.com/joerick/pyinstrument) - CLI sampling profiler that runs your program and prints a call stack (or a nice HTML output)
- [cProfile](https://docs.python.org/3/library/profile.html) - part of the standard lib

# Package & Environment Management

Have a look at the [official recommendations](https://packaging.python.org/en/latest/guides/tool-recommendations/).

## venv

This is a [built-in module](https://docs.python.org/3/library/venv.html) (since python 3.3) for creating isolated virtual python environments.
It is comparable (but not 100% identical) to the third party tool `virtualenv`.

> [!note] `pyenv` was a way to invoce `venv` but is deprecated since python 3.6.

Create a virtual environment (you can choose any name you like instead of *.venv*):
```bash
cd project-dir
python -m venv .venv # plain environment
python -m venv --system-site-packages .venv # including packages of your system python (I guess all python-* packages on Ubuntu)
```


Activate / deactivate the environment
```bash
source venv/bin/activate
deactivate
```

### virtualenv

[`virtualenv`](https://virtualenv.pypa.io) provides some features not contained in `venv`.
Consider using it when you need those.

Create and (de)activate a new virtual environment:
```bash
virtualenv venv # creates a folder `venv`
source venv/bin/activate
deactivate
```

## pip

`pip` is the (official) python package manager that installs packages from [PyPI - the Python Package Index](https://pypi.org/).

> [!warning] Don't destroy your system python
> Starting with Ubuntu 24.04 the system python environment is protected
> and it is strongly encouraged to only install packages into virtual environments

Install a single package
```bash
pip install packagename
pip install --upgrade packagename
pip uninstall packagename
```

Install dependencies (typically for a whole project) of a [requirements file](https://pip.pypa.io/en/stable/reference/requirements-file-format/)
```bash
pip install -r requirements.txt
```

A simple `requirements.txt` can look like this:
```
# comment
black # any version
pytest==7.2.0 # exact version
anotherproject >= 1.2, < 2.0 # range of versions
```

More useful commands:
```bash
pip list # view installed dependencies and their version
```

Install a library residing in a subfolder (e.g. a dependency where you checked out the source code from GitHub) in *editable* mode.
This means any changes to the library code are immediately visible - no need to re-package the library.
```bash
pip install -e libs/the-library
```

## Combining venv and pip

It is best practice to avoid installing packages in the system python.
Instead create a virtual environment for each project and install packages there.

```bash
cd project-dir
python -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```

## conda / mamba

[`conda`](https://conda.io) and [`mamba`](https://mamba.readthedocs.io) are package + environment managers with similar features.
`mamba` is younger, faster, and often used as of 2023.

> [!NOTE] Licensing issues with [Anaconda](https://www.anaconda.com/)
> Before 2020 it was customary to install Anaconda, which is a `conda`-based distribution with its own repo. However, nowadays you require a commercial license. Simply use `conda` with the free `conda-forge` repo.

Free packages can be retrieved from the [conda-forge](https://conda-forge.org/) repo.

More reads:
- https://towardsdatascience.com/a-guide-to-conda-environments-bc6180fc533
- https://www.anaconda.com/blog/understanding-conda-and-pip

### Setup

Install either [miniforge](https://github.com/conda-forge/miniforge) (conda with conda-forage as default repo) or [micromamba](https://mamba.readthedocs.io/en/latest/user_guide/micromamba.html) (mamba withoub base environment, i.e. really small footprint).

Make `conda-forge` the default channel (if not already)

	conda config --add channels conda-forge
	conda config --set channel_priority strict

Disable auto-activation of base profile (so that starting a new terminal does not automatically activate conda)

	conda config --set auto_activate_base false

Install specific packages, e.g. Spyder via https://docs.spyder-ide.org/5/installation.html

	conda create -n spyder-env spyder numpy scipy pandas matplotlib sympy cython


It is probably best practice to have a clean and minimal `base` env basically only containing `conda` itself and separate envs for your work, e.g. one for `spyder`, one for your project work,..

Configure a **package cache** (similar to ~/.m2): https://docs.anaconda.com/anaconda/user-guide/tasks/shared-pkg-cache/

### Working with Conda

Show various conda stats

	conda info

Show channels

	conda config --show channels

list available conda environments and (de)activate one
(when an env is active it is prepended to your terminal prompt, e.g. `(base)`)

	conda env list
	conda activate MY_ENV
	conda deactivate

Create a new environment (optionally with certain packages, which can be restricted to certain versions, useful to specify python version!)

	conda create -n MY_ENV
	conda create -n MY_ENV python=3.8 pandas=0.24.1

Show installed packages and versions

	conda list # of current env
	conda list -n MY_ENV

Show python version of an environment

	conda list -n MY_ENV | grep python

Install a package (from a certain channel)
> Note: mixing channels is **not** recommended, this probably leads to unresolvable dependency cycles
> during the next times you install packages.

	conda install packagename
	conda install -c channelname packagename

Use a  `requirements.txt` file

    conda create -n env-name --file requirements.txt # new environment
    conda install --file requirements.txt            # current (existing) environment

Update conda itself (for the `base` environment)

	conda update -n base conda

Update everything in the given conda environment (except for the python version, this can only be changed by creating a new evironment (?))

	conda update --all

Export the current environment to an `environment.yml` file (put it in your project's root directory)

	conda env export --file environment.yml  # export the current environment

Update an existing environment after manually changing `environment.yml`

    conda env update --file environment.yml --prune

Recreate an environment from `environment.yml`
```bash
conda env create -f /path/to/environment.yml
conda env create -n MY_ENV -f /path/to/environment.yml # overriding environment name
```

### Free up space

Environments take up quite a bit of space, delete ones you don't need anymore:

    conda env list
    conda env remove -n ENV_NAME

Then remove unused packages and caches

    conda clean -a


# Build Systems

- [uv](https://docs.astral.sh/uv/) - extremely fast Python package and project manager (successor of [rye](https://rye.astral.sh/))
- [flit](https://flit.pypa.io) - especially for building libraries (instead of apps)
- [pdm](https://pdm-project.org/) - a modern Python package and dependency manager
- [Poetry](Poetry.md) 
- "good old" [setuptools](https://setuptools.pypa.io/).


# Fluent Python

> My notes for the book Fluent Python (2nd edition)

##### Chapter 1 - The Python Data Model

- [**special (aka dunder) methods**](https://docs.python.org/3.10/reference/datamodel.html#special-method-names) e.g. `__iter__()`
  - most of them are expected to be run by the Python interpretor, not you
  - implementing them is similar to *operator overloading* and a way to make your classes "pythonic", i.e. compatible with 
    - python's [built-in-functions](https://docs.python.org/3.10/library/functions.html), e.g. implement `__len__()` to support `len()` 
    - special syntax, e.g. implement `__getitem__()` to support `x[i]`
- **Abstract Base Classes** (`abc`) are part of the API - e.g. `collections.abc`
  - but it is not required to inherit from one (e.g. the core collection classes don't)
  - it is sufficient to simply provide the methods

Regarding Python's typing system

- **dynamically typed**:
  - a variable's type may change
  - type checking at runtime (as opposed to compile time)
- **duck typing**:
  - instead of checking an object's type (e.g. with `type()` or `isinstance()`) its interface (i.e. implemented methods) are inspected
  - -> *If it walks like a duck and it quacks like a duck, then it must be a duck*

##### Chapter 2 - An Array of Sequences

- *flat sequences* (e.g. array) are much more memory efficient than *container sequences* (e.g. list)
- *tuples* (`(1,2)`) can be used as immutable lists but also as records (order = semantic)
  - performance advantage over lists (memory but also access speed) 
  - especially interesting: [collections.namedtuple](https://docs.python.org/3.10/library/collections.html#collections.namedtuple)

**list comprehensions**

actual list vs. generator
```python
symbols = '$¢£¥€¤'
codes = [ord(symbol) for symbol in symbols] # brackets: creates an actual list
codes_gen = (ord(symbol) for symbol in symbols) # parenthesis: creates a generator 
```

can be nested (same result as with nested for loops)
```python
tshirts = [(color, size) for size in sizes
                         for color in colors]
```

walrus operator: variable is created in outer scope and available after listcomp finishes
```python
codes = [last := ord(symbol) for symbol in symbols]
print(last) 
```


# PyQGIS

> see also [QGIS](gis/QGIS.md)

Example for processing scripts: https://github.com/gacarrillor/pyqgis_scripts/tree/master/pyqgis_custom_processing_algorithm_standalone


## Loading Vector Layers

https://qgis.org/pyqgis/3.28/core/QgsVectorLayer.html#module-QgsVectorLayer

Most of the time we can use `ogr` as data provider. It supports [many drivers](https://gdal.org/drivers/vector/index.html) to common data formats.
### hidden URI options

When creating a vector layer for `GeoJSON` it is useful to define `geometrytype` in the ogr connection URI.
```python
QgsVectorLayer(
        "/tmp/osm-facilities/overpass/industrial_landuse.geojsonl|geometrytype=Polygon", "layer name", "ogr"
    )
```
I wonder where this is actually specified? Maybe here: https://gdal.org/api/python/osgeo.gdal.html#osgeo.gdal.VectorTranslateOptions