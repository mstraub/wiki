When using HTTPS on your server you should most probably also provide a **valid** [TLS](https://de.wikipedia.org/wiki/Transport_Layer_Security) (aka SSL) certificate.
Otherwise browsers tend to block access to your page or at least give a big warning that the page is not secure.

[TOC]

# Let's Encrypt

[Let's Encrypt](https://letsencrypt.org) is a free, automated, and open certificate authority (CA), run for the public’s benefit.
It is a service provided by the Internet Security Research Group (ISRG).

When using Apache configuration is straightforward and documented in the official docs.

https://letsencrypt.org/how-it-works/
http://knowles.co.za/creating-renewing-a-lets-encrypt-certificate-for-apache-and-wildfly/

## Workflow when using WildFly

[WildFly](WildFly) is not directly supported, so we have to use certbot's standalone mode.
Install certbot (see https://certbot.eff.org/#ubuntuxenial-other)

### Certbot

Install certbot:

```bash
sudo apt update
sudo apt install software-properties-common
sudo add-apt-repository ppa:certbot/certbot
sudo apt update
sudo apt install certbot

sudo mkdir /opt/letsencrypt; cd /opt/letsencrypt
```

Then use interactive certificate creation (make sure port 80 or 443 are available!):

```bash
# sudo systemctl stop wildfly
sudo certbot certonly --standalone -d YOURDOMAIN
```

Then certificates reside in `/etc/letsencrypt/live/YOURDOMAIN`

**FIXME** document backing up of certificates

#### Create Java Keystore

Now we have to create a java keystore (.jks) for use with WildFly. Adapt the variables to match your use case, and note, that you should delete the lines containing passwords from your `~/.bash_history` (or equivalent):

```bash
YOURDOMAIN=example.com
YOURKEYSTORENAME=k
KEYSTOREALIAS=a
OPENSSL_PASS=p
WILDFLY_NEW_STORE_PASS=p
WILDLFY_NEW_KEY_PASS=p
NEW_KEYSTORE_FILE=f
```

```bash
sudo openssl pkcs12 -export -in /etc/letsencrypt/live/${YOURDOMAIN}/fullchain.pem -inkey /etc/letsencrypt/live/${YOURDOMAIN}/privkey.pem -out ${YOURKEYSTORENAME}.p12 -name ${KEYSTOREALIAS} -passout pass:${OPENSSL_PASS}
sudo keytool -importkeystore -deststorepass ${WILDFLY_NEW_STORE_PASS} -destkeypass ${WILDLFY_NEW_KEY_PASS} -destkeystore ${NEW_KEYSTORE_FILE}.jks -deststoretype PKCS12 -srckeystore ${YOURKEYSTORENAME}.p12 -srcstoretype PKCS12 -srcstorepass ${OPENSSL_PASS} -alias ${KEYSTOREALIAS}
```

Copy the keystore to WildFly:
```bash
sudo cp ${NEW_KEYSTORE_FILE}.jks /opt/wildfly/standalone/configuration
sudo chown wildfly:nogroup /opt/wildfly/standalone/configuration/${NEW_KEYSTORE_FILE}.jks
```

#### WildFly Setup

Update the WildFly configuration in `/opt/wildfly/standalone/configuration/standalone.xml` in section server -> management:

```xml
`<security-realm name="SslRealm">`
    `<server-identities>`
        `<ssl>`
            <keystore path="NEW_KEYSTORE_FILE.jks"
                      relative-to="jboss.server.config.dir"
                      keystore-password="WILDFLY_NEW_STORE_PASS"
                      alias="KEYSTOREALIAS"
                      key-password="WILDFLY_NEW_KEY_PASS"/>
        `</ssl>`
    `</server-identities>`
`</security-realm>`
```

#### Update certificate

Since the certificates are not valid for long (3 months) we need regular updates. Check the validity of your certificates:

```bash
sudo certbot certificates
```

In case WildFly uses port 80/443 you have to shut it down now. Unfortunately you can not specify a different port for the validation, see `%%certbot --help standalone%%` (especially the option `%%--tls-sni-01-port%%`), and https://github.com/certbot/certbot/issues/2697.

Then renew the certificate (updates `/etc/letsencrypt/live/${YOURDOMAIN}/fullchain.pem`):

```bash
sudo certbot renew
```

Then repeat the steps for creating a new java keystore.

Note: as of 2018-08 shutting down may not be necessary anymore, see `certbot --help standalone` and the option `--tls-sni-01-port`. See also https://github.com/certbot/certbot/issues/2697 

#### old approach?

TODO I found this old script.. still useful?

```
#!/bin/bash
echo "this script must be run as root"
echo "also before running this you should run $ sudo certbot renew --dry-run"
echo ".. and of course without dry-run if everything is OK"

cat /etc/letsencrypt/live/mytrips.ait.ac.at/cert.pem /etc/letsencrypt/live/mytrips.ait.ac.at/privkey.pem > /etc/letsencrypt/live/mytrips.ait.ac.at/web.pem
```