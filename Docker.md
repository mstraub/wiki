>[!tldr] Docker is a tool that is used to automate the deployment of applications in lightweight containers so that applications can work efficiently in different environments in isolation. 

The core of docker is the [engine](https://docs.docker.com/engine/), which is sufficient to work with docker on the CLI, and is free (open source).
[Docker Desktop](https://docs.docker.com/engine/) provides a GUI but requires a commercial license.

# Jargon

- **Image**: can be thought of as a (lifeless) blueprint of an application. It is an ordered collection of root filesystem changes and the corresponding execution parameters for use within a **container** runtime. An image typically contains a union of **layered** filesystems stacked on top of each other. An image does not have state and it never changes.
- **Layer**: in an **image**, a layer is modification to the image, represented by an instruction in the **Dockerfile**. Layers are applied in sequence to the base image to create the final image. When an image is updated or rebuilt, only layers that change need to be updated, and unchanged layers are cached locally.
- **Dockerfile**: contains instructions for creating an **image**, i.e. use a base image and then adding **layers** by executing instructions on top of it.
- **Container**: is a runtime instance packaging code and dependencies together to create a light-weight, abstracted, portable app created on top of **images** and is run in its own isolated environment.
- **Volume**: independent construct designed for persisting data, can be attached to **containers**.
- **Compose**: tool for defining a multi-container application in a single file

see https://docs.docker.com/glossary/

## Images

Search for images on https://hub.docker.com and then download (pull) an image
```bash
docker pull NAME # shortcut
docker image pull NAME # the full command
```

List all images (and their sizes)
```sh
docker images # shortcut
docker image ls 
```

Remove unused images
```bash
docker image prune -a
```

## Containers

List containers
```sh
docker ps      # only running containers
docker ps -a   # all containers
docker container ls -a
```

Start / stop a container 
```bash
docker start CONTAINER_NAME_OR_ID
docker stop CONTAINER_NAME_OR_ID
```

Open a shell on a running container (only works if `sh` is available of course)
```
docker exec -it CONTAINER_NAME_OR_ID sh
```
This can also be conveniently done in VSCode via the docker plugin.

Delete a container
```bash
docker container rm CONTAINER_NAME_OR_ID
```

Delete all stopped containers
```bash
docker container prune
```

## Volumes

Volumes hold persisted data. On Linux you find data for your volume in `/var/lib/docker/volumes/NAME/_data`.

List all volumes
```bash
docker volume ls
```

Delete a volume
```bash
docker volume rm VOLUME_NAME
```

## Contexts

Contexts define endpoints, e.g. typically the `default` context is local and we have an additional context for the server we want to deploy to.

list all contexts
```sh
docker context ls
```

deploy to a context (detached)
```sh
docker --context THECONTEXT compose up -d 
```

# Usage

## Build an image

To build an image from a `Dockerfile` go to its directory and:
```bash
docker build .
docker build . -t username/imagename # assign tag (name) to image
```

## Run an Image

Create a container (from an image) and give it a `name` for easier handling - otherwise you must use IDs.
```bash
docker run --name NEWLY_CREATED_CONTAINER_NAME IMAGE
```

Interactively run an image (i.e. create a container)
```bash
docker run --name XXX -it ubuntu:22.04 bash # -i = interactive, -t = tty
```

Start and attach to the newly created container
```bash
docker start XXX -ia # -i = interactive, -a = attach
```

To publish a container's port to a host port use the `-p ` option, which can also be used multiple times:
`-p <host_port1>:<container_port1> -p <host_port2>:<container_port2>`

## Compose

Use a [compose.yml](https://docs.docker.com/compose/compose-file/) file to define multi-container applications (think db, caches, web services,..) and their orchestration.
All commands below are issued in the directory where the YAML file resides.

View a list of all services defined in the compose file
```bash
docker compose config --services
```

(Re)build (selected) services
```bash
docker compose build # build all services
docker compose build [SERVICE_NAME...] # build only selected services
```

Deploy (and build if not yet built)
```sh
docker compose up # by default reads compose.yml and compose.override.yml
docker compose up --build # same as above but before: rebuild images
docker compose -f compose.yml -f compose.alternative.yml # override usage of default config files
```

### Detached (run in the background)

add `-d`:
```sh
docker --context THECONTEXT compose up -d
```

## Logs

> [!warning] Volatile logs
> Note that logs are **lost** when the container is removed or updated when using the default driver (e.g. via `compose up`)!
> Choose a different log driver as needed: https://docs.docker.com/engine/logging/configure/#configure-the-logging-driver-for-a-container

view logs, print timestamps, and **follow**:
```sh
docker compose logs -t -f
```

view logs of a specific service for the last hour
```sh
docker compose logs THE_SERVICE --since 1h
```

get a list of all service
```sh
docker compose ps
```

# Server/Engine Installation

Info below written for configuration of an [APT-based](https://docs.docker.com/engine/install/ubuntu/) installation on Ubuntu 24.04

- users that want to interact with docker should be added to the `docker` group. otherwise you will see errors like `permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock`

View logs of the server: `sudo journalctl -fu docker.service`

# Troubleshooting

A good starting point is to use `docker inspect` to get low-level details about any docker object (e.g. the network settings and IP of a container).

Docker stores most data in `/var/lib/docker` and `/var/run/docker`, `~/.docker`

## Docker command is stuck

docker commands are stuck, not even `docker --help` prints an output but hangs. rebooting the system has no effect.

Let's use `strace -f docker` to show where it is stuck.

    [pid  9722] connect(3, {sa_family=AF_INET, sin_port=htons(22), sin_addr=inet_addr("62.218.45.74")}, 16

**SOLUTION**: if the default context is not properly reachable (in our case the VPN must be active.. to connect to the proper ports I guess) then docker completely gives up on us.

## Connection issues with VPNs

A common problem is a collision of the subnet used by the VPN with the subnet used by docker.
See e.g. [DNS issues](https://engineering.talis.com/articles/docker-vpn-conflict/) or [IP conflicts](https://www.lullabot.com/articles/fixing-docker-and-vpn-ip-address-conflicts)

I encountered something even weirder: a [smaller MTU of the VPN tunnel than docker's default of 1500](notes/2024-10%20docker%20vpn.md)
lead to some HTTP connections timing out.
So check your connection's MTU with `ip address` and adjust your docker config accordingly:

```
❯ cat /etc/docker/daemon.json
{ "mtu": 1360 }
```


## Wipe

### Remove a compose (and all dependencies)


Stop and remove container(s) from a specific composition:

    docker compose down

This makes all images and volumes for that composition unused.
The next step is to remove those.

Remove unused images

    docker image prune -a

Remove unused volumes

    docker volume prune -a

### Brute force

>[!WARNING] Completely wipe all docker data - including persisted data in volumens

Remove all containers:

    docker rm -f $(docker ps -a -q)

Delete all volumes:

    docker volume rm $(docker volume ls -q)

## Reclaim disk space

If you notice that  `/var/lib/docker/overlay2` takes up a lot of space try to remove the build cache (or all unused images etc):

```bash
docker builder prune # only build cache
docker system prune # build cache, unused images,..
```

# Tricks

https://medium.com/@TimvanBaarsen/how-to-connect-to-the-docker-host-from-inside-a-docker-container-112b4c71bc66