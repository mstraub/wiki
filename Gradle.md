[Gradle Build Tool](https://gradle.org/)

> [!TIP] Usage of the [Gradle Wrapper](https://docs.gradle.org/current/userguide/gradle_wrapper.html) is recommended instead of a system-wide installation of gradle!
> That's because gradle moves fast and you probably don't want to fix an old project's setup that is broken in the most recent gradle version due to deprecations.
> Hence `./gradlew` and not `gradle` in the following commands

Note: create a wrapper from scratch with the VSCode plugin for gradle: `Create a Gradle Java Project`

List available tasks
```bash
./gradlew tasks
./gradlew tasks --all # also show custom tasks
```

Create a project (first copy `gradlew`, `gradlew.bat` and the folder `gradle` from an existing project into a new directory)
```bash
./gradlew init # an interactive session that sets up your project follows
```

Build (use `-x` to exclude an otherwise automatically included task)
```bash
./gradlew build
./gradlew clean build -x test	# build but exclude the unit tests
```

Show list of subprojects
```bash
./gradlew projects
```

Build only a submodule (or replace build with any other phase you want to execute)

	./gradlew :SUBMODULE_NAME:build

Publish (e.g. to Nexus)

	./gradlew clean build publish

List dependency trees for different build phases

	./gradlew dependencies

Update the Gradle Wrapper script (see [list of releases](https://gradle.org/releases) for a valid target version)

	./gradlew wrapper --gradle-version TARGET_VERSION

Build fat jars with the [shadowJar plugin](https://github.com/johnrengelman/shadow)

# Caveats

## SNAPSHOT dependencies

Snapshot dependencies are not updated automatically immediately (as you may expect when migrating from `maven`).
There is only a command to update and re-resolve **all** dependencies:

	gradle clean build --refresh-dependencies

Or even better add this section to your project's `build.gradle` so that gradle **always** checks if there is a new snapshot available:

```
configurations.all {
    resolutionStrategy.cacheDynamicVersionsFor 0, 'seconds'
    // -SNAPSHOTs are automatically detected as changing
    resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
}
```

As a last resort you could remove cached artifacts directly in the file system:

	rm -rf /home/mstraub/.gradle/caches/modules-2/files-2.1/at.ac.ait/ariadne-*
	rm -rf /home/mstraub/.gradle/caches/modules-2/metadata-2.96/descriptors/at.ac.ait/ariadne-*

**When using Eclipse:**

After refreshing the dependencies on the CLI you still have to `Refresh Gradle Project` within Eclipse, otherwise the dependencies there are not updated!
