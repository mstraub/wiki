# Docker Troubles

## Reproduce (basic):

0. Im Homeoffice sitzen mit aktiver VPN
1. Ubuntu Image pullen: `docker pull ubuntu:22.04`
2. Image interaktiv laufen lassen: `docker run --rm -it --name ubutest ubuntu:22.04 bash`
3. Versuchen den apt cache runterzuladen `apt update`

## Reproduce (custom image for better debugging info)

Use this`Dockerfile`
```
FROM ubuntu:22.04
RUN apt update -y
RUN apt install -y iputils-ping iputils-tracepath traceroute wget curl
```

Build and run image with curl ping and other tools:
```bash
docker build . -t dockerfail
docker run --rm -it --name ubutest dockerfail bash
```

In the container run (and save the output):
```bash
ping -c 3 google.at
curl --connect-timeout 20 --trace - https://www.google.at

ping -c 3 blog.fefe.de
curl --connect-timeout 20 --trace - https://blog.fefe.de

ping -c 3 archive.ubuntu.com
curl --connect-timeout 20 --trace - http://archive.ubuntu.com/ubuntu/dists/jammy/Release
```

For debugging save network settings:
```bash
ifconfig > output-ifconfig.txt
docker inspect ubutest > output-inspect-ubutest.txt
```

### Results

1) Magenta Wifi + VPN:
   - all pings OK
   - download google OK
   - download fefe + ubuntu FAIL

2) Wifi hotspot with HOT as provider + VPN
   - all pings OK
   - all downloads OK


### Solution? It's an MTU problem!

Apparently docker automatically uses an MTU of 1500.

If the network interface's MTU is smaller then problems may arise.
Even though (IP) addresses are reachable some http connections work, some don't. Exactly as described in https://github.com/moby/moby/issues/13475.

In my case the PulseSecure tunnel has an MTU of **1360**:

```
❯ ip address
...
39: tun0: <POINTOPOINT,MULTICAST,NOARP,UP,LOWER_UP> mtu 1360 qdisc fq_codel state UNKNOWN group default qlen 500
    link/none
    inet 10.18.16.135/32 scope global tun0
       valid_lft forever preferred_lft forever
    inet6 fe80::85ee:559a:da5:1b11/64 scope link stable-privacy
       valid_lft forever preferred_lft forever
```

Simply configuring the docker daemon to use this lower MTU fixes all issues:

```
❯ cat /etc/docker/daemon.json
{ "mtu": 1360 }
```

https://www.civo.com/learn/fixing-networking-for-docker
https://stackoverflow.com/questions/73101754/docker-change-mtu-on-the-fly



## Maybe related - troubles mstubenschrott hat with openssl from WSL

```bash
openssl s_client -connect cip-data-api.sbcdc.ch:443
```

hängt im container nach

CONNECTED(00000003)

Erst wenn ich TLS 1.1 force gehts:

```bash
openssl s_client -tls1_1 -connect cip-data-api.sbcdc.ch:443
```

TLS 1.2 und 1.3 nicht 
